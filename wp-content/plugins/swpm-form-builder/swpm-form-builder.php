<?php
/*
Plugin Name: Simple Membership Form Builder
Description: Simple Membership Addon to Dynamically Build Registration and Edit Profile Forms.
Plugin URI: https://simple-membership-plugin.com/simple-membership-form-builder-addon/
Author: wp.insider
Author URI: https://simple-membership-plugin.com/
Version: 3.8
*/

/* 
 * Helpful pointers for dev
 * See save_add_new_form() for after a new form is created
 */


define('SWPMFB_VERSION', '3.8' );
define('SWPMFB_SCRIPT_DEBUG', true);
define('SWPM_FORM_BUILDER_PATH', dirname(__FILE__) . '/');
define('SWPM_FORM_BUILDER_URL', plugins_url('',__FILE__));
require_once ('classes/class.swpm-form-builder.php');
require_once ('classes/class.swpm-fb-installer.php');
require_once('classes/class.swpm-fb-utils.php');
add_action('plugins_loaded', 'swpm_load_form_builder');
register_activation_hook( SWPM_FORM_BUILDER_PATH .'swpm-form-builder.php', 'SwpmFbInstaller::activate' );

function swpm_load_form_builder(){
    new Swpm_Form_Builder();
}
