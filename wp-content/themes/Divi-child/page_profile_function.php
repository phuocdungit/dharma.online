<?php
/**
 * Created by PhpStorm.
 * User: ANH
 * Date: 4/28/2016
 * Time: 9:28 AM
 */

/*
 * CheckProfile
 * Get data khi vao page Profile
 */
add_action('wp_ajax_CheckProfile', 'CheckProfile');
add_action('wp_ajax_nopriv_CheckProfile', 'CheckProfile');

function CheckProfile()
{
    $total_user_practice = 0;
    $current_user = wp_get_current_user();
    if (0 != $current_user->ID) {
        $user_id = $current_user->ID;
    }
    if ($user_id) {
        $user_info = get_userdata($user_id);
        global $wpdb;
        $get_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $user_id . " AND `meta_key`='user_meta_current_practice'");
        $center_select = get_the_author_meta('user_meta_center_filter', $user_id);
        $leaderboard = get_the_author_meta('user_meta_leaderboard_list', $user_id);
        $description = get_the_author_meta('user_meta_group_description', $user_id);
        $personal_target = get_the_author_meta('user_meta_my_target', $user_id);
        $avatar = get_user_meta($user_id, 'user_avatar', TRUE);
        $html_practice = '';
        foreach ($get_practice as $key => $value) {
            $name[] = unserialize($value->value);
        }
        $name = array_sort($name, 'practices_name', SORT_ASC);
        if (isset($_COOKIE['accumulation'])) {
            $accumulation = $_COOKIE['accumulation'];
        } else {
            $accumulation = check_current_accumulations();
        }
        $meta_data = get_post_meta($accumulation);

        foreach ($name as $key => $value) {

            $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $user_info->user_login . "' AND b.meta_value='" . $value['practices_name'] . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
            foreach ($manis_done as $practice) {
                $date_manis = get_post_meta($practice->ID, 'wpcf-date-center', true);
//                if ($date_manis > $meta_data['wpcf-acc-start-date'][0] && $date_manis < $meta_data['wpcf-acc-end-date'][0]) {
                $total_user_practice += get_post_meta($practice->ID, 'wpcf-manis-done', true);
//                }
            }
            $target = ($value['target_practice_all'] > 0) ? " of " . number_format($value['target_practice_all']) : "";

//            $target = number_format($value['target_practice_all']);

            $starting_count = get_starting_count($value['practices_name'], $accumulation);
            $html_practice .= '<div class="group-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="row ' . $key . '">
                        <div class="main-content">
                            <div class="text-left col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="row practices_name">
                                   <p class="title">' . $value['practices_name'] . '</p>
                                 </div>
                            </div>
                            <div class="text-right col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="row practices_button details">
                                   <a class="button details" href="javascript:;" onclick="App.editPractices(\'' . $value['practices_name'] . '\',' . $key . ',\'' . number_format($total_user_practice + $starting_count) . $target . '\')">' . __('details', 'Divi') . '</a>
                                   <a class="button load display" href="javascript:;">Load...</a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="row">
                                    <input type="text" class="count_practice" value="' . number_format($total_user_practice + $starting_count) . $target . '" readonly>
                                </div>
                            </div>
                         </div>
                         <div class="edit-content">
                         </div>
                    </div>
              </div>';
            $total_user_practice = 0;
            $target = '';
            $over_target = get_user_meta($user_id, 'overall_target_' . $accumulation, TRUE);
        }
//        exit;
        echo json_encode(array(
            'msg' => 'sendinfo',
            'err' => 0,
            'type' => 3,
            'total_user_target' => $personal_target,
            'center_select' => $center_select,
            'leaderboard' => $leaderboard,
            'description' => $description,
            'myname' => html_entity_decode($user_info->display_name),
            'avatar' => $avatar,
            'email' => $_POST['email'],
            'html_practice' => $html_practice,
            'id' => $user_id,
            'over_target' => $over_target,
        ));
        exit;
    }
}

/*
 * profile_func
 * add shortcode page profile
 */

function profile_func()
{
    $current_user = wp_get_current_user();
    if (0 != $current_user->ID) {
        $userId = $current_user->ID;
    }

// $userInfo = get_user_by('email', $_COOKIE['email_center']);
//    $userId = $userInfo->data->ID;
    if ($_GET['tab'] == 'practices') {
        $active_a2 = 'active';
        $active_tab2 = 'in active';
    } else {
        $active_a1 = 'active';
        $active_tab1 = 'in active';
    }
    $mail_center_html = ($current_user->user_login) ? $current_user->user_login : '';
    $html = '<div class="et_pb_row center-box more profile_func">';
    $html .= '<ul class="nav nav-tabs et_pb_tabs">
            <li class="' . $active_a1 . '"><a href="#profile">' . __('General Profile', 'Divi') . '</a></li>
            <li class="' . $active_a2 . '"><a href="#practices">' . __('my Practices', 'Divi') . '</a></li>
        </ul>';
    $html .= '<div class="tab-content et_pb_tabs">
            <div id="profile" class="tab-pane fade ' . $active_tab1 . ' center-box profile">
                <form id="formSendProfile" class="formSendProfile" name="formSendProfile" method="POST"  enctype="multipart/form-data">
                        <p class="notice"></p>
                        <input type="hidden" name="action" value="upProfile">
                        <input type="hidden" name="id" class="id" value=""/>
                        <input type="hidden" name="id" value="' . $userId . '">
                        <div class="group-box">
                            <label class="label" for="email">' . __('my eMail', 'Divi') . '</label>
                            <input type="text" name="email" id="email" class="email" value="' . $mail_center_html . '" placeholder="E-mail" readonly/>
                        </div>
                        <div class="group-box">
                            <label class="label" for="name"></label>
                            <input type="checkbox" name="leaderboard" id="leaderboard" onchange="App.ChangeLeaderboard()" class="leaderboard" value="1"/>This is a group collection
                        </div>
                        <div class="group-box">
                            <label class="label myname" for="name">' . __('my Displayname', 'Divi') . '</label>
                            <input type="text" name="name_profile" id="name" class="name" value="" placeholder="Name"/>
                        </div>
                        <div class="group-box description display">
                            <label class="label" for="group-description">' . __('Group description / member names', 'Divi') . '</label>
                            <textarea rows="4" cols="50" class="group-description" name="group-description"></textarea>
                        </div>
                        <div class="group-box">
                            <label class="label" for="firstname">' . __('First Name', 'Divi') . '</label>
                            <input type="text" id="firstname" name="firstname" class="firstname" value="' . $current_user->first_name . '"/>
                        </div>
                        <div class="group-box">
                            <label class="label" for="lastname">' . __('Last Name', 'Divi') . '</label>
                            <input type="text" id="lastname" name="lastname" class="lastname" value="' . $current_user->last_name . '"/>
                        </div>
                        <div class="group-box">
                            <label class="label">' . __('my Center', 'Divi') . '</label>' .
        get_all_center_html() . '
                        </div>
                        <div class="add_new_center display">' . formAddCenter() . '</div>';
//                        <div class="group-box">
//                            <label class="label" for="my-target">' . __('my Target', 'Divi') . '</label>
//                            <input type="text" id="my-target" name="my-target" class="my-target" value="" placeholder="Target"/>
//                            <script type="text/javascript">$(".my-target").maskMoney();</script>
//</div>
    $html .= '<div class="group-box">
                            <label class="label">' . __('my Picture', 'Divi') . '</label>
                            <div class="Picture">
                                &nbsp;
                            </div>
                            <div class="control-image">
                                <div class="img-avatar">
                                    <a href="javascript:void(0)" onclick="jQuery(\'.upload-avatar\').fadeIn(\'slow\').css(\'display\', \'inline-block\');
                                                                jQuery(this).hide()" class="btn-showupload">' . __('Change Avatar', 'Divi') . '</a>
                                    <div class="upload-avatar">
                                        <input type="file" accept="image/*" name="img-avatar" class="input-upload">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="submit profile">
                            <a class="button" href="javascript:;" onclick="jQuery(\'#formSendProfile\').submit();">' . __('Update', 'Divi') . '</a>
                        </div>
                    </form>
            </div>
            <div id="practices" class="tab-pane fade ' . $active_tab2 . '">
             <p>Select practice to add to your profile</p>
            <div class="group-box">' .
        get_all_practice_html('', 1)
        . '<div class="submit addpractices">
               <a class="button" href="javascript:;" onclick="App.formAddPractices()">' . __('Add', 'Divi') . '</a>
               </div>' .
        formAddPractice()
        . '</br></br><p><b>Your current practices</b></p>
                <div class="accumulations_select">
            <label class="label">' . __('Accumulation', 'Divi') . '</label>
            ' . get_all_accumulations_html(3) . '
            </div>
            <div class="image-loading loading display"></div>
            
                 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 overall-target-box">
                                <div class="row">
                                <p class="title overall-target-title">Overall target</p>
                                    <input type="text" class="overall-target count_practice" value="" >
                                    <div class="practices_button update">
                                <a class="button update-overall-target" href="javascript:;" onclick="App.updateOverallTarget()">Update</a>
                              </div>
                                    <script type="text/javascript">$(".overall-target").maskMoney();</script>
                                </div>
                            </div>
                 <div class="your-practices">
                 

            </div>
        </div>
    </div>';

    return $html;
}

add_shortcode('profile', 'profile_func');

/*
 * upProfile
 * Update thong tin profile
 */
add_action('wp_ajax_upProfile', 'upProfile');
add_action('wp_ajax_nopriv_upProfile', 'upProfile');

function upProfile()
{
    $current_user = wp_get_current_user();
    if (0 != $current_user->ID) {
        $user_id = $current_user->ID;
    }
    if ($user_id && $_POST['name_profile']) {
//    die(json_encode($_FILES));
        if ($_FILES['img-avatar']) {
            $extension = end(explode('.', $_FILES['img-avatar']['name']));
            if ($extension == 'png' || $extension == 'jpg' || $extension == 'gif') {

                $file = basename($_FILES['img-avatar']['name']);
                $file = str_replace(' ', '_', $file);
                $path = WP_CONTENT_DIR . '/uploads/' . $file;
                move_uploaded_file($_FILES['img-avatar']['tmp_name'], $path);

                update_user_meta($_REQUEST['id'], 'user_avatar', $file);
            }
        }
        $denied_dmail = 2;
        if ($_POST['leaderboard']):
            $denied_dmail = 1;
        endif;
        global $wpdb;
        if ($_POST['center_filter'] && $_POST['center_filter'] != 'add') {
            $wpdb->get_results('UPDATE ' . $wpdb->prefix . 'posts a INNER JOIN ' . $wpdb->prefix . 'postmeta b'
                . ' SET b.`meta_value`="' . $_POST['center_filter'] . '" '
                . 'WHERE a.`post_title`="' . $_POST['email'] . '" AND a.`post_status`="private" AND a.`ID`=b.`post_id` AND b.`meta_key`="wpcf-my-center"');
            update_user_meta($user_id, 'user_meta_center_filter', $_POST['center_filter']);
        }
        update_usermeta($user_id, 'user_meta_leaderboard_list', $denied_dmail);
        update_usermeta($user_id, 'user_meta_group_description', $_REQUEST['group-description']);
//wp_set_password($_POST['password'], $user_id);
        update_user_meta($_REQUEST['id'], 'user_meta_my_target', preg_replace('/[^a-zA-Z0-9]/', '', $_REQUEST['my-target']));
        $wpdb->get_results('UPDATE ' . $wpdb->prefix . "swpm_members_tbl a INNER JOIN " . $wpdb->prefix . "swpm_form_builder_custom b"
            . ' SET b.value="' . $_POST['name_profile'] . '" '
            . 'WHERE a.user_name ="' . $_POST['email'] . '" AND a.member_id = b.user_id');
        wp_update_user(array('ID' => $user_id, 'display_name' => $_REQUEST['name_profile']));
        wp_update_user(array('ID' => $user_id, 'first_name' => $_REQUEST['firstname']));
        wp_update_user(array('ID' => $user_id, 'last_name' => $_REQUEST['lastname']));
        echo json_encode(array(
            'msg' => (get_option('update_profile') ? get_option('update_profile') : ""),
            'err' => 0,
        ));
        exit;
    } else {
        $user_id = wp_create_user($_POST['email'], 123456, $_POST['email']);
        $name = explode('@', $_POST['email']);
        $fname = ($_REQUEST['name_profile']) ? $_REQUEST['name_profile'] : $name[0];
        $api = mc4wp_get_api();
        $merge_vars = array(
            'FNAME' => $fname,
            'LNAME' => ''
        );
        $api->subscribe('10b1af0d94', $_POST['email'], $merge_vars, 'html', true);
//        $api->subscribe('10b1af0d94', , $merge_vars);
        $denied_dmail = 2;
        if ($_POST['leaderboard']):
            $denied_dmail = 1;
        endif;
        $data = array(
            'practices_name' => 'Chenrezig',
            'practice_description' => '',
            'starting_count' => 0,
            'target' => 0
        );
        update_usermeta($user_id, 'user_meta_leaderboard_list', $denied_dmail);
        update_usermeta($user_id, 'user_meta_group_description', $_REQUEST['group-description']);
        update_user_meta($user_id, 'user_meta_center_filter', $_POST['center_filter']);
        update_user_meta($user_id, 'user_meta_my_target', preg_replace('/[^a-zA-Z0-9]/', '', $_REQUEST['my-target']));
        update_user_meta($user_id, 'user_meta_current_practice', $data);
        wp_update_user(array('ID' => $user_id, 'display_name' => $fname));
        if ($_FILES['img-avatar']) {
            $extension = end(explode('.', $_FILES['img-avatar']['name']));
            if ($extension == 'png' || $extension == 'jpg' || $extension == 'gif') {

                $file = basename($_FILES['img-avatar']['name']);
                $file = str_replace(' ', '_', $file);
                $path = WP_CONTENT_DIR . '/uploads/' . $file;
                move_uploaded_file($_FILES['img-avatar']['tmp_name'], $path);

                update_user_meta($user_id, 'user_avatar', $file);
            }
        }
        echo json_encode(array(
            'msg' => (get_option('add_profile') ? get_option('add_profile') : ""),
            'err' => 0,
        ));
        exit;
    }
    echo json_encode(array(
        'msg' => (get_option('err_update_profile') ? get_option('err_update_profile') : ""),
        'err' => 1,
    ));
    exit;
}

/*
 * Accumulation_profile
 * Get data if user change acc in page profile
 */

function Accumulation_profile($post_value)
{
    $current_user = wp_get_current_user();
    $meta_data = get_post_meta($post_value['id']);
    if (0 != $current_user->ID) {
        $user_id = $current_user->ID;
    }
    if ($user_id) {
        $total_user_practice = 0;
        $user_info = get_userdata($user_id);
        global $wpdb;
        $get_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $user_id . " AND `meta_key`='user_meta_current_practice'");
        $html_practice = '';
        foreach ($get_practice as $key => $value) {
            $name[] = unserialize($value->value);
        }
        $name = array_sort($name, 'practices_name', SORT_ASC);
        foreach ($name as $key => $value) {
            $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $current_user->user_login . "' AND b.meta_value='" . $value['practices_name'] . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
            foreach ($manis_done as $practice) {
                $date_manis = get_post_meta($practice->ID, 'wpcf-date-center', true);
                if ($date_manis > $meta_data['wpcf-acc-start-date'][0] && $date_manis < $meta_data['wpcf-acc-end-date'][0]) {
                    $total_user_practice += get_post_meta($practice->ID, 'wpcf-manis-done', true);
                }
            }

            $target = ($value['date_target_' . $post_value['id']] > 0) ? " of " . number_format($value['date_target_' . $post_value['id']]) : "";
            if ($post_value['id'] == "") {
                $target = number_format($value['target_practice_all']);
            }
            $starting_count = get_starting_count($value['practices_name'], $post_value['id']);
            $html_practice .= '<div class="group-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="row ' . $key . '">
                        <div class="main-content">
                            <div class="text-left col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="row practices_name">
                                   <p class="title">' . $value['practices_name'] . '</p>
                                 </div>
                            </div>
                            <div class="text-right col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="row practices_button details">
                                   <a class="button details" href="javascript:;" onclick="App.editPractices(\'' . $value['practices_name'] . '\',' . $key . ',\'' . number_format($total_user_practice + $starting_count) . $target . '\')">' . __('details', 'Divi') . '</a>
                                   <a class="button load display" href="javascript:;">Load...</a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="row">
                                    <input type="text" class="count_practice" value="' . number_format($total_user_practice + $starting_count) . $target . '" readonly>
                                </div>
                            </div>
                         </div>
                         <div class="edit-content">
                         </div>
                    </div>
              </div>';
            $total_user_practice = 0;
            $target = '';
            $over_target = get_user_meta($user_id, 'overall_target_' . $post_value['id'], TRUE);
        }

        echo json_encode(array(
            'msg' => 'sendinfo',
            'err' => 0,
            'type' => 3,
            'html_practice' => $html_practice,
            'id' => $user_id,
            'over_target' => $over_target

        ));
        exit;
    }
}

add_action('wp_ajax_updateOverallTarget', 'updateOverallTarget');
add_action('wp_ajax_nopriv_updateOverallTarget', 'updateOverallTarget');

function updateOverallTarget()
{
    $current_user = wp_get_current_user();
    if (0 != $current_user->ID) {
        $user_id = $current_user->ID;
    }
    update_user_meta($user_id, 'overall_target_' . $_POST['accumulation'], $_POST['target']);
    echo json_encode(array(
        'err' => 0,
    ));
    exit;
}