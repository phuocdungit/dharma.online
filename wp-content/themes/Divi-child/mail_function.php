<?php
/**
 * Created by PhpStorm.
 * User: ANH
 * Date: 4/28/2016
 * Time: 9:09 AM
 */

/*
 * sendMail
 * Goi email thong bao cho admin va user khi user count
 * $email: email nguoi nhan
 * $center: center duoc count
 * $practice: practice duoc count
 * $manis: number count
 * $number: == 1 chi goi cho admin, == 2 goi cho admin va user
 */

function sendMail($email, $center, $practice, $manis, $number)
{
//theme admin
    if ($number == 2) :
        $emailAdmin = get_option('admin_email');
        $getIDAdmin = get_page_by_path('theme-center-admin', OBJECT, 'theme-mail');
        $subjectAdmin = get_post_meta($getIDAdmin->ID, 'wpcf-subject', true);
        $headersAdmin[] = 'From: ' . get_post_meta($getIDAdmin->ID, 'wpcf-headers', true) . "\r\n";
        $find = array("[email]", "[center_name]", "[practice_name]", "[manis]");
        $add = array($email, $center, $practice, $manis);
        $messageAdmin = str_replace($find, $add, $getIDAdmin->post_content);
        $messageAdmin = apply_filters('the_content', $messageAdmin);

        add_filter('wp_mail_content_type', function ($content_type) {
            return 'text/html';
        });
        wp_mail($emailAdmin, $subjectAdmin, $messageAdmin, $headersAdmin);
        remove_filter('wp_mail_content_type', 'set_html_content_type');

        function set_html_content_type()
        {
            return 'text/html';
        }

//theme user
        $getIDUser = get_page_by_path('theme-center-user', OBJECT, 'theme-mail');
        $subjectUser = get_post_meta($getIDUser->ID, 'wpcf-subject', true);
        $headersUser[] = 'From: ' . get_post_meta($getIDUser->ID, 'wpcf-headers', true) . "\r\n";
        $theme_user = get_post($getID->ID)->post_content;
        $find = array("[email]", "[center_name]", "[practice_name]", "[manis]");
        $add = array($email, $center, $practice, $manis);
        $messageUser = str_replace($find, $add, $getIDUser->post_content);
        $messageUser = apply_filters('the_content', $messageUser);

        add_filter('wp_mail_content_type', function ($content_type) {
            return 'text/html';
        });
        wp_mail($email, $subjectUser, $messageUser, $headersUser);

        remove_filter('wp_mail_content_type', 'set_html_content_type_user');

        function set_html_content_type_user()
        {
            return 'text/html';
        }
    elseif ($number == 1) :
        $emailAdmin = get_option('admin_email');
        $getIDAdmin = get_page_by_path('theme-center-admin', OBJECT, 'theme-mail');
        $subjectAdmin = get_post_meta($getIDAdmin->ID, 'wpcf-subject', true);
        $headersAdmin[] = 'From: ' . get_post_meta($getIDAdmin->ID, 'wpcf-headers', true) . "\r\n";
        $find = array("[email]", "[center_name]", "[practice_name]", "[manis]");
        $add = array($email, $center, $practice, $manis);
        $messageAdmin = str_replace($find, $add, $getIDAdmin->post_content);
        $messageAdmin = apply_filters('the_content', $messageAdmin);
        add_filter('wp_mail_content_type', function ($content_type) {
            return 'text/html';
        });
        wp_mail($emailAdmin, $subjectAdmin, $messageAdmin, $headersAdmin);
        remove_filter('wp_mail_content_type', 'set_html_content_type');

        function set_html_content_type()
        {
            return 'text/html';
        }

    endif;
}

function sendMailDedication($data)
{
    $emailAdmin = 'phuocdungit@gmail.com';
//    $emailAdmin = get_option('admin_email');
    $getIDAdmin = get_page_by_path('theme-center-admin', OBJECT, 'theme-mail');
    $subjectAdmin = ($_POST['type'] == 'save') ? 'Add new dedication request' : 'Edit dedication request';
    $headersAdmin[] = 'From: ' . get_post_meta($getIDAdmin->ID, 'wpcf-headers', true) . "\r\n";
    $type = ($_POST['form-dedication-visibilit'] == 1) ? 'Private' : 'Public';
    $messageAdmin = '<p><strong>Status: </strong>' . $_POST['type'] . '</p>' . '<p><strong>Dedication Subject: </strong>' . $_POST['form-dedication-subject'] . '</p><p><strong>When?: </strong>' . $_POST['form-dedication-date'] . '</p><p><strong>Description: </strong>' . $_POST['form-dedication-description'] . '</p><p><strong>Type: </strong>' . $type . '</p>';
    $messageAdmin = apply_filters('the_content', $messageAdmin);
    add_filter('wp_mail_content_type', function ($content_type) {
        return 'text/html';
    });
    wp_mail($emailAdmin, $subjectAdmin, $messageAdmin, $headersAdmin);
    remove_filter('wp_mail_content_type', 'set_html_content_type');

    function set_html_content_type()
    {
        return 'text/html';
    }
}
