<?php

/**
 * Created by PhpStorm.
 * User: ANH
 * Date: 4/28/2016
 * Time: 9:24 AM
 */
/*
 * more_func
 * add shortcode page more
 */

function more_func()
{
    $html = '<div class="accumulations_select">
            <label class="label">' . __('Accumulation', 'Divi') . '</label>
            ' . get_all_accumulations_html(2) . '
            </div>
            <div class="image-loading loading display"></div>';
    $html .= '<div class="et_pb_row center-box more more_func">';
    $html .= '<ul class="nav nav-tabs et_pb_tabs">
            <li class="active"><a href="#practice">' . __('Mantras by Practice', 'Divi') . '</a></li>
            <li><a href="#region">' . __('Mantras by Region', 'Divi') . '</a></li>
            <li><a href="#center">' . __('Mantras by Center', 'Divi') . '</a></li>
            <li><a href="#person">' . __('Mantras by Person', 'Divi') . '</a></li>
            <li><a href="#collections">' . __('Mantras by Groups', 'Divi') . '</a></li>
        </ul>';

    $html .= '<div class="tab-content et_pb_tabs">
            <div id="practice" class="tab-pane fade in active">
                <div class="box-top">
                    <div class="group-box">
                        <label class="label">' . __('grand total', 'Divi') . '</label>
                        <input type="text" class="grand_total" value="" readonly>
                    </div>
                    <div class="group-box practice-top">
                    </div>
                    <div class="group-box practice">
                    </div>

                </div>
                <div class="box-content">

                </div>
            </div>
            <div id="region" class="tab-pane fade">
                <div class="box-top">
                    <div class="group-box">
                        <label class="label">' . __('grand total', 'Divi') . '</label>
                        <input type="text" class="grand_total" value="" readonly>
                    </div>
                    <div class="group-box region-top">
                    </div>
                    <div class="group-box region">
                        <label class="label mine">my Region</label>
                        <input type="text" class="my-region" value="" readonly="">
                    </div>

                </div>
                <div class="box-content">

                </div>
            </div>
            <div id="center" class="tab-pane fade">
                <div class="box-top">
                    <div class="group-box">
                        <label class="label">' . __('grand total', 'Divi') . '</label>
                        <input type="text" class="grand_total" value="" readonly>
                    </div>
                    <div class="group-box center-top">
                    </div>
                    <div class="group-box">
                        <label class="label center">' . __('my Center', 'Divi') . '</label>
                        <input type="text" class="myCenter" value="" readonly>
                    </div>
                </div>
                <div class="box-content">

                </div>
            </div>
            <div id="person" class="tab-pane fade">
                <div class="box-top">
                    <div class="group-box">
                        <label class="label">' . __('grand total', 'Divi') . '</label>
                        <input type="text" class="grand_total" value="" readonly>
                    </div>
                    <div class="persontop">

                    </div>
                    <div class="group-box">
                        <label class="label mine">' . __('mine', 'Divi') . '</label>
                        <input type="text" class="mine" value="" readonly>
                    </div>
                </div>
                <div class="box-content">
                </div>
            </div>
            <div id="collections" class="tab-pane fade">
                <div class="box-top">
                    <div class="group-box">
                        <label class="label">' . __('grand total', 'Divi') . '</label>
                        <input type="text" class="grand_total" value="" readonly>
                    </div>
                    <div class="persontop">

                    </div>
                    <div class="group-box">
                        <label class="label mine">' . __('mine', 'Divi') . '</label>
                        <input type="text" class="mine" value="" readonly>
                    </div>
                </div>
                <div class="box-content">
                </div>
            </div>
        </div>
    </div>';
    return $html;
}

add_shortcode('more', 'more_func');

/*
 * more_func custom
 * add shortcode page more
 */

function more_public($data)
{
    $acc = ($data['accumulation']) ? $data['accumulation'] : '';
    $html = '<div class="image-loading loading display"></div>';
    $html .= '<div class="et_pb_row center-box more more_func_custom">';
    $html .= '<input name="accumulation_more" class="accumulation_more" value="' . $acc . '" type="hidden">';
    $html .= '<ul class="nav nav-tabs et_pb_tabs">
            <li class="active"><a href="#practice">' . __('Mantras by Practice', 'Divi') . '</a></li>
            <li><a href="#region">' . __('Mantras by Region', 'Divi') . '</a></li>
        </ul>';

    $html .= '<div class="tab-content et_pb_tabs">
            <div id="practice" class="tab-pane fade in active">
                <div class="box-top">
                    <div class="group-box">
                        <label class="label">' . __('grand total', 'Divi') . '</label>
                        <input type="text" class="grand_total" value="" readonly>
                    </div>

                </div>
                <div class="box-content">

                </div>
            </div>
            <div id="region" class="tab-pane fade">
                <div class="box-top">
                    <div class="group-box">
                        <label class="label">' . __('grand total', 'Divi') . '</label>
                        <input type="text" class="grand_total" value="" readonly>
                    </div>
                   

                </div>
                <div class="box-content">

                </div>
            </div>
        </div>
    </div>';
    return $html;
}


add_shortcode('more_public', 'more_public');

/*
 * getMore
 * Get data khi vao page more
 */
add_action('wp_ajax_getMore', 'getMore');
add_action('wp_ajax_nopriv_getMore', 'getMore');

function getMore()
{
    global $wpdb;
    $days_left = $total_user = $total_user_center = $my_center_target = 0;
    $total_target = get_option('total_target');
//    $user_id = username_exists($current_user->user_login);
    $current_user = wp_get_current_user();
    if (0 != $current_user->ID) {
        $user_id = $current_user->ID;
    }
    /**
     * list Practice
     */
    if ($user_id) {
        /**
         * Get Total practice of User
         */
        $total_practice_of_user = 0;
        $arrayPractice = array();
        $get_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $user_id . " AND `meta_key`='user_meta_current_practice'");
        foreach ($get_practice as $value) {
            $data = unserialize($value->value);
            $name[] = $data['practices_name'];
        }
//        echo '<pre>';
//        print_r($name);
//        exit;
        $args = array(
            'numberposts' => -1,
            'post_type' => 'practic',
            'post_status' => 'private',
            'orderby' => 'name',
            'order' => 'ASC',
        );
        $postslist = get_posts($args);
        $i = 0;
        foreach ($postslist as $key => $value) {
            $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE  b.meta_value='" . $value->post_title . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private' AND b.meta_key='wpcf-my-practice'");
            foreach ($manis_done as $practice) {
                $total_user_practice += get_post_meta($practice->ID, 'wpcf-manis-done', true);
            }
//            echo '<pre>';
//            print_r($total_user_practice);
////            exit;
            $meta_practice = getStartingPractices($value->post_title, $meta_data);
            $total_user_practice += $meta_practice['sum'];
            if ($total_user_practice > 0 && $name) {
                if (in_array($value->post_title, $name)) {
                    $class_active = "active";
                }
                $arrayPractice[] = array('target' => $meta_practice['target'], 'value' => $total_user_practice, 'name' => $value->post_title, 'active' => $class_active);
                $class_active = '';
                $total_user_practice = 0;
            }
        }

//        exit;

        function cmp2($a, $b)
        {
            return $b["value"] - $a["value"];
        }

        uasort($arrayPractice, "cmp2");
//        echo '<pre>';
//        print_r($arrayPractice);
//        exit;
        $i = 1;
        foreach ($arrayPractice as $key => $value) {

            if ($value['active'] == 'active') {
                $htmlPracticTop .= '<label class="label">' . $value['name'] . ' (' . $i . ')' . '</label>
                        <input type="text" class="Centerflag" value="' . number_format($value['value']) . '" readonly="">';
            }
            $htmlPractic .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label">' . $i . '. ' . $value['name'] . '</label>
                        <input type="text" class="grand total left" value="' . number_format($value['value']) . '" readonly="">
                    </div>';
            $i++;
        }

        /**
         * Get practice value of practice
         */
//        $practices_value = $wpdb->get_results("SELECT DISTINCT p.meta_value FROM " . $wpdb->prefix . "postmeta AS p "
//                . "WHERE p.meta_key = 'wpcf-my-practice'");
//        $practice_by_value = array();
//        foreach ($practices_value as $value) {
//            $practices_value = $wpdb->get_results("SELECT p.post_id FROM " . $wpdb->prefix . "postmeta AS p WHERE p.meta_value = '" . $value->meta_value . "'");
//            $tmp_prac = 0;
//            foreach ($practices_value as $prac_id) {
//                $tmp_prac += get_post_meta($prac_id->post_id, 'wpcf-manis-done', true);
//            }
//            $practice_by_value[$value->meta_value] = $tmp_prac;
//        }
    }

    /**
     * End list Practice
     */
    /*
     *     list center
     */
    if ($user_id) {
        $personal_target = get_the_author_meta('user_meta_my_target', $user_id);
        $my_center_target_user = get_the_author_meta('user_meta_center_filter', $user_id);
    }
    $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $current_user->user_login . "' AND b.meta_value='" . $my_center_target_user . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
    $total_all_center = $wpdb->get_results("SELECT SUM(meta_value)AS sum FROM " . $wpdb->prefix . "postmeta b INNER JOIN " . $wpdb->prefix . "posts a WHERE meta_key='wpcf-manis-done' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
//    $total_user_center = $wpdb->get_results("SELECT SUM(b.`meta_value`)AS sum FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $_REQUEST['email'] . "' AND b.meta_key='wpcf-manis-done' AND a.`ID` = b.`post_id` ");
//    $getpost = get_page_by_title($my_center_target_user, OBJECT, 'list-center');
//    $center_target = get_post_meta($getpost->ID, 'wpcf-center_target', true);
    foreach ($manis_done as $key => $value) {
        $total_user += get_post_meta($value->ID, 'wpcf-manis-done', true);
    }

    $args = array(
        'numberposts' => -1,
        'post_type' => 'list-center',
        'post_status' => 'private',
        'orderby' => 'name',
        'order' => 'ASC',
    );
    $postslist = get_posts($args);
    $htmlCenterTop = $htmlCenter = $htmlUser = $htmlRegion = '';
    $arrayCenter = $arrayUser = array();
    foreach ($postslist as $key => $value) {
        $center_name = $value->post_title;
        $Leaderboard = get_post_meta($value->ID, 'wpcf-leaderboard', true);
        $ontop = get_post_meta($value->ID, 'wpcf-on-top', true);
        $center_target = get_post_meta($value->ID, 'wpcf-center_target', true);
        $listID = $wpdb->get_results("SELECT post_id AS ID FROM " . $wpdb->prefix . "postmeta b INNER JOIN " . $wpdb->prefix . "posts a WHERE meta_value ='" . $center_name . "' AND meta_key='wpcf-my-center' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
        foreach ($listID as $key => $ID) {
            $total_list_center += get_post_meta($ID->ID, 'wpcf-manis-done', true);
        }
        $total_list_center += getStartingCenter($center_name);
        if ($center_name == $my_center_target_user) {
            $my_center_target = $center_target;
            $total_user_center = $total_list_center;
            $center_by_user = $center_name;
        }
        if ($center_name == $my_center_target_user && $Leaderboard != 1) {
            $class_active = "active";
        }

        if ($total_list_center > 0 && $ontop == 1):
            $nameOnTop = (get_post_meta($value->ID, 'wpcf-name-on-top', true) != '') ? get_post_meta($value->ID, 'wpcf-name-on-top', true) : $center_name;

            $htmlCenterTop .= '<label class="label">' . $nameOnTop . '</label>
                        <input type="text" class="Centerflag" value="' . number_format($total_list_center) . '" readonly="">';

//            $total_list_center = 0;
        endif;
        if ($total_list_center > 0 && $Leaderboard != 1):
            $arrayCenter[] = array('value' => $total_list_center, 'name' => $center_name, 'target' => $center_target, 'active' => $class_active, 'Leaderboard' => $Leaderboard);
            $class_active = '';
        endif;
        $total_list_center = 0;
    }

    function cmp($a, $b)
    {
        return $b["value"] - $a["value"];
    }

    uasort($arrayCenter, "cmp");
    $i = 1;
    foreach ($arrayCenter as $key => $value) {
        if ($center_by_user == $value['name'] && $value['Leaderboard'] != 1) {
            $number_center_top = $i;
        }

        $htmlCenter .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label">' . $i . '. ' . $value['name'] . '</label>
                        <input type="text" class="grand total left" value="' . number_format($value['value']) . '" readonly="">
                    </div>';
        $i++;
    }

    /*
     *   list user
     */
    $args = array(
        'blog_id' => $GLOBALS['blog_id'],
        'role' => '',
        'role' => 'Subscriber',
        'orderby' => 'ID',
        'order' => 'ASC',
        'offset' => '',
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'user_meta_leaderboard_list',
                'value' => '1',
                'compare' => '!=',
            )
        ),
        'orderby' => 'login',
        'order' => 'ASC',
        'count_total' => false,
        'fields' => 'all',
    );
    $blogusers = get_users($args);

    $total_user2 = 0;
    foreach ($blogusers as $key => $user) {
        $userOntop = get_the_author_meta('user_meta_ontop', $user->ID);
        $my_center_target_user = get_the_author_meta('user_meta_center_filter', $user->ID);
        $personal_target_user = get_the_author_meta('user_meta_my_target', $user->ID);
        $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $user->user_login . "' AND b.meta_value='" . $my_center_target_user . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
//echo ("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $user->user_login . "' AND b.meta_value='" . $my_center_target_user . "' AND a.`ID` = b.`post_id` ").'------';
        foreach ($manis_done as $key => $value) {
            $total_user2 += get_post_meta($value->ID, 'wpcf-manis-done', true);
        }
        $total_user2 += getStartingUser($user->user_login);
        if ($total_user2 > 0):
            $class_active_user = ($current_user->user_login == $user->user_login) ? "active" : "";
            $arrayUser[] = array('ontop' => $userOntop, 'value' => $total_user2, 'name' => $user->display_name, 'target' => $personal_target_user, 'active' => $class_active_user, 'email' => $user->user_login, 'id' => $user->ID);
            $total_user2 = 0;
        endif;
    }

    function cmp_user($a, $b)
    {
        return $b["value"] - $a["value"];
    }

    uasort($arrayUser, "cmp_user");
    $i = 1;
    $number_on_top = 0;
    $htmlUserTop = '';
    foreach ($arrayUser as $key => $value) {
        $image = '';
        $getimage = get_user_meta($value['id'], 'user_avatar', TRUE);
        if ($getimage != '') {
            $image = ' <img src = "/wp-content/uploads/' . $getimage . '" class = "thumbnail img-responsive"/>';
        }
        $current_user = wp_get_current_user();
        if (0 != $current_user->ID) {
            if ($current_user->user_login == $value['email']) {
                $number_on_top = $i;
            }
        }
        if ($value['ontop'] == 1) {

            $htmlUserTop .= '<div class="group-box">
                                <label class="label">' . $value['name'] . '</label>
                                <input type="text" class="grand total left" value="' . number_format($value['value']) . '" readonly="">
                            </div>';

        }

        $htmlUser .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label"> ' . $i . '. ' . $image . $value['name'] . '</label>
                        <input type="text" class="grand total left" value="' . number_format($value['value']) . '" readonly="">
                    </div>';
        $i++;
    }

    /*
     * list Region'
     */

    $my_center = $wpdb->get_results("SELECT b.`meta_value` AS center FROM " . $wpdb->prefix . "users a INNER JOIN " . $wpdb->prefix . "usermeta b WHERE a.`user_login`='" . $current_user->user_login . "' AND b.`meta_key`='user_meta_center_filter' AND b.`user_id`=" . $user_id);
    $getID = get_page_by_title($my_center[0]->center, OBJECT, 'list-center');
    $my_region = get_post_meta($getID->ID, 'my-region', true);
//    echo '<pre>';
//    print_r($my_center);
//    exit;
    $argsregion = array(
        'numberposts' => -1,
        'post_type' => 'region',
        'orderby' => 'name',
        'post_status' => 'private',
        'order' => 'ASC',
    );
    $postslistregion = get_posts($argsregion);
    foreach ($postslistregion as $key => $region) {
        $regionReference = get_post_meta($region->ID, 'wpcf-reference-region-only', TRUE);
        $regionOnTop = get_post_meta($region->ID, 'wpcf-region-on-top', TRUE);
        $regionNameOnTop = get_post_meta($region->ID, 'wpcf-top-name-region', TRUE);

        $target_region = get_post_meta($region->ID, 'region_target', TRUE);
        $args = array(
            'numberposts' => -1,
            'post_type' => 'list-center',
            'orderby' => 'name',
            'order' => 'ASC',
            'post_status' => 'private',
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => 'my-region',
                    'value' => $region->post_title,
                    'compare' => '=',
                ),
            )
        );
        $postslist = get_posts($args);
        $results = array();
        $startcount = 0;
        foreach ($postslist as $key => $simplePost) {
            $argruments = array(
                'numberposts' => -1,
                'post_type' => 'manis-history',
                'post_status' => 'private',
                'orderby' => 'name',
                'order' => 'ASC',
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => 'wpcf-my-center',
                        'value' => $simplePost->post_title,
                        'compare' => '=',
                    ),
                )
            );
            $historyPost = get_posts($argruments);
            $results[] = day_count($historyPost);
            $startcount = getStartingCenter($simplePost->post_title);
        }
//        echo '<pre>';
//        print_r($startcount);
////        exit;
        $running_total_center = $day_count_center = $day_count_today = 0;
        foreach ($results as $simpleResult) {
            $running_total_center += $simpleResult['running_total_center'];
        }
        $running_total_center += $startcount;
        if ($region->post_title == $my_region) {
            $class_active_region = "active";
        }
        $arrayRegion[] = array('reference' => $regionReference, 'ontop' => $regionOnTop, 'nameOnTop' => $regionNameOnTop, 'value' => $running_total_center, 'name' => $region->post_title, 'target' => $target_region, 'active' => $class_active_region);
        $class_active_region = '';
        $running_total_center = 0;
    }

//    exit;

    function cmp_region($a, $b)
    {
        return $b["value"] - $a["value"];
    }

    uasort($arrayRegion, "cmp_region");
    $i = 1;
    $my_region_top = '';
    foreach ($arrayRegion as $key => $value) {

        if ($value['ontop'] == 1):
            $my_region_top .= '<label class="label mine">' . $value['nameOnTop'] . '</label><input type="text" class="my-region" value="' . number_format($value['value']) . '"  readonly="">';
        endif;

        if ($value['name'] == $my_region && $value['value']):
            if ($value['reference'] == 0):
                $my_region_user .= '<label class="label mine">my Region (' . $i . ')</label><input type="text" class="my-region" value="' . number_format($value['value']) . '" readonly="">';
            else:
                $my_region_user .= '<label class="label mine">my Region</label><input type="text" class="my-region" value="' . number_format($value['value']) . '" readonly="">';
            endif;
//        elseif ($value['value']):
//            $my_region_user .= '<label class="label mine">my Region</label><input type="text" class="my-region" value="" readonly="">';
        endif;

        if ($value['target'] <= 0 && $value['value'] > 0 && $value['reference'] != 1):
            $htmlRegion .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label">' . $i . '. ' . $value['name'] . '</label>
                         <input type="text" class="grand total left" value="' . number_format($value['value']) . '" readonly="">
                    </div>';
            $i++;
        elseif ($value['value'] > 0 && $value['reference'] != 1):
            $htmlRegion .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label"> ' . $i . '. ' . $value['name'] . '</label>
                        <input type="text" class="grand total left" value="' . number_format($value['value']) . '" readonly="">
                    </div>';
            $i++;
        endif;
    }
    /*
     * End Render Html for 'Manis by Region'
     */

    /*
     * list collection'
     */

    $argscollection = array(
        'blog_id' => $GLOBALS['blog_id'],
        'role' => '',
        'role' => 'Subscriber',
        'orderby' => 'ID',
        'order' => 'ASC',
        'offset' => '',
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'user_meta_leaderboard_list',
                'value' => '2',
                'compare' => '!=',
            ),
        ),
        'orderby' => 'login',
        'order' => 'ASC',
        'count_total' => false,
        'fields' => 'all',
    );
    $userscollection = get_users($argscollection);
    $total_user_collection = 0;
    $arrayUserCollection = array();
    foreach ($userscollection as $key => $user) {
        $personal_target_user = get_the_author_meta('user_meta_my_target', $user->ID);
        $my_center_target_user = get_the_author_meta('user_meta_center_filter', $user->ID);
        $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $user->user_login . "' AND b.meta_value='" . $my_center_target_user . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
        foreach ($manis_done as $key => $value) {
            $total_user_collection += get_post_meta($value->ID, 'wpcf-manis-done', true);
        }
        $total_user_collection += getStartingUser($user->user_login);
        if ($total_user_collection > 0):
            $class_active_user = ($current_user->user_login == $user->user_login) ? "active" : "";
            $arrayUserCollection[] = array('value' => $total_user_collection, 'name' => $user->display_name, 'target' => $personal_target_user, 'active' => $class_active_user, 'email' => $user->user_login, 'id' => $user->ID);
            $total_user_collection = 0;
        endif;
    }

    function cmp_collections($a, $b)
    {
        return $b["value"] - $a["value"];
    }

    uasort($arrayUserCollection, "cmp_collections");
    $i = 1;

    $htmlUserCollections = '';
    foreach ($arrayUserCollection as $key => $value) {
        if ($current_user->user_login == $value['email']) {
            $number_collections_on_top = $i;
        }
        $image = '';
        $getimage = get_user_meta($value['id'], 'user_avatar', TRUE);
        if ($getimage != '') {
            $image = ' <img src = "/wp-content/uploads/' . $getimage . '" class = "thumbnail img-responsive"/>';
        }


        $htmlUserCollections .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label">' . $i . '. ' . $image . $value['name'] . '</label>
                        <input type="text" class="grand total left" value="' . number_format($value['value']) . '" readonly="">
                    </div>';

        $i++;
    }

    /*
     * End Render Html for 'Group Collections'
     */
    if ($user_id) {
        echo json_encode(array(
            'msg' => 'send',
            'check' => 1,
            'err' => 0,
            'type' => 2,
            'total_all_center' => $total_all_center[0]->sum + getStartingAll(),
//            'total_all_center_target' => $total_target,
            'total_user' => $total_user + getStartingUser(),
//            'total_user_target' => $personal_target,
            'total_user_center' => $total_user_center,
//            'total_user_center_target' => $my_center_target,
            'htmlCenter' => $htmlCenter,
            'htmlPractic' => $htmlPractic,
            'htmlPracticTop' => $htmlPracticTop,
            'htmlCenterTop' => $htmlCenterTop,
            'htmlUser' => $htmlUser,
            'htmlUserTop' => $htmlUserTop,
//            'number_top_user' => $number_on_top,
            'htmlUserCollections' => $htmlUserCollections,
            'number_collections_on_top' => $number_collections_on_top,
            'number_top_center' => $number_center_top,
            'my_region_top' => $my_region_top,
            'my_region' => $my_region_user,
            'htmlRegion' => $htmlRegion
        ));
        exit;
    } else {
        echo json_encode(array(
            'msg' => 'send',
            'err' => 1,
        ));
        exit;
    }
}

/*
 * Accumulation_more
 * Get data if user change acc in page more
 *
 */

function Accumulation_more($post_value)
{
    $meta_data = get_post_meta($post_value['id']);
    $connect_target = unserialize($meta_data['connect-target'][0]);
    $name_connect_target = unserialize($meta_data['name-connect-target'][0]);


    global $wpdb;
    $total_all_center = $days_left = $total_user = $total_user_center = $my_center_target = 0;
    $total_target = $meta_data['wpcf-total-target'][0];
    $current_user = wp_get_current_user();
    if (0 != $current_user->ID) {
        $user_id = $current_user->ID;
    }
    /**
     * list Practice
     */
    if ($user_id) {
        /**
         * Get Total practice of User
         */
        $total_practice_of_user = 0;
        $arrayPractice = array();
        $get_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $user_id . " AND `meta_key`='user_meta_current_practice'");
        foreach ($get_practice as $value) {
            $data = unserialize($value->value);
            $name[] = $data['practices_name'];
        }

        $args = array(
            'numberposts' => -1,
            'post_type' => 'practic',
            'post_status' => 'private',
            'orderby' => 'name',
            'order' => 'ASC',
//            'connected_type' => 'practic_to_acc',
            'connected_items' => array($post_value['id'])
        );
        $postslist = get_posts($args);
        if (!$postslist):
            $args = array(
                'numberposts' => -1,
                'post_type' => 'practic',
                'post_status' => 'private',
                'orderby' => 'name',
                'order' => 'ASC',
            );
            $postslist = get_posts($args);
        endif;
        $i = 0;

        foreach ($postslist as $key => $value) {
            $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE  b.meta_value='" . $value->post_title . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private' AND b.meta_key='wpcf-my-practice'");

            foreach ($manis_done as $practice) {
                $date_manis = get_post_meta($practice->ID, 'wpcf-date-center', true);
                $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
                if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'][0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
                    $total_user_practice += get_post_meta($practice->ID, 'wpcf-manis-done', true);
                }
            }
            $meta_practice = getStartingPractices($value->post_title, $meta_data, $post_value['id']);
            $total_all_center += $total_user_practice;
            $total_user_practice += $meta_practice['sum'];
            if ($total_user_practice > 0 && $name) {
                if (in_array($value->post_title, $name)) {
                    $class_active = "active";
                }
                $arrayPractice[] = array('target' => $meta_practice['target'], 'value' => $total_user_practice, 'name' => $value->post_title, 'active' => $class_active, 'id' => $value->ID);
                $class_active = '';
                $total_user_practice = 0;
            }
        }

        function cmp2_acc($a, $b)
        {
            return $b["value"] - $a["value"];
        }

        uasort($arrayPractice, "cmp2_acc");
        $i = 1;
        $htmlPracticTop = $htmlPractic = '';
        foreach ($arrayPractice as $key => $value) {
            $check_target = array_search($value['id'], $connect_target);
            $target = ($check_target) ? $name_connect_target[$check_target] : 0;
            if ($target > 0) {
                if ($value['active'] == 'active') {
                    $htmlPracticTop .= '<label class="label">' . $value['name'] . ' (' . $i . ')' . '</label>
                        <input type="text" class="Centerflag" value="' . number_format($value['value']) . ' of ' . number_format($target) . '" readonly="">';
                }
                $htmlPractic .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label">' . $i . '. ' . $value['name'] . '</label>
                        <input type="text" class="grand total left" value="' . number_format($value['value']) . ' of ' . number_format($target) . '" readonly="">
                    </div>';
                $target = 0;
                $i++;
            } else {
                if ($value['active'] == 'active') {
                    $htmlPracticTop .= '<label class="label">' . $value['name'] . ' (' . $i . ')' . '</label>
                        <input type="text" class="Centerflag" value="' . number_format($value['value']) . '" readonly="">';
                }
                $htmlPractic .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label">' . $i . '. ' . $value['name'] . '</label>
                        <input type="text" class="grand total left" value="' . number_format($value['value']) . '" readonly="">
                    </div>';
                $i++;
            }


        }
    }
//    echo '11';
//    print_r($htmlPractic);
//    exit;
    /*
     *     list center
     */

    if ($user_id) {
        $personal_target = getTargetUser($current_user->user_login, $post_value['id']);
        $my_center_target_user = get_the_author_meta('user_meta_center_filter', $user_id);
    }
    $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $current_user->user_login . "' AND b.meta_value='" . $my_center_target_user . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
//    $total_all_center = $wpdb->get_results("SELECT SUM(meta_value)AS sum FROM " . $wpdb->prefix . "postmeta b INNER JOIN " . $wpdb->prefix . "posts a WHERE meta_key='wpcf-manis-done' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
    if ($total_all_center < 1):
        $args = array(
            'numberposts' => -1,
            'post_type' => 'manis-history',
            'post_status' => 'private',
            'orderby' => 'name',
            'order' => 'ASC',
        );
        $postslist = get_posts($args);
        foreach ($postslist as $key => $post) {
            $meta_post = get_post_meta($post->ID);
            $date_manis = get_post_meta($post->ID, 'wpcf-date-center', true);
            $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
            if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'][0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
                $total_all_center += $meta_post['wpcf-manis-done'][0];
            }
        }
    endif;
    foreach ($manis_done as $key => $value) {
        $date_manis = get_post_meta($value->ID, 'wpcf-date-center', true);
        $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
        if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'][0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
            $total_user += get_post_meta($value->ID, 'wpcf-manis-done', true);
        }
    }

    $args = array(
        'numberposts' => -1,
        'post_type' => 'list-center',
        'post_status' => 'private',
        'orderby' => 'name',
        'order' => 'ASC',
    );
    $postslist = get_posts($args);
    $htmlCenterTop = $htmlCenter = $htmlUser = $htmlRegion = '';
    $arrayCenter = $arrayUser = array();
    foreach ($postslist as $key => $value) {
        $center_name = $value->post_title;
        $Leaderboard = get_post_meta($value->ID, 'wpcf-leaderboard', true);
        $ontop = get_post_meta($value->ID, 'wpcf-on-top', true);
        $center_target = get_post_meta($value->ID, 'wpcf-center_target', true);
        $listID = $wpdb->get_results("SELECT post_id AS ID FROM " . $wpdb->prefix . "postmeta b INNER JOIN " . $wpdb->prefix . "posts a WHERE meta_value ='" . $center_name . "' AND meta_key='wpcf-my-center' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
        foreach ($listID as $key => $ID) {
            $date_manis = get_post_meta($ID->ID, 'wpcf-date-center', true);
            $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
            if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'][0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
                $total_list_center += get_post_meta($ID->ID, 'wpcf-manis-done', true);
            }
        }
        $total_list_center += getStartingCenter($center_name, $post_value['id']);
//        echo '<pre>';
//        print_r($total_list_center);
//        exit;
        if ($center_name == $my_center_target_user) {
            $my_center_target = $center_target;
            $total_user_center = $total_list_center;
            $center_by_user = $center_name;
        }
        if ($center_name == $my_center_target_user && $Leaderboard != 1) {
            $class_active = "active";
        }

        if ($total_list_center > 0 && $ontop == 1):
            $nameOnTop = (get_post_meta($value->ID, 'wpcf-name-on-top', true) != '') ? get_post_meta($value->ID, 'wpcf-name-on-top', true) : $center_name;
            if ($center_target > 0) {
                $htmlCenterTop .= '<label class="label">' . $nameOnTop . '</label>
                              <input type="text" class="Centerflag" value="' . number_format($total_list_center) . " of " . number_format($center_target) . '" readonly="">';
            } else {
                $htmlCenterTop .= '<label class="label">' . $nameOnTop . '</label>
                        <input type="text" class="Centerflag" value="' . number_format($total_list_center) . '" readonly="">';
            }
//            $total_list_center = 0;
        endif;
        if ($total_list_center > 0 && $Leaderboard != 1):
            $arrayCenter[] = array('value' => $total_list_center, 'name' => $center_name, 'target' => $center_target, 'active' => $class_active, 'Leaderboard' => $Leaderboard);
            $class_active = '';
        endif;
        $total_list_center = 0;
    }

    function cmp_acc($a, $b)
    {
        return $b["value"] - $a["value"];
    }

    uasort($arrayCenter, "cmp_acc");
    $i = 1;
    foreach ($arrayCenter as $key => $value) {
        if ($center_by_user == $value['name'] && $value['Leaderboard'] != 1) {
            $number_center_top = $i;
        }
        if ($value['target'] > 0) {
            $htmlCenter .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label">' . $i . '. ' . $value['name'] . '</label>
                        <input type="text" class="grand total left " value="' . number_format($value['value']) . " of " . number_format($value['target']) . '" readonly="">
                    </div>';
        } else {
            $htmlCenter .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label">' . $i . '. ' . $value['name'] . '</label>
                        <input type="text" class="grand total left" value="' . number_format($value['value']) . '" readonly="">
                    </div>';
        }
        $i++;
    }

    /*
     *   list user
     */
    $args = array(
        'blog_id' => $GLOBALS['blog_id'],
        'role' => '',
        'role' => 'Subscriber',
        'orderby' => 'ID',
        'order' => 'ASC',
        'offset' => '',
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'user_meta_leaderboard_list',
                'value' => '1',
                'compare' => '!=',
            )
        ),
        'orderby' => 'login',
        'order' => 'ASC',
        'count_total' => false,
        'fields' => 'all',
    );
    $blogusers = get_users($args);

    $total_user2 = 0;
    foreach ($blogusers as $key => $user) {
        $userOntop = get_the_author_meta('user_meta_ontop', $user->ID);
        $my_center_target_user = get_the_author_meta('user_meta_center_filter', $user->ID);
        $personal_target_user = get_the_author_meta('user_meta_my_target', $user->ID);
        $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $user->user_login . "' AND b.meta_value='" . $my_center_target_user . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
//echo ("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $user->user_login . "' AND b.meta_value='" . $my_center_target_user . "' AND a.`ID` = b.`post_id` ").'------';
        foreach ($manis_done as $key => $value) {
            $date_manis = get_post_meta($value->ID, 'wpcf-date-center', true);
            $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
            if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'][0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
                $total_user2 += get_post_meta($value->ID, 'wpcf-manis-done', true);
            }
        }
        $total_user2 += getStartingUser($user->user_login, $post_value['id']);
        if ($total_user2 > 0):
            $class_active_user = ($current_user->user_login == $user->user_login) ? "active" : "";
            $arrayUser[] = array('ontop' => $userOntop, 'value' => $total_user2, 'name' => $user->display_name, 'target' => $personal_target_user, 'active' => $class_active_user, 'email' => $user->user_login, 'id' => $user->ID);
            $total_user2 = 0;
        endif;
    }

    function cmp_user_acc($a, $b)
    {
        return $b["value"] - $a["value"];
    }

    uasort($arrayUser, "cmp_user_acc");
    $i = 1;
    $number_on_top = 0;
    $htmlUserTop = '';
    foreach ($arrayUser as $key => $value) {
        $image = '';
        $getimage = get_user_meta($value['id'], 'user_avatar', TRUE);
        if ($getimage != '') {
            $image = ' <img src = "/wp-content/uploads/' . $getimage . '" class = "thumbnail img-responsive"/>';
        }
        $current_user = wp_get_current_user();
        if (0 != $current_user->ID) {
            if ($current_user->user_login == $value['email']) {
                $number_on_top = $i;
            }
        }
        if ($value['ontop'] == 1) {
            if ($value['target'] > 0) {
                $htmlUserTop .= '<div class="group-box">
                            <label class="label">' . $value['name'] . '</label>
                            <input type="text" class="grand total left" value="' . number_format($value['value']) . " of " . number_format($value['target']) . '" readonly="">
                        </div>';
            } else {
                $htmlUserTop .= '<div class="group-box">
                                <label class="label">' . $value['name'] . '</label>
                                <input type="text" class="grand total left" value="' . number_format($value['value']) . '" readonly="">
                            </div>';
            }
        }
        if ($value['target'] > 0) {
            $total_target_only_user = getTargetUser($value['email'], $post_value['id']);
            $total_target_person = ($total_target_only_user ? " of " . number_format($total_target_only_user) : '');
            $htmlUser .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label">' . $i . '. ' . $image . $value['name'] . '</label>
                        <input type="text" class="grand total left" value="' . number_format($value['value']) . $total_target_person . '" readonly="">
                    </div>';
        } else {
            $htmlUser .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label"> ' . $i . '. ' . $image . $value['name'] . '</label>
                        <input type="text" class="grand total left" value="' . number_format($value['value']) . '" readonly="">
                    </div>';
        }
        $i++;
    }
    /*
     * list Region'
     */
    $my_center = $wpdb->get_results("SELECT b.`meta_value` AS center FROM " . $wpdb->prefix . "users a INNER JOIN " . $wpdb->prefix . "usermeta b WHERE a.`user_login`='" . $current_user->user_login . "' AND b.`meta_key`='user_meta_center_filter' AND b.`user_id`=" . $user_id);
    $getID = get_page_by_title($my_center[0]->center, OBJECT, 'list-center');
    $my_region = get_post_meta($getID->ID, 'my-region', true);
    $argsregion = array(
        'numberposts' => -1,
        'post_type' => 'region',
        'orderby' => 'name',
        'post_status' => 'private',
        'order' => 'ASC',
    );
    $postslistregion = get_posts($argsregion);
    foreach ($postslistregion as $key => $region) {
        $regionReference = get_post_meta($region->ID, 'wpcf-reference-region-only', TRUE);
        $regionOnTop = get_post_meta($region->ID, 'wpcf-region-on-top', TRUE);
        $regionNameOnTop = get_post_meta($region->ID, 'wpcf-top-name-region', TRUE);

        $target_region = get_post_meta($region->ID, 'region_target', TRUE);
        $args = array(
            'numberposts' => -1,
            'post_type' => 'list-center',
            'orderby' => 'name',
            'order' => 'ASC',
            'post_status' => 'private',
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => 'my-region',
                    'value' => $region->post_title,
                    'compare' => '=',
                ),
            )
        );
        $postslist = get_posts($args);
        $results = array();
        $startcount = 0;
        foreach ($postslist as $key => $simplePost) {
            $argruments = array(
                'numberposts' => -1,
                'post_type' => 'manis-history',
                'post_status' => 'private',
                'orderby' => 'name',
                'order' => 'ASC',
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => 'wpcf-my-center',
                        'value' => $simplePost->post_title,
                        'compare' => '=',
                    ),
                )
            );
            $historyPost = get_posts($argruments);
            $results[] = day_count_acc($historyPost, $meta_data);
            $startcount = getStartingCenter($simplePost->post_title, $post_value['id']);
        }
        $running_total_center = $day_count_center = $day_count_today = 0;
        foreach ($results as $simpleResult) {
            $running_total_center += $simpleResult['running_total_center'];
        }
        $running_total_center += $startcount;
        if ($region->post_title == $my_region) {
            $class_active_region = "active";
        }
        $arrayRegion[] = array('reference' => $regionReference, 'ontop' => $regionOnTop, 'nameOnTop' => $regionNameOnTop, 'value' => $running_total_center, 'name' => $region->post_title, 'target' => $target_region, 'active' => $class_active_region);
        $class_active_region = '';
        $running_total_center = 0;
    }

    function cmp_region_acc($a, $b)
    {
        return $b["value"] - $a["value"];
    }

    uasort($arrayRegion, "cmp_region_acc");
    $i = 1;
    $my_region_top = '';
    foreach ($arrayRegion as $key => $value) {
        $target = ($value['target'] > 0) ? ' of ' . number_format($value['target']) : '';
        if ($value['ontop'] == 1):
            $my_region_top .= '<label class="label mine">' . $value['nameOnTop'] . '</label><input type="text" class="my-region" value="' . number_format($value['value']) . $target . '"  readonly="">';
        endif;

        if ($value['name'] == $my_region && $value['value']):
            if ($value['reference'] == 0):
                $my_region_user .= '<label class="label mine">my Region (' . $i . ')</label><input type="text" class="my-region" value="' . number_format($value['value']) . $target . '" readonly="">';
            else:
                $my_region_user .= '<label class="label mine">my Region</label><input type="text" class="my-region" value="' . number_format($value['value']) . $target . '" readonly="">';
            endif;
//        elseif ($value['value']):
//            $my_region_user .= '<label class="label mine">my Region</label><input type="text" class="my-region" value="" readonly="">';
        endif;

        if ($value['target'] <= 0 && $value['value'] > 0 && $value['reference'] != 1):
            $htmlRegion .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label">' . $i . '. ' . $value['name'] . '</label>
                         <input type="text" class="grand total left" value="' . number_format($value['value']) . '" readonly="">
                    </div>';
            $i++;
        elseif ($value['value'] > 0 && $value['reference'] != 1):
            $htmlRegion .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label"> ' . $i . '. ' . $value['name'] . '</label>
                        <input type="text" class="grand total left" value="' . number_format($value['value']) . " of " . number_format($value['target']) . '" readonly="">
                    </div>';
            $i++;
        endif;
    }
    /*
     * End Render Html for 'Manis by Region'
     */

    /*
     * list collection'
     */

    $argscollection = array(
        'blog_id' => $GLOBALS['blog_id'],
        'role' => '',
        'role' => 'Subscriber',
        'orderby' => 'ID',
        'order' => 'ASC',
        'offset' => '',
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'user_meta_leaderboard_list',
                'value' => '2',
                'compare' => '!=',
            ),
        ),
        'orderby' => 'login',
        'order' => 'ASC',
        'count_total' => false,
        'fields' => 'all',
    );
    $userscollection = get_users($argscollection);
    $total_user_collection = 0;
    $arrayUserCollection = array();
    foreach ($userscollection as $key => $user) {
        $personal_target_user = get_the_author_meta('user_meta_my_target', $user->ID);
        $my_center_target_user = get_the_author_meta('user_meta_center_filter', $user->ID);
        $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $user->user_login . "' AND b.meta_value='" . $my_center_target_user . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
        foreach ($manis_done as $key => $value) {
            $date_manis = get_post_meta($value->ID, 'wpcf-date-center', true);
            $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
            if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'][0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
                $total_user_collection += get_post_meta($value->ID, 'wpcf-manis-done', true);
            }
        }
        $total_user_collection += getStartingUser($user->user_login);
        if ($total_user_collection > 0):
            $class_active_user = ($current_user->user_login == $user->user_login) ? "active" : "";
            $arrayUserCollection[] = array('value' => $total_user_collection, 'name' => $user->display_name, 'target' => $personal_target_user, 'active' => $class_active_user, 'email' => $user->user_login, 'id' => $user->ID);
            $total_user_collection = 0;
        endif;
    }

    function cmp_collections_acc($a, $b)
    {
        return $b["value"] - $a["value"];
    }

    uasort($arrayUserCollection, "cmp_collections_acc");
    $i = 1;

    $htmlUserCollections = '';
    foreach ($arrayUserCollection as $key => $value) {
        if ($current_user->user_login == $value['email']) {
            $number_collections_on_top = $i;
        }
        $image = '';
        $getimage = get_user_meta($value['id'], 'user_avatar', TRUE);
        if ($getimage != '') {
            $image = ' <img src = "/wp-content/uploads/' . $getimage . '" class = "thumbnail img-responsive"/>';
        }

        if ($value['target'] > 0) {
            $htmlUserCollections .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label">' . $i . '. ' . $image . $value['name'] . '</label>
                        <input type="text" class="grand total left" value="' . number_format($value['value']) . " of " . number_format($value['target']) . '" readonly="">
                    </div>';
        } else {
            $htmlUserCollections .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label">' . $i . '. ' . $image . $value['name'] . '</label>
                        <input type="text" class="grand total left" value="' . number_format($value['value']) . '" readonly="">
                    </div>';
        }
        $i++;
    }


    /*
     * End Render Html for 'Group Collections'
     */
    echo json_encode(array(
        'msg' => 'send',
        'check' => 1,
        'err' => 0,
        'type' => 2,
        'total_all_center' => $total_all_center + getStartingAll('', $post_value['id']),
        'total_all_center_target' => $total_target,
        'total_user' => $total_user + getStartingUser('', $post_value['id']),
        'total_user_target' => $personal_target,
        'total_user_center' => $total_user_center,
        'total_user_center_target' => $my_center_target,
        'htmlCenter' => $htmlCenter,
        'htmlPractic' => $htmlPractic,
        'htmlPracticTop' => $htmlPracticTop,
        'htmlCenterTop' => $htmlCenterTop,
        'htmlUser' => $htmlUser,
        'htmlUserTop' => $htmlUserTop,
        'number_top_user' => $number_on_top,
        'htmlUserCollections' => $htmlUserCollections,
        'number_collections_on_top' => $number_collections_on_top,
        'number_top_center' => $number_center_top,
        'my_region_top' => $my_region_top,
        'my_region' => $my_region_user,
        'htmlRegion' => $htmlRegion
    ));
    exit;
}

/*
 * Accumulation_more public
 * Get data if user change acc in page more
 *
 */

add_action('wp_ajax_Accumulation_more_public', 'Accumulation_more_public');
add_action('wp_ajax_nopriv_Accumulation_more_public', 'Accumulation_more_public');


function Accumulation_more_public($post_value)
{
    if ($_POST['id'] > 0) {
        $post_value = $_POST;
        $meta_data = get_post_meta($_POST['id']);
        $connect_target = unserialize($meta_data['connect-target'][0]);
        $name_connect_target = unserialize($meta_data['name-connect-target'][0]);


        global $wpdb;
        $total_all_center = $days_left = $total_user = $total_user_center = $my_center_target = 0;
        $total_target = $meta_data['wpcf-total-target'][0];


        /**
         * list Practice
         */

        /**
         * Get Total practice of User
         */
        $total_practice_of_user = 0;
        $arrayPractice = array();

        $args = array(
            'numberposts' => -1,
            'post_type' => 'practic',
            'post_status' => 'private',
            'orderby' => 'name',
            'order' => 'ASC',
        );
        $postslist = get_posts($args);

        $i = $total_user_practice = 0;

        foreach ($postslist as $key => $value) {
            $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE  b.meta_value='" . $value->post_title . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private' AND b.meta_key='wpcf-my-practice'");
            foreach ($manis_done as $practice) {
                $date_manis = get_post_meta($practice->ID, 'wpcf-date-center', true);
                $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
                if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'][0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
                    $total_user_practice += get_post_meta($practice->ID, 'wpcf-manis-done', true);
                }
            }
            $meta_practice = getStartingPractices($value->post_title, $meta_data, $_POST['id']);
            $total_all_center += $total_user_practice;
            $total_user_practice + $meta_practice['sum'];
            if ($total_user_practice > 0) {
                $class_active = "";
                $arrayPractice[] = array('target' => $meta_practice['target'], 'value' => $total_user_practice, 'name' => $value->post_title, 'active' => $class_active, 'id' => $value->ID);
                $class_active = '';
                $total_user_practice = 0;
            }
        }

        function cmp2_acc($a, $b)
        {
            return $b["value"] - $a["value"];
        }

        uasort($arrayPractice, "cmp2_acc");
        $i = 1;
        $htmlPracticTop = $htmlPractic = '';
        foreach ($arrayPractice as $key => $value) {
            $check_target = array_search($value['id'], $connect_target);
            $target = ($check_target) ? $name_connect_target[$check_target] : 0;
            if ($target > 0) {
                if ($value['active'] == 'active') {
                    $htmlPracticTop .= '<label class="label">' . $value['name'] . ' (' . $i . ')' . '</label>
                        <input type="text" class="Centerflag" value="' . number_format($value['value']) . ' of ' . number_format($target) . '" readonly="">';
                }
                $htmlPractic .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label">' . $i . '. ' . $value['name'] . '</label>
                        <input type="text" class="grand total left" value="' . number_format($value['value']) . ' of ' . number_format($target) . '" readonly="">
                    </div>';
                $target = 0;
                $i++;
            } else {
                if ($value['active'] == 'active') {
                    $htmlPracticTop .= '<label class="label">' . $value['name'] . ' (' . $i . ')' . '</label>
                        <input type="text" class="Centerflag" value="' . number_format($value['value']) . '" readonly="">';
                }
                $htmlPractic .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label">' . $i . '. ' . $value['name'] . '</label>
                        <input type="text" class="grand total left" value="' . number_format($value['value']) . '" readonly="">
                    </div>';
                $i++;
            }


        }

        /*
         * list Region'
         */

        $argsregion = array(
            'numberposts' => -1,
            'post_type' => 'region',
            'orderby' => 'name',
            'post_status' => 'private',
            'order' => 'ASC',
        );
        $postslistregion = get_posts($argsregion);

        foreach ($postslistregion as $key => $region) {
            $regionReference = get_post_meta($region->ID, 'wpcf-reference-region-only', TRUE);
            $regionOnTop = get_post_meta($region->ID, 'wpcf-region-on-top', TRUE);
            $regionNameOnTop = get_post_meta($region->ID, 'wpcf-top-name-region', TRUE);

            $target_region = get_post_meta($region->ID, 'region_target', TRUE);
            $args = array(
                'numberposts' => -1,
                'post_type' => 'list-center',
                'orderby' => 'name',
                'order' => 'ASC',
                'post_status' => 'private',
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => 'my-region',
                        'value' => $region->post_title,
                        'compare' => '=',
                    ),
                )
            );
            $postslist = get_posts($args);
            $results = array();
            $startcount = 0;
            foreach ($postslist as $key => $simplePost) {
                $argruments = array(
                    'numberposts' => -1,
                    'post_type' => 'manis-history',
                    'post_status' => 'private',
                    'orderby' => 'name',
                    'order' => 'ASC',
                    'meta_query' => array(
                        'relation' => 'AND',
                        array(
                            'key' => 'wpcf-my-center',
                            'value' => $simplePost->post_title,
                            'compare' => '=',
                        ),
                    )
                );
                $historyPost = get_posts($argruments);
                $results[] = day_count_acc($historyPost, $meta_data);
                $startcount = getStartingCenter($simplePost->post_title, $_POST['id']);
            }
            $running_total_center = $day_count_center = $day_count_today = 0;
            foreach ($results as $simpleResult) {
                $running_total_center += $simpleResult['running_total_center'];
            }
            $running_total_center += $startcount;

            $class_active_region = "";

            $arrayRegion[] = array('reference' => $regionReference, 'ontop' => $regionOnTop, 'nameOnTop' => $regionNameOnTop, 'value' => $running_total_center, 'name' => $region->post_title, 'target' => $target_region, 'active' => $class_active_region);
            $class_active_region = '';
            $running_total_center = 0;
        }

        function cmp_region_acc($a, $b)
        {
            return $b["value"] - $a["value"];
        }

        uasort($arrayRegion, "cmp_region_acc");
        $i = 1;
        $htmlRegion = $my_region_user = $my_region_top = '';
        foreach ($arrayRegion as $key => $value) {
            $target = ($value['target'] > 0) ? ' of ' . number_format($value['target']) : '';
            if ($value['ontop'] == 1):
                $my_region_top .= '<label class="label mine">' . $value['nameOnTop'] . '</label><input type="text" class="my-region" value="' . number_format($value['value']) . $target . '"  readonly="">';
            endif;


            $my_region_user .= '<label class="label mine">my Region</label><input type="text" class="my-region" value="' . number_format($value['value']) . $target . '" readonly="">';


            if ($value['target'] <= 0 && $value['value'] > 0 && $value['reference'] != 1):
                $htmlRegion .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label">' . $i . '. ' . $value['name'] . '</label>
                         <input type="text" class="grand total left" value="' . number_format($value['value']) . '" readonly="">
                    </div>';
                $i++;
            elseif ($value['value'] > 0 && $value['reference'] != 1):
                $htmlRegion .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label"> ' . $i . '. ' . $value['name'] . '</label>
                        <input type="text" class="grand total left" value="' . number_format($value['value']) . " of " . number_format($value['target']) . '" readonly="">
                    </div>';
                $i++;
            endif;
        }
        /*
         * End Render Html for 'Manis by Region'
         */

        echo json_encode(array(
            'msg' => 'send',
            'check' => 1,
            'err' => 0,
            'type' => 2,
            'total_all_center' => $total_all_center + getStartingAll('', $_POST['id']),
            'total_all_center_target' => $total_target,
            'total_user' => $total_user + getStartingUser('', $_POST['id']),
            'total_user_center' => $total_user_center,
            'total_user_center_target' => $my_center_target,
            'htmlPractic' => $htmlPractic,
            'htmlRegion' => $htmlRegion
        ));

        exit;
    } else {
        global $wpdb;
        $days_left = $total_user = $total_user_center = $my_center_target = 0;
        $total_target = get_option('total_target');

        /**
         * list Practice
         */

        /**
         * Get Total practice of User
         */
        $total_practice_of_user = 0;
        $arrayPractice = array();
        $total_all_center = $wpdb->get_results("SELECT SUM(meta_value)AS sum FROM " . $wpdb->prefix . "postmeta b INNER JOIN " . $wpdb->prefix . "posts a WHERE meta_key='wpcf-manis-done' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
//        echo '<pre>';
//        print_r($name);
//        exit;
        $args = array(
            'numberposts' => -1,
            'post_type' => 'practic',
            'post_status' => 'private',
            'orderby' => 'name',
            'order' => 'ASC',
        );
        $postslist = get_posts($args);
        $i = 0;
        foreach ($postslist as $key => $value) {
            $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE  b.meta_value='" . $value->post_title . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private' AND b.meta_key='wpcf-my-practice'");
            foreach ($manis_done as $practice) {
                $total_user_practice += get_post_meta($practice->ID, 'wpcf-manis-done', true);
            }
//            echo '<pre>';
//            print_r($total_user_practice);
////            exit;
            $meta_practice = getStartingPractices($value->post_title, $meta_data);
            $total_user_practice += $meta_practice['sum'];

            if ($total_user_practice > 0) {

                $class_active = "";

                $arrayPractice[] = array('target' => $meta_practice['target'], 'value' => $total_user_practice, 'name' => $value->post_title, 'active' => $class_active);
                $class_active = '';
                $total_user_practice = 0;
            }
        }

//        exit;

        function cmp2($a, $b)
        {
            return $b["value"] - $a["value"];
        }

        uasort($arrayPractice, "cmp2");
//        echo '<pre>';
//        print_r($arrayPractice);
//        exit;
        $i = 1;
        foreach ($arrayPractice as $key => $value) {

            $htmlPractic .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label">' . $i . '. ' . $value['name'] . '</label>
                        <input type="text" class="grand total left" value="' . number_format($value['value']) . '" readonly="">
                    </div>';
            $i++;
        }

        /**
         * Get practice value of practice
         */


        /**
         * End list Practice
         */

        /*
         * list Region'
         */


        $argsregion = array(
            'numberposts' => -1,
            'post_type' => 'region',
            'orderby' => 'name',
            'post_status' => 'private',
            'order' => 'ASC',
        );
        $postslistregion = get_posts($argsregion);
        foreach ($postslistregion as $key => $region) {
            $regionReference = get_post_meta($region->ID, 'wpcf-reference-region-only', TRUE);
            $regionOnTop = get_post_meta($region->ID, 'wpcf-region-on-top', TRUE);
            $regionNameOnTop = get_post_meta($region->ID, 'wpcf-top-name-region', TRUE);

            $target_region = get_post_meta($region->ID, 'region_target', TRUE);
            $args = array(
                'numberposts' => -1,
                'post_type' => 'list-center',
                'orderby' => 'name',
                'order' => 'ASC',
                'post_status' => 'private',
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => 'my-region',
                        'value' => $region->post_title,
                        'compare' => '=',
                    ),
                )
            );
            $postslist = get_posts($args);
            $results = array();
            $startcount = 0;
            foreach ($postslist as $key => $simplePost) {
                $argruments = array(
                    'numberposts' => -1,
                    'post_type' => 'manis-history',
                    'post_status' => 'private',
                    'orderby' => 'name',
                    'order' => 'ASC',
                    'meta_query' => array(
                        'relation' => 'AND',
                        array(
                            'key' => 'wpcf-my-center',
                            'value' => $simplePost->post_title,
                            'compare' => '=',
                        ),
                    )
                );
                $historyPost = get_posts($argruments);
                $results[] = day_count($historyPost);
                $startcount = getStartingCenter($simplePost->post_title);
            }
//        echo '<pre>';
//        print_r($startcount);
////        exit;
            $running_total_center = $day_count_center = $day_count_today = 0;
            foreach ($results as $simpleResult) {
                $running_total_center += $simpleResult['running_total_center'];
            }
            $running_total_center += $startcount;

            $class_active_region = "";

            $arrayRegion[] = array('reference' => $regionReference, 'ontop' => $regionOnTop, 'nameOnTop' => $regionNameOnTop, 'value' => $running_total_center, 'name' => $region->post_title, 'target' => $target_region, 'active' => $class_active_region);
            $class_active_region = '';
            $running_total_center = 0;
        }

//    exit;

        function cmp_region($a, $b)
        {
            return $b["value"] - $a["value"];
        }

        uasort($arrayRegion, "cmp_region");
        $i = 1;
        $my_region_top = '';
        foreach ($arrayRegion as $key => $value) {


            if ($value['value']):
                if ($value['reference'] == 0):
                    $my_region_user .= '<label class="label mine">my Region (' . $i . ')</label><input type="text" class="my-region" value="' . number_format($value['value']) . '" readonly="">';
                else:
                    $my_region_user .= '<label class="label mine">my Region</label><input type="text" class="my-region" value="' . number_format($value['value']) . '" readonly="">';
                endif;
//        elseif ($value['value']):
//            $my_region_user .= '<label class="label mine">my Region</label><input type="text" class="my-region" value="" readonly="">';
            endif;

            if ($value['target'] <= 0 && $value['value'] > 0 && $value['reference'] != 1):
                $htmlRegion .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label">' . $i . '. ' . $value['name'] . '</label>
                         <input type="text" class="grand total left" value="' . number_format($value['value']) . '" readonly="">
                    </div>';
                $i++;
            elseif ($value['value'] > 0 && $value['reference'] != 1):
                $htmlRegion .= '<div class="group-box ' . $value['active'] . '">
                        <label class="label"> ' . $i . '. ' . $value['name'] . '</label>
                        <input type="text" class="grand total left" value="' . number_format($value['value']) . '" readonly="">
                    </div>';
                $i++;
            endif;
        }
        /*
         * End Render Html for 'Manis by Region'
         */


        /*
         * End Render Html for 'Group Collections'
         */

        echo json_encode(array(
            'msg' => 'send',
            'check' => 1,
            'err' => 0,
            'type' => 2,
            'total_all_center' => $total_all_center[0]->sum + getStartingAll(),
            'total_user' => $total_user + getStartingUser(),
            'total_user_center' => $total_user_center,
            'htmlPractic' => $htmlPractic,
            'htmlRegion' => $htmlRegion
        ));
        exit;


    }


}

function day_count($postslist)
{
    $running_total_center = $day_count_center = $day_count_today = 0;
    foreach ($postslist as $key => $value) {
        $meta_post = get_post_meta($value->ID);
        $running_total_center += $meta_post['wpcf-manis-done'][0];

        if (strtotime(date('d-m-Y')) > $meta_post['wpcf-date-center'][0]) {
            $day_count_center += $meta_post['wpcf-manis-done'][0];
        } else {
            $day_count_today += $meta_post['wpcf-manis-done'][0];
        }
    }
    $result = array(
        'day_count_center' => $day_count_center,
        'running_total_center' => $running_total_center,
        'day_count_today' => $day_count_today
    );

    return $result;
}

function day_count_acc($postslist, $meta_data)
{
    $running_total_center = $day_count_center = $day_count_today = 0;
    foreach ($postslist as $key => $value) {
        $meta_post = get_post_meta($value->ID);
        $date_manis = get_post_meta($value->ID, 'wpcf-date-center', true);
        $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
        if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'][0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
            $running_total_center += $meta_post['wpcf-manis-done'][0];
            if (strtotime(date('d-m-Y')) > $meta_post['wpcf-date-center'][0]) {
                $day_count_center += $meta_post['wpcf-manis-done'][0];
            } else {
                $day_count_today += $meta_post['wpcf-manis-done'][0];
            }
        }
    }
    $result = array(
        'day_count_center' => $day_count_center,
        'running_total_center' => $running_total_center,
        'day_count_today' => $day_count_today
    );

    return $result;
}
