<?php

/**
 * Created by PhpStorm.
 * User: ANH
 * Date: 4/28/2016
 * Time: 9:15 AM
 */
/*
 * addCenter
 * add center page count
 * neu user ton tai thi add center, chua thi tao user va add center
 */
add_action('wp_ajax_addCenter', 'addCenter');
add_action('wp_ajax_nopriv_addCenter', 'addCenter');

function addCenter()
{
    global $wpdb;
    $manisdone = preg_replace('/[^a-zA-Z0-9\-_]/', '', $_POST['manisdone']);
//    $post = get_page_by_title($_POST['center_select'], OBJECT, 'list-center');
//    $postMeta = get_post_meta($post->ID, 'wpcf-count', true);
//    $sum_all_center = $postMeta + $manisdone;

    $results = $wpdb->get_results("SELECT COUNT(user_login) AS user FROM " . $wpdb->prefix . "users WHERE user_login ='" . $_POST['email'] . "'");
    if ($results[0]->user > 0) {
        $user = get_user_by('email', $_POST['email']);
        $user_info = get_userdata($user_id);
//        $meta = get_the_author_meta('user_meta_center', $user->ID);
//        update_post_meta($post->ID, 'wpcf-count', $sum_all_center);
        $post_id = wp_insert_post(array(
//            'post_author' => $user_info->display_name,
            'post_title' => $_POST['email'],
            'post_type' => 'manis-history',
            'post_status' => 'private',
        ));
        if ($_POST['center_select'] && $_POST['center_select'] != 'add') {
            $wpdb->get_results('UPDATE ' . $wpdb->prefix . 'posts a INNER JOIN ' . $wpdb->prefix . 'postmeta b'
                . ' SET b.`meta_value`="' . $_POST['center_select'] . '" '
                . 'WHERE a.`post_title`="' . $_POST['email'] . '" AND a.`post_status`="private" AND a.`ID`=b.`post_id` AND b.`meta_key`="wpcf-my-center"');
            update_user_meta($user->ID, 'user_meta_center_filter', $_POST['center_select']);
        }
        add_post_meta($post_id, 'wpcf-my-center', $_POST['center_select']);
        add_post_meta($post_id, 'wpcf-my-practice', $_POST['practice_filter']);
        add_post_meta($post_id, 'wpcf-manis-done', $manisdone);
        add_post_meta($post_id, 'wpcf-date-center', strtotime(date('Y-m-d H:i')));
        sendMail($_POST['email'], $_POST['center_select'], $_POST['practice_filter'], $manisdone, 1);
        echo json_encode(array(
            'msg' => (get_option('add_center') ? get_option('add_center') : ""),
            'err' => 0,
        ));
        exit;
    } else {
        $user_id = username_exists($_POST['email']);
        if (!$user_id and email_exists($_POST['email']) == false) {
            $user_id = wp_create_user($_POST['email'], 123456, $_POST['email']);
            $fname = explode('@', $_POST['email']);
            $api = mc4wp_get_api();
            $merge_vars = array(
                'FNAME' => $fname[0],
                'LNAME' => ''
            );
//            $api->subscribe('10b1af0d94', $_POST['email'], $merge_vars);
            $api->subscribe('3aa030d659', $_POST['email'], $merge_vars, 'html', true);
            //$api->unsubscribe('e93b3ce6c8', $_POST['email']);
            $data = array(
                'practices_name' => 'Chenrezig',
                'practice_description' => '',
                'starting_count' => 0,
                'target' => 0
            );
            update_user_meta($user_id, 'user_meta_leaderboard_list', 2);
            update_user_meta($user_id, 'user_meta_center_filter', $_POST['center_select']);
            update_user_meta($user_id, 'user_meta_current_practice', $data);
            wp_update_user(array('ID' => $user_id, 'display_name' => $fname[0]));
        }
//        update_post_meta($post->ID, 'wpcf-count', $sum_all_center);
        $post_id = wp_insert_post(array(
//            'post_author' => $fname[0],
            'post_title' => $_POST['email'],
            'post_type' => 'manis-history',
            'post_status' => 'private',
        ));
        add_post_meta($post_id, 'wpcf-my-center', $_POST['center_select']);
        add_post_meta($post_id, 'wpcf-my-practice', $_POST['practice_filter']);
        add_post_meta($post_id, 'wpcf-manis-done', $manisdone);
        add_post_meta($post_id, 'wpcf-date-center', strtotime(date('d-m-Y H:i:s')));

        sendMail($_POST['email'], $_POST['center_select'], $_POST['practice_filter'], $manisdone, 2);
        echo json_encode(array(
            'msg' => (get_option('add_user_center') ? get_option('add_user_center') : ""),
            'err' => 0,
        ));
        exit;
    }
    exit;
}

/*
 * getAutoCheck
 * Get data khi vao page count
 */
add_action('wp_ajax_getAutoCheck', 'getAutoCheck');
add_action('wp_ajax_nopriv_getAutoCheck', 'getAutoCheck');

function getAutoCheck()
{
//    echo '<pre>';
//    print_r(111);
//    print_r($_POST);
//    exit;
    $current_practice = (isset($_POST['practice']) and $_POST['practice']) ? $_POST['practice'] : '';
    global $wpdb;
//    $user_id = username_exists($_POST['email']);
    $current_user = wp_get_current_user();
    if (0 != $current_user->ID) {
        $user_id = $current_user->ID;
    }
    $data_id = check_current_accumulations();
    $meta_data = get_post_meta($data_id);
    $center_select = '';
    $today = strtotime(date('d-m-Y'));

    if ($user_id) {
        $center_select = get_the_author_meta('user_meta_center_filter', $user_id);
        $personal_target = $personal_starting_count = 0;
        $practice_html = get_all_practice_html_by_user($user_id, $current_practice);
        $get_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $user_id . " AND `meta_key`='user_meta_current_practice'");
        $html_practice = '';
        foreach ($get_practice as $key => $value) {
            $name[] = unserialize($value->value);
        }
        $name = array_sort($name, 'practices_name', SORT_ASC);
        foreach ($name as $key => $value) {
            if (!$current_practice OR ($current_practice and $value['practices_name'] == $current_practice)) {
                $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $current_user->user_login . "' AND b.meta_value='" . $value ['practices_name'] . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
                foreach ($manis_done as $practice) {
                    $date_manis = get_post_meta($practice->ID, 'wpcf-date-center', true);
                    $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
                    if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'] [0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
                        $total_user_practice += get_post_meta($practice->ID, 'wpcf-manis-done', true);
                    }
                }
                $getTarget = $value['date_target_' . $data_id];
                $personal_target += $getTarget;
                $getStart = $value ['starting_count_' . $data_id];
                $personal_starting_count += $getStart;
                $target = ($getTarget > 0) ? " of " . number_format($getTarget) : "";
                $html_practice .= '<div class="group-box col-md-12">
            <label class="label">my ' . $value ['practices_name'] . '</label>
            <input type="text" class="my_total" value="' . number_format($total_user_practice + sum_all_starting_count($value)) . $target . '" readonly>
        </div>';
                $total_user_practice = 0;
                $target = '';
                $array_practices_name[] = $value['practices_name'];
            }
        }
    } else {
        $current_practice = (isset($_COOKIE['current_practice']) ? $_COOKIE['current_practice'] : '');
        $practice_html = get_all_practice_html_by_user(0, $current_practice);
    }

    $remaining_daily = $days_left = $total_user = $total_user_center = 0;
    $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $current_user->user_login . "' AND b.meta_value='" . $center_select . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
    $manis_done_center = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE b.meta_value='" . $center_select . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
    foreach ($manis_done as $key => $value) {
        $date_manis = get_post_meta($value->ID, 'wpcf-date-center', true);
        $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
        if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'] [0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
            $total_user += get_post_meta($value->ID, 'wpcf-manis-done', true);
        }
    }

    foreach ($manis_done_center as $key => $value) {
        $date_manis = get_post_meta($value->ID, 'wpcf-date-center', true);
        $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
        if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'] [0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
            $total_user_center += get_post_meta($value->ID, 'wpcf-manis-done', true);
        }
    }

    $args = array(
        'numberposts' => -1,
        'post_type' => 'manis-history',
        'post_status' => 'private',
        'orderby' => 'name',
        'order' => 'ASC',
    );
    $postslist = get_posts($args); // data lay tu history
    $id_post = $running_total_yesterday = $day_count_today = $total_all_center = $total_counted = 0;
    // - daily_target (on all levels, this value is recalculated each day): ROUNDUP((total_target-running_total_yesterday)/days_left)
    // - running_total_yesterday = counted trong qua khu + counted toi thoi diem hien tai
    // - day_count_today: counted trong hom nay
    foreach ($postslist as $key => $post) { // tinh toan dua vao du lieu manis histories
        $meta_post = get_post_meta($post->ID);
        $total_all_center += $meta_post['wpcf-manis-done'][0];

        if ($post->post_title == $current_user->user_login) { // se bi sai neu nguoi dung thay doi email !?
            $date_manis = get_post_meta($post->ID, 'wpcf-date-center', true); // cu tung record history 
            $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
            // tinh toan tong counted tu dau cho den thoi diem hien tai (neu hom nay co counted)
            if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'] [0] && $date_manis_interval <= $meta_data ['wpcf-acc-end-date'][0] && $today > $date_manis_interval && in_array($meta_post['wpcf-my-practice'][0], $array_practices_name)) {
                $running_total_yesterday += $meta_post['wpcf-manis-done'][0];
            } elseif ($date_manis_interval >= $meta_data['wpcf-acc-start-date'] [0] && $date_manis_interval <= $meta_data ['wpcf-acc-end-date'][0] && in_array($meta_post['wpcf-my-practice'][0], $array_practices_name)) {
                $day_count_today += $meta_post['wpcf-manis-done'][0];
            }
        }
    }

    $getpost = get_page_by_title($center_select, OBJECT, 'list-center');
    $center_target = get_post_meta($getpost->ID, 'wpcf-center_target', true);
    $total_target = $meta_data['wpcf-total-target'][0];
    $start_date = $meta_data['wpcf-acc-start-date'][0];
    $end_date = $meta_data['wpcf-acc-end-date'][0];

    if ($today >= $start_date and $today <= $end_date) {
        $datediff = abs($today - $end_date);
        $days_left = ($datediff / (60 * 60 * 24)) + 1;
        $daily_target = round(($personal_target - ($running_total_yesterday + $personal_starting_count)) / $days_left);
        $left_today = $daily_target - $day_count_today;
        $remaining_daily = round((($personal_target - ($personal_starting_count + $running_total_yesterday + $day_count_today)) / ($days_left - 1)), 0);
        $show_remaining_daily = (($days_left - 1) >= 0 and $current_practice) ? 1 : 0;
//        echo '$personal_target: '.$personal_target.'---';
//        echo '$running_total_yesterday: '.$running_total_yesterday.'---';
//        echo '$days_left: '.$days_left.'---';
//        echo '$daily_target: '.$daily_target.'---';
//        echo '$day_count_today: '.$day_count_today.'---';
    }
//    echo getStartingCenter();
//    exit;
    $show_days_left = ($personal_target > 0 and $current_practice) ? 1 : 0;
    if ($user_id && $center_select) {
        echo json_encode(array(
            'msg' => 'send',
            'err' => 0,
            'type' => 1,
            'total_center' => $total_user_center + getStartingCenter(),
//
            'total_center_target' => get_post_meta($id_post, 'wpcf-center_target', true),
            'total_center_target' => $center_target,
            'total_all_center' => $total_all_center + getStartingAll(),
            'total_all_center_target' => $total_target,
            'name' => $post->post_title,
            'total_user' => $total_user + getStartingUser(),
            'total_user_target' => $personal_target,
            'left_today' => $left_today,
            'daily_target' => $daily_target,
            'running_total_yesterday' => $running_total_yesterday,
            'personal_starting_count' => $personal_starting_count,
            'center_select' => $center_select,
            'practice_select' => $practice_html,
            'id' => $post->ID,
            'day_count_today' => $day_count_today,
            'days_left' => $days_left,
            'email' => $current_user->user_login,
            'show_days_left' => $show_days_left,
            'remaining_daily' => $remaining_daily,
            'show_remaining_daily' => $show_remaining_daily,
            'html_practice' => $html_practice
        ));
        exit;
    } else {
        echo json_encode(array(
            'msg' => 'send',
            'err' => 1,
            'practice_select' => $practice_html,
            'left_today' => $left_today,
            'daily_target' => $daily_target,
            'center_select' => $center_select,
            'show_days_left' => $show_days_left,
        ));
        exit;
    }
}

/*
get all count
*/
add_action('wp_ajax_getAllCount', 'getAllCount');
add_action('wp_ajax_nopriv_getAllCount', 'getAllCount');
function getAllCount()
{
//    echo '<pre>';
//    print_r(222);
//    print_r($_POST);
//    exit;
    $current_practice = (isset($_POST['practice']) and $_POST['practice']) ? $_POST['practice'] : '';

    global $wpdb;
//    $user_id = username_exists($_POST['email']);
    $current_user = wp_get_current_user();
    if (0 != $current_user->ID) {
        $user_id = $current_user->ID;
    }

    $data_id = check_current_accumulations();

    $meta_data = get_post_meta($data_id);
    $center_select = '';
    $today = strtotime(date('d-m-Y'));

    if ($user_id) {
        $center_select = get_the_author_meta('user_meta_center_filter', $user_id);
        $personal_target = $personal_starting_count = 0;
        $practice_html = get_all_practice_html_by_user($user_id, $current_practice);

        $get_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $user_id . " AND `meta_key`='user_meta_current_practice'");
        $html_practice = '';
        foreach ($get_practice as $key => $value) {
            $name[] = unserialize($value->value);
        }

        $name = array_sort($name, 'practices_name', SORT_ASC);

        foreach ($name as $key => $value) {

            $total_user_practice = 0;
            if (!$current_practice OR ($current_practice and $value['practices_name'] == $current_practice)) {
                $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $current_user->user_login . "' AND b.meta_value='" . $value ['practices_name'] . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");

                foreach ($manis_done as $practice) {
                    $date_manis = get_post_meta($practice->ID, 'wpcf-date-center', true);
                    $date_manis_interval = strtotime(date('Y-m-d', $date_manis));

//                    if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'][0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
                    $total_user_practice += get_post_meta($practice->ID, 'wpcf-manis-done', true);
//                    }
                }
                $getTarget = $value['date_target_' . $data_id];
                $personal_target += $getTarget;
                $getStart = sumAllStartingCount($value);
                $personal_starting_count += $getStart;
                $target = ($value['target_practice_all'] > 0) ? " of " . number_format($value['target_practice_all']) : "";
                $html_practice .= '<div class="group-box col-md-12">
            <label class="label">my ' . $value ['practices_name'] . '</label>
            <input type="text" class="my_total" value="' . number_format($total_user_practice + get_starting_count($value['practices_name'])) . $target . '" readonly>
        </div>';
                if ($value['practices_name'] == $_POST['practice_filter']) {
                    $getTargetAll = $value['target_practice_all'];
                    $total_all = $getTargetAll - ($total_user_practice + get_starting_count($value['practices_name']));
                }
                $total_user_practice = 0;
                $target = '';
                $array_practices_name[] = $value['practices_name'];
            }
        }
    } else {
        $current_practice = (isset($_COOKIE['current_practice']) ? $_COOKIE['current_practice'] : '');
        $practice_html = get_all_practice_html_by_user(0, $current_practice);
    }

    $remaining_daily = $days_left = $total_user = $total_user_center = 0;
    $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $current_user->user_login . "' AND b.meta_value='" . $center_select . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
    $manis_done_center = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE b.meta_value='" . $center_select . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
    foreach ($manis_done as $key => $value) {
        $date_manis = get_post_meta($value->ID, 'wpcf-date-center', true);
        $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
//        if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'] [0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
        $total_user += get_post_meta($value->ID, 'wpcf-manis-done', true);
//        }
    }

    foreach ($manis_done_center as $key => $value) {
        $date_manis = get_post_meta($value->ID, 'wpcf-date-center', true);
        $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
//        if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'] [0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
        $total_user_center += get_post_meta($value->ID, 'wpcf-manis-done', true);
//        }
    }

    $args = array(
        'numberposts' => -1,
        'post_type' => 'manis-history',
        'post_status' => 'private',
        'orderby' => 'name',
        'order' => 'ASC',
    );
    $postslist = get_posts($args); // data lay tu history
    $id_post = $running_total_yesterday = $day_count_today = $total_all_center = $total_counted = 0;
    // - daily_target (on all levels, this value is recalculated each day): ROUNDUP((total_target-running_total_yesterday)/days_left)
    // - running_total_yesterday = counted trong qua khu + counted toi thoi diem hien tai
    // - day_count_today: counted trong hom nay
    foreach ($postslist as $key => $post) { // tinh toan dua vao du lieu manis histories
        $meta_post = get_post_meta($post->ID);
        $total_all_center += $meta_post['wpcf-manis-done'][0];

        if ($post->post_title == $current_user->user_login) { // se bi sai neu nguoi dung thay doi email !?
            $date_manis = get_post_meta($post->ID, 'wpcf-date-center', true); // cu tung record history
            $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
            // tinh toan tong counted tu dau cho den thoi diem hien tai (neu hom nay co counted)
//            if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'] [0] && $date_manis_interval <= $meta_data ['wpcf-acc-end-date'][0] && $today > $date_manis_interval && in_array($meta_post['wpcf-my-practice'][0], $array_practices_name)) {
            $running_total_yesterday += $meta_post['wpcf-manis-done'][0];
//            } elseif ($date_manis_interval >= $meta_data['wpcf-acc-start-date'] [0] && $date_manis_interval <= $meta_data ['wpcf-acc-end-date'][0] && in_array($meta_post['wpcf-my-practice'][0], $array_practices_name)) {
            $day_count_today += $meta_post['wpcf-manis-done'][0];
//            }
        }
    }

    $getpost = get_page_by_title($center_select, OBJECT, 'list-center');
    $center_target = get_post_meta($getpost->ID, 'wpcf-center_target', true);
    $total_target = $meta_data['wpcf-total-target'][0];
    $start_date = $meta_data['wpcf-acc-start-date'][0];
    $end_date = $meta_data['wpcf-acc-end-date'][0];

    if ($today >= $start_date and $today <= $end_date) {
        $datediff = abs($today - $end_date);
        $days_left = ($datediff / (60 * 60 * 24)) + 1;
        $daily_target = round(($personal_target - ($running_total_yesterday + $personal_starting_count)) / $days_left);
        $left_today = $daily_target - $day_count_today;
        $remaining_daily = round((($personal_target - ($personal_starting_count + $running_total_yesterday + $day_count_today)) / ($days_left - 1)), 0);
        $show_remaining_daily = (($days_left - 1) >= 0 and $current_practice) ? 1 : 0;
//        echo '$personal_target: '.$personal_target.'---';
//        echo '$running_total_yesterday: '.$running_total_yesterday.'---';
//        echo '$days_left: '.$days_left.'---';
//        echo '$daily_target: '.$daily_target.'---';
//        echo '$day_count_today: '.$day_count_today.'---';
    }
//    echo getStartingCenter();
//    exit;
    $show_days_left = ($personal_target > 0 and $current_practice) ? 1 : 0;
    if ($user_id && $center_select) {
        echo json_encode(array(
            'msg' => 'send',
            'err' => 0,
            'type' => 1,
            'total_center' => $total_user_center + getStartingCenter(),
            'total_center_target' => get_post_meta($id_post, 'wpcf-center_target', true),
//            'total_center_target' => $center_target,
            'total_all_center' => $total_all_center + getStartingAll(),
//            'total_all_center_target' => $total_target,
            'name' => $post->post_title,
            'total_user' => $total_user + getStartingUser(),
//            'total_user_target' => $personal_target,
            'left_today' => $left_today,
            'daily_target' => $daily_target,
            'running_total_yesterday' => $running_total_yesterday,
            'personal_starting_count' => $personal_starting_count,
            'center_select' => $center_select,
            'practice_select' => $practice_html,
            'id' => $post->ID,
            'day_count_today' => $day_count_today,
            'days_left' => $days_left,
            'email' => $current_user->user_login,
            'show_days_left' => $show_days_left,
            'remaining_daily' => $remaining_daily,
            'show_remaining_daily' => $show_remaining_daily,
            'html_practice' => $html_practice,
            'targetAll' => $getTargetAll,
            'total_all' => $total_all,
            'show_total_all' => 1,
        ));
        exit;
    } else {
        echo json_encode(array(
            'msg' => 'send',
            'err' => 1,
            'practice_select' => $practice_html,
            'left_today' => $left_today,
            'daily_target' => $daily_target,
            'center_select' => $center_select,
            'show_days_left' => $show_days_left,
        ));
        exit;
    }
}

/*
 * center_func
 * add shortcode page count
 */

function center_func()
{
//    $value = ($_COOKIE['email_center']) ? $_COOKIE['email_center'] : '';
    $current_user = wp_get_current_user();
    if (0 != $current_user->ID) {
        $userId = $current_user->ID;
    }
    $my_center_target_user = get_the_author_meta('user_meta_center_filter', $userId);
    $value = ($current_user->user_login) ? $current_user->user_login : '';

    $current_practice = (isset($_COOKIE ['current_practice_' . $userId]) ? $_COOKIE ['current_practice_' . $userId] : '');
    //<p class="notice"></p>
    $html = "<div class='box-count-home'>";
    $html .= '<form id="formSendCenter" class="formSendCenter" name="formSendCenter" method="POST" >
        <input type="hidden" name="email" id="email" class="email" value="' . $value . '" placeholder="E-mail" readonly/>';
    $html .= '<label class="label hide">' . __('my Center', 'Divi') . '</label>' . get_all_center_html($my_center_target_user, 1, 1, 'hide');
    $html .= '<div class="add_new_center display">' . formAddCenter() . '</div>';
    $html .= '<label class="label">' . __('my Practice to count', 'Divi') . '</label>' . get_all_practice_html_by_user(1, $current_practice);
    $html .= '<div class="box-change-count"><div class="left"><div class="show-count"><label class="label"  for="manis-done">' . __('Mantras done', 'Divi') . '</label>
        <input type="text" id="manis-done" name="manis-done" class="manis-done" value="" placeholder="Mantras Done"/></div>
          <div class="right custom hide">
        <label class="btn-live button">
        <a class="count-live" href="javascript:void(0)">' . __('Count Live', 'Divi') . '</a>
        </label>
</div>
        <script type="text/javascript">$(".manis-done").maskMoney();</script>';
    $html .= ' <div class="remaining_box display">
      <label class="label">' . __('Remaining', 'Divi') . '</label>
      <input type="text" readonly id="remaining" name="remaining" class="remaining" value=""/>
      </div>';
    $html .= ' <div class="left_today_box display">
      <label class="label">' . __('Left today', 'Divi') . '</label>
      <input type="text" readonly id="left_today" name="left_today" class="left_today" value=""/>
      </div>';
    $html .= '<div class="remaining_daily_box display">
            <label class="label">' . __('Remaining daily', 'Divi') . '</label>
            <input type="text" readonly id="remaining_daily" name="remaining_daily" class="remaining_daily" value=""/>
            <a class="button" href="/profile/?tab=practices">No Target Yet - Set Target</a>
        </div>
         <div class="left_today_all_box w50 display">
      <label class="label">' . __('Left today all', 'Divi') . '</label>
      <input type="text" readonly id="left_today_all" name="left_today_all" class="left_today_all" value=""/>
      </div>
      <div class="remaining_daily_all_box w50 fr display">
            <label class="label">' . __('Remaining daily all', 'Divi') . '</label>
            <input type="text" readonly id="remaining_daily_all" name="remaining_daily_all" class="remaining_daily_all" value=""/>
        </div>
        </div>
        <div class="right">
        <label class="btn-live button">
        <a class="count-live" href="javascript:void(0)">' . __('Count Live', 'Divi') . '</a>
        <a class="done-count-live hide" href="javascript:void(0)">' . __('Finish live counting', 'Divi') . '</a>
        </label>
</div>
        </div>
       
        <input type="hidden" name="id" class="id" value=""/>
        <input type="hidden" name="total_user" class="total_user" value=""/>
        <input type="hidden" name="total_center" class="total_center" value=""/>
        <input type="hidden" name="personal_target" class="personal_target" value=""/>
        <input type="hidden" name="personal_starting_count" class="personal_starting_count" value=""/>
        <input type="hidden" name="running_total_yesterday" class="running_total_yesterday" value=""/>
        <input type="hidden" name="days_left" class="days_left" value=""/>
        <input type="hidden" name="day_count_today" class="day_count_today" value=""/>
        <input type="hidden" name="left_today_2" class="left_today_2" value=""/>
        <input type="hidden" name="check_count" class="check_count" value="input"/>
        <input type="hidden" name="old_data" class="old_data" value=""/>
        <input type="hidden" name="remaining_all" class="remaining_all" value=""/>
        <input type="hidden" name="left_today_all_2" class="left_today_all_2" value=""/>
        <input type="hidden" name="remaining_daily_all_2" class="remaining_daily_all_2" value=""/>
        <div class="submit center">
            <a class="button" href="javascript:;" onclick="jQuery(\'#formSendCenter\').submit();">' . __('Count!', 'Divi') . '</a>
        </div>
    </form>
    <div class="group-box accumulations_select">
            <label class="label">' . __('Accumulation', 'Divi') . '</label>
            ' . get_all_accumulations_html(1) . '
    </div>
    <div class="box-count-live hide">
        <div class="text-center">
        <style>
.btn-count-big.type1-count0 {
    background: white;
    color: #1E278F;
    border: 1px solid #1E278F;
}

.btn-count-big.type1-count1 {
    background: #E82928;
    color: white;
    border: 1px solid #E82928;
}

.btn-count-big.type1-count2 {
    background: #37772D;
    color: white;
    border: 1px solid #37772D;
}

.btn-count-big.type1-count3 {
    background: #E7D135;
    color: #1E278F;
    border: 1px solid #E7D135;
}

.btn-count-big.type1-count4 {
    background: #1E278F;
    color: white;
    border: 1px solid #1E278F;
}

button[class*="type108"] {
    border-radius: 0;
    background-size: 100% 100% !important;;
    position: relative;
    border: 1px solid transparent;
}

.btn-count-big.type108-count0 {
    background: url("' . get_stylesheet_directory_uri() . '/assets/img/0_white.png");
    color: #1E278F;
}

.btn-count-big.type108-count1 {
    background: url("' . get_stylesheet_directory_uri() . '/assets/img/1_red.png");
    color: white;
}

.btn-count-big.type108-count2 {
    background: url("' . get_stylesheet_directory_uri() . '/assets/img/2_green.png");
    color: white;
}

.btn-count-big.type108-count3 {
    background: url("' . get_stylesheet_directory_uri() . '/assets/img/3_yellow.png");
    color: #1E278F;
}

.btn-count-big.type108-count4 {
    background: url("' . get_stylesheet_directory_uri() . '/assets/img/4_blue.png");
    color: white;
}

button[class*="type108"] span {
    display: inline-block;
    width: 100%;
    position: absolute;
    bottom: 30px;
    left: 0;
}</style>
       <button class="btn-count-big type1-count0" data-type="1" data-count="0" data-value="1"><span>Click to add <br>1</span></button>
       <button class="show-box-count"><span class="dashicons dashicons-edit"></span></button>
       </div>
    
        <div class="box-select text-center">
           <a class="value-count count-1 active" href="javascript:void(0)" data-value="1">1</a><a class="value-count count-108" href="javascript:void(0)" data-value="108">108</a>
       </div>

       <label class="btn-live button hide">
        <a class="done-count-live hide" href="javascript:void(0)">' . __('Finish live counting', 'Divi') . '</a>
        </label>
    </div>
    <div class="info display">
        <h2>' . __('Total Practices Count', 'Divi') . '</h2>
        <div class="group-box col-md-12">
            <label class="label">' . __('my Total (all practices)', 'Divi') . '</label>
            <input type="text" class="total_user" value="" readonly>
        </div>

        <div class="group-box html_practice">
        </div>
        <div class="group-box col-md-12">
            <label class="label">' . __('my Center total (all practices)', 'Divi') . '</label>
            <input type="text" class="total_center" value="" readonly>
        </div>
        <div class="group-box-left">
            <label class="label">' . __('grand total (all practices)', 'Divi') . '</label>
            <input type="text" class="total_all_center" value="" readonly>
        </div>
        <div class="group-box-right">
            <a class="button" onclick="App.pageMore()" href="javascript:;">' . __('More...', 'Divi') . '</a>
        </div>
    </div>
    <div class="image-loading loading display">
        </div>
    </div>';
    $html .= "</div>";
    return $html;
}

add_shortcode('count', 'center_func');

// ajax get data khi onchange practice page count

add_action('wp_ajax_getOnchangeData', 'getOnchangeData');
add_action('wp_ajax_nopriv_getOnchangeData', 'getOnchangeData');

function getOnchangeData()
{
    global $wpdb;
    $total_user_practice = 0;
    $current_user = wp_get_current_user();
    if (0 != $current_user->ID) {
        $user_id = $current_user->ID;
    }
    $current_practice = $_POST['practices'];

//    $data_id = check_current_accumulations();
    $data_id = $_POST['accumulation'];
    $meta_data = get_post_meta($data_id);

    $center_select = '';
    $today = strtotime(date('d-m-Y'));
    if ($user_id) {
        $center_select = get_the_author_meta('user_meta_center_filter', $user_id);
        $personal_target = $personal_starting_count = 0;
        $practice_html = get_all_practice_html_by_user($user_id);
        $get_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $user_id . " AND `meta_key`='user_meta_current_practice'");
        $html_practice = '';
        foreach ($get_practice as $key => $value) {
            $name[] = unserialize($value->value);
        }
        $name = array_sort($name, 'practices_name', SORT_ASC);
        foreach ($name as $key => $value) {
            if ($value['practices_name'] == $current_practice) {
                $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $current_user->user_login . "' AND b.meta_value='" . $value ['practices_name'] . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
                foreach ($manis_done as $practice) {
                    $date_manis = get_post_meta($practice->ID, 'wpcf-date-center', true);
                    $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
                    if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'] [0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
                        $total_user_practice += get_post_meta($practice->ID, 'wpcf-manis-done', true);
                    }
                }
                $getTarget = $value['date_target_' . $data_id];
                $personal_target += $getTarget;
                $target = ($getTarget > 0) ? " of " . number_format($getTarget) : "";
                $getStart = $value ['starting_count_' . $data_id];
                $personal_starting_count += $getStart;
                $html_practice .= '<div class="group-box col-md-12">
            <label class="label">my ' . $value ['practices_name'] . '</label>
            <input type="text" class="my_total" value="' . number_format($total_user_practice + sum_all_starting_count($value)) . $target . '" readonly>
        </div>';

                $getTargetAll = $value['target_practice_all'];
                $total_all = $getTargetAll - ($total_user_practice + sum_all_starting_count($value));
                $total_user_practice = 0;
                $target = '';
                $array_practices_name[] = $value['practices_name'];

            }
        }
    } else {
        $practice_html = get_all_practice_html_by_user(0);
    }
    $remaining_daily = $days_left = $total_user = $total_user_center = 0;
    $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $current_user->user_login . "' AND b.meta_value='" . $center_select . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
    $manis_done_center = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE b.meta_value='" . $center_select . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
    foreach ($manis_done as $key => $value) {
        $date_manis = get_post_meta($value->ID, 'wpcf-date-center', true);
        $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
        if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'] [0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
            $total_user += get_post_meta($value->ID, 'wpcf-manis-done', true);
        }
    }
    foreach ($manis_done_center as $key => $value) {
        $date_manis = get_post_meta($value->ID, 'wpcf-date-center', true);
        $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
        if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'] [0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
            $total_user_center += get_post_meta($value->ID, 'wpcf-manis-done', true);
        }
    }

    $args = array(
        'numberposts' => -1,
        'post_type' => 'manis-history',
        'post_status' => 'private',
        'orderby' => 'name',
        'order' => 'ASC',
    );
    $postslist = get_posts($args);
    $id_post = $running_total_yesterday = $day_count_today = $total_all_center = 0;
    foreach ($postslist as $key => $post) {
        $meta_post = get_post_meta($post->ID);
        $total_all_center += $meta_post['wpcf-manis-done'][0];
        $date_manis = get_post_meta($post->ID, 'wpcf-date-center', true);
        $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
        if ($post->post_title == $current_user->user_login) {
            if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'] [0] && $date_manis_interval <= $meta_data ['wpcf-acc-end-date'][0] && $today > $date_manis_interval && in_array($meta_post['wpcf-my-practice'][0], $array_practices_name)) {
                $running_total_yesterday += $meta_post['wpcf-manis-done'][0];
            } elseif ($date_manis_interval >= $meta_data['wpcf-acc-start-date'] [0] && $date_manis_interval <= $meta_data ['wpcf-acc-end-date'][0] && in_array($meta_post['wpcf-my-practice'][0], $array_practices_name)) {
                $day_count_today += $meta_post['wpcf-manis-done'][0];
            }
        }
    }
    $getpost = get_page_by_title($center_select, OBJECT, 'list-center');
    $center_target = get_post_meta($getpost->ID, 'wpcf-center_target', true);
    $total_target = $meta_data['wpcf-total-target'][0];
    $start_date = $meta_data['wpcf-acc-start-date'][0];
    $end_date = $meta_data['wpcf-acc-end-date'] [0];
    if ($today >= $start_date && $today <= $end_date) {
        $datediff = abs($today - $end_date);
        $days_left = ($datediff / (60 * 60 * 24)) + 1;
        $daily_target = round(($personal_target - ($running_total_yesterday + $personal_starting_count)) / $days_left);
        $left_today = $daily_target - $day_count_today;
        $remaining_daily = round((($personal_target - ($personal_starting_count + $running_total_yesterday + $day_count_today)) / ($days_left - 1)), 0);
        $show_remaining_daily = (($days_left - 1) >= 0) ? 1 : 0;
    }
    $show_days_left = ($personal_target > 0) ? 1 : 0;
    $show_target_all = ($data_id > 0) ? 0 : 1;

    if ($user_id):
        setcookie('current_practice_' . $user_id, $current_practice, strtotime('+1 day'), '/');
    else:
        setcookie('current_practice', $current_practice, strtotime('+1 day'), '/');
    endif;

    if ($user_id && $center_select) {
        echo json_encode(array(
            'msg' => 'send',
            'err' => 0,
            'type' => 1,
            'total_center' => $total_user_center + getStartingCenter(),
            'total_center_target' => $center_target,
            'total_all_center' => $total_all_center + getStartingAll(),
            'total_all_center_target' => $total_target,
            'name' => $post->post_title,
            'total_user' => $total_user + getStartingUser(),
            'total_user_target' => $personal_target,
            'left_today' => $left_today,
            'daily_target' => $daily_target,
            'running_total_yesterday' => $running_total_yesterday,
            'personal_starting_count' => $personal_starting_count,
            'center_select' => $center_select,
            'practice_select' => $practice_html,
            'id' => $post->ID,
            'day_count_today' => $day_count_today,
            'days_left' => $days_left,
            'email' => $current_user->user_login,
            'show_days_left' => $show_days_left,
            'remaining_daily' => $remaining_daily,
            'show_remaining_daily' => $show_remaining_daily,
            'html_practice' => $html_practice,
            'targetAll' => $getTargetAll,
            'total_all' => $total_all,
            'show_total_all' => $show_target_all,

        ));
        exit;
    } else {
        echo json_encode(array(
            'msg' => 'send',
            'err' => 1,
            'practice_select' => $practice_html,
            'left_today' => $left_today,
            'daily_target' => $daily_target,
            'center_select' => $center_select,
            'show_days_left' => $show_days_left,
        ));
        exit;
    }
}

function getLeftToday($practice, $accumulations, $count_user)
{

    global $wpdb;
    $current_user = wp_get_current_user();
    if (0 != $current_user->ID) {
        $user_id = $current_user->ID;
    }
    $current_practice = $practice;

//    $data_id = check_current_accumulations();
    $data_id = $accumulations;
    $over_target = get_user_meta($user_id, 'overall_target_' . $data_id, TRUE);

    $meta_data = get_post_meta($data_id);

    $today = strtotime(date('d-m-Y'));
    if ($user_id) {
        $personal_target = $personal_starting_count = 0;
        $get_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $user_id . " AND `meta_key`='user_meta_current_practice'");

        foreach ($get_practice as $key => $value) {
            $name[] = unserialize($value->value);
        }
        $name = array_sort($name, 'practices_name', SORT_ASC);
        foreach ($name as $key => $value) {
            if ($value['practices_name'] == $current_practice) {
                $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $current_user->user_login . "' AND b.meta_value='" . $value ['practices_name'] . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
                $getTarget = $value['date_target_' . $data_id];
                $personal_target += $getTarget;
                $getStart = $value ['starting_count_' . $data_id];
                $personal_starting_count += $getStart;
                $total_user_practice = 0;
                $target = '';
                $array_practices_name[] = $value['practices_name'];
            }
        }
    }
    $remaining_daily = $days_left = 0;

    $args = array(
        'numberposts' => -1,
        'post_type' => 'manis-history',
        'post_status' => 'private',
        'orderby' => 'name',
        'order' => 'ASC',
    );
    $postslist = get_posts($args);
    $id_post = $running_total_yesterday = $day_count_today = $day_count_today_all = 0;
    foreach ($postslist as $key => $post) {
        $meta_post = get_post_meta($post->ID);
        $date_manis = get_post_meta($post->ID, 'wpcf-date-center', true);
        $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
        if ($post->post_title == $current_user->user_login) {
            if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'][0] && $date_manis_interval <= $meta_data ['wpcf-acc-end-date'][0] && $today > $date_manis_interval && in_array($meta_post['wpcf-my-practice'][0], $array_practices_name)) {
                $running_total_yesterday += $meta_post['wpcf-manis-done'][0];
            } elseif ($date_manis_interval >= $meta_data['wpcf-acc-start-date'] [0] && $date_manis_interval <= $meta_data ['wpcf-acc-end-date'][0] && in_array($meta_post['wpcf-my-practice'][0], $array_practices_name)) {
                $day_count_today += $meta_post['wpcf-manis-done'][0];
            }
            if ($today == $date_manis_interval && in_array($meta_post['wpcf-my-practice'][0], $array_practices_name)) {
                $day_count_today_all += $meta_post['wpcf-manis-done'][0];
            }

        }
    }

    $start_date = $meta_data['wpcf-acc-start-date'][0];
    $end_date = $meta_data['wpcf-acc-end-date'] [0];
    if ($today >= $start_date && $today <= $end_date) {
        $datediff = abs($today - $end_date);
        $days_left = ($datediff / (60 * 60 * 24)) + 1;
        $daily_target = round(($personal_target - ($running_total_yesterday + $personal_starting_count)) / $days_left);
        $left_today = $daily_target - $day_count_today;
        $remaining_daily = round((($personal_target - ($personal_starting_count + $running_total_yesterday + $day_count_today)) / ($days_left - 1)), 0);
        $show_remaining_daily = (($days_left - 1) >= 0) ? 1 : 0;
        $left_today_all_check = round(($over_target - $count_user) / $days_left);
        $left_today_all = $left_today_all_check - $day_count_today_all;
        $remaining_daily_all = round(($over_target - $count_user - $day_count_today_all) / ($days_left - 1));
    }
    $show_days_left = ($left_today > 0) ? 1 : 0;


    return array(
        'left_today' => $left_today,
        'daily_target' => $daily_target,
        'running_total_yesterday' => $running_total_yesterday,
        'personal_starting_count' => $personal_starting_count,
        'day_count_today' => $day_count_today,
        'total_user_target' => $personal_target,
        'days_left' => $days_left,
        'show_days_left' => $show_days_left,
        'remaining_daily' => $remaining_daily,
        'show_remaining_daily' => $show_remaining_daily,
        'remaining_daily_all' => $remaining_daily_all,
        'left_today_all' => $left_today_all,
    );


}

add_shortcode('share_button_count', 'share_button_count_func');

function share_button_count_func($atts = array())
{
    ob_start();
    $current_url = home_url() . $_SERVER['REQUEST_URI'];
    $wporg_atts = shortcode_atts(array(
        'button_text' => 'Share!',
        'share_link' => $current_url
    ), $atts);
    set_query_var('button_text', $wporg_atts['button_text']);
    set_query_var('share_link', $wporg_atts['share_link']);
    get_template_part('social_share_count');

    return ob_get_clean();
}

add_shortcode('dedicate_button_count', 'dedicate_button_count_func');

function dedicate_button_count_func($atts = [], $content = null, $tag = '')
{
    $atts = array_change_key_case((array)$atts, CASE_LOWER);
    $wporg_atts = shortcode_atts(['button_text' => 'Dedicate!'], $atts, $tag);
    $html = '<a href="' . get_page_link(8150) . '" id="shareBtn" class="button">' . esc_html__($wporg_atts ['button_text'], 'Divi') . '</a>';
    return $html;
}


function sumAllStartingCount($data)
{
    $sumStart = 0;
    foreach ($data as $key => $value) {
        if (strpos($key, 'starting_count_') !== false) {
            $check = explode('starting_count_', $key);
            if ($check[1] > 100) {
                $sumStart += $value;
            }
        }
    }
    return $sumStart;
}