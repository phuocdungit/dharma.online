<?php
/**
 * Created by PhpStorm.
 * User: ANH
 * Date: 4/28/2016
 * Time: 9:05 AM
 */

/*
 * sum_all_starting_count
 * tinh tong all starting count cua Practices duoc truyen vao
 * $value: mang cua 1 Practices
 * $id: neu co $id thi lay gia tri theo bo loc, khong thi lay all
 */

function sum_all_starting_count_center($name)
{
    global $wpdb;
    $sum = 0;
    $current_user = wp_get_current_user();
    $check = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "user_practice_starting_count_date WHERE `practices` ='" . $name . "'");
    if (count($check) > 0) {
        foreach ($check as $item) {
            $sum += $item->starting_count;
        }
    }
    return $sum;
}

function sum_all_starting_count($data, $id = 0)
{
    $current_user = wp_get_current_user();
    global $wpdb;
    $sum = 0;
    if ($id == 0) {
        global $wpdb;
        $current_user = wp_get_current_user();
        $check = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "user_practice_starting_count_date");
        if (count($check) > 0) {
            foreach ($check as $item) {
                $sum += $item->starting_count;
            }
        }
    } else {
        $get_accumulation_select = get_post_meta($id);
        $get_practice = check_save_count($get_accumulation_select, $data);
        if (count($get_practice) > 0) {
            foreach ($get_practice as $item) {
                $sum += $item->starting_count;
            }
        }
    }
    return $sum;
}

function get_starting_count($name, $id = 0)
{
    $sum = 0;
    if ($id == 0) {
        global $wpdb;
        $current_user = wp_get_current_user();
        $check = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "user_practice_starting_count_date WHERE `user_id`=" . $current_user->ID . " AND `practices` ='" . $name . "'");
        if (count($check) > 0) {
            foreach ($check as $item) {
                $sum += $item->starting_count;
            }
        }
    } else {
        $get_accumulation_select = get_post_meta($id);
        $get_practice = check_save_count($get_accumulation_select, $name);
//    echo '<pre>';
//    print_r($get_practice);
        if (count($get_practice) > 0) {
            foreach ($get_practice as $item) {
                $sum += $item->starting_count;
            }
        }
    }
    return $sum;
}

/*
 * sum_all_target_count
 * tinh tong all target count cua Practices duoc truyen vao
 * $value: mang cua 1 Practices
 * $id: neu co $id thi lay gia tri theo bo loc, khong thi lay all
 */

function sum_all_target_count($value, $id = 0)
{
    $sum = 0;
    if ($id == 0) {
        $args = array(
            'post_type' => 'accumulations',
            'post_status' => 'any',
            'meta_key' => 'wpcf-acc-start-date',
            'orderby' => 'meta_value',
        );
        $all_accumulations = get_posts($args);
        foreach ($all_accumulations as $acc) {
            $data_id = $acc->ID;
            $getTarget = $value['date_target_' . $data_id];
            $sum += $getTarget;
        }
    } else {
        $sum = $value['date_target_' . $id];
    }
    return $sum;
}


function check_save_count($data = array(), $Practices)
{
    global $wpdb;
    $current_user = wp_get_current_user();
    $start_date = $data['wpcf-acc-start-date'][0];
    $end_date = $data['wpcf-acc-end-date'][0];
    $today = strtotime('today');
    $check = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "user_practice_starting_count_date WHERE `user_id`=" . $current_user->ID . " AND `date_save` BETWEEN '" . date('Y-m-d', $start_date) . "' AND '" . date('Y-m-d', $end_date) . "' AND `practices` ='" . $Practices . "'");
//    echo '<pre>';
//    echo '<pre>';
//    print_r($check);
//    print_r($wpdb->last_query);
    return $check;


}