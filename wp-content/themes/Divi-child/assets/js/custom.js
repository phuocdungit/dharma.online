/*********************************************************************************
 * @name: common functions
 * @description:  Call functions of website
 * @author: (c) App MEDIA VIET NAM (http://xgoon.com - contact@xgoon.com)
 * @version: 1.0
 *********************************************************************************/

/****************************
 ***** Global Namespace *****
 ****************************/
var App = App = App || {
    isLog: true,
    initTest: function () {

    }
};
/*************************************
 ***** Main functions start here *****
 *************************************/
jQuery(document).ready(function () {
    // var Accumulation = App.readCookie('accumulation');
    with (App) {
        redirectNoCookie();
        initRegistrationProfile();
        initFormCenter();
        initAutoCheck();
        initFormMore();
        initFormMorePublic();
        initAutoCheckProfile();
        initFormProfile();
        loadContact();
        initDedications();
        loadCountPractice();
        getHasChanges();
        initCounLive();
        setCountLive();
        saveOldPractice();
        // checkMovePage();
        // setWidthButton();
//        AccumulationCheck();
//         $(".manis-done").currency();
        // .....
    }
    jQuery(".nav-tabs a").click(function () {
        jQuery(this).tab('show');
    });
    jQuery('.Simuler-son-tarif').bind('click', function () {
        jQuery(".thickbox.button-colisee").trigger("click");
    });
    jQuery("html, body").click(function () {
        jQuery('.notice').html("");
    });
    jQuery('.center_filter').click(function () {
        jQuery(this).data('val', jQuery(this).val());
    });
//    jQuery('.accumulation_filter').children('option[id=8224]').text('new text');
//     jQuery('.accumulation_filter option[id="8224"]').text('Wedding Ceremony');
});

/******************************
 ***** Show error if have *****
 ******************************/
function log(msg) {
    if (!App.isLog)
        return false;
    if (typeof (console) != 'undefined') {
        console.log(msg);
    }
}

window.onerror = function (msg, url, linenumber) {
    if (typeof (console) != 'undefined') {
        console.log('Error message: ' + msg + '\nURL: ' + url + '\nLine Number: ' + linenumber);
    }
    return false;
};
/******************************
 ***** Functions built in *****
 ******************************/
App.browserName = function () {
    var Browser = navigator.userAgent;
    if (Browser.indexOf('MSIE') >= 0) {
        Browser = 'MSIE';
    } else if (Browser.indexOf('Firefox') >= 0) {
        Browser = 'Firefox';
    } else if (Browser.indexOf('Chrome') >= 0) {
        Browser = 'Chrome';
    } else if (Browser.indexOf('Safari') >= 0) {
        Browser = 'Safari';
    } else if (Browser.indexOf('Opera') >= 0) {
        Browser = 'Opera';
    } else {
        Browser = 'UNKNOWN';
    }
    return Browser;
};
/*
 * initCheck
 * get data khi load page count
 */
App.initCheck = function (email, center_select) {
    App.Accumulation(1);
}

/*
 * initCheckProfile
 * get data khi load page profile
 */
App.initCheckProfile = function (email) {
    jQuery.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: {action: 'CheckProfile', 'email': email},
        success: function (response) {
            //  console.log(response)
//            return false;
            if (response) {
                var data = jQuery.parseJSON(response);
                if (data.err == 0) {
                    if (data.center_select != '') {
                        jQuery('#formSendProfile .center_filter').val(data.center_select);
//                        jQuery('#formSendProfile .center_filter').val(data.center_select).attr("disabled", "disabled");
                    }
                    if (data.leaderboard == 1) {
                        jQuery('#formSendProfile .leaderboard').attr('checked', true);
                        jQuery(".description").show(); // checked
                        jQuery("label.myname").html('my Group name');
//                        jQuery('#formSendProfile .center_filter').val(data.center_select).attr("disabled", "disabled");
                    }
                    if (data.myname != '') {
                        jQuery('#formSendProfile .name').val(data.myname);
                    }
                    if (data.description != '') {
                        jQuery('#formSendProfile .group-description').val(data.description);
                    }
                    if (data.html_practice != '') {
                        jQuery('.your-practices').html(data.html_practice);
                    }
                    pathArray = location.href.split('/');
                    protocol = pathArray[0];
                    host = pathArray[2];
                    url = protocol + '//' + host;
                    if (data.avatar != '') {
                        var $avatar = '<img src="' + url + '/wp-content/uploads/' + data.avatar + '" class="img-responsive"/>';
                        jQuery('#formSendProfile .Picture').html($avatar);
                    } else {
                        jQuery('#formSendProfile .Picture').html('');
                    }
                    if (data.total_user_target > 0) {
                        jQuery('#formSendProfile input#my-target').val(App.format(data.total_user_target));
                    }
                    if (data.email != '') {
                        document.cookie = "email_center=" + data.email + " ; path=/";
                    }
                    jQuery('#formSendProfile input.id').val(data.id);
                    if (data.over_target > 0) {
                        jQuery('.overall-target-box').addClass('exit-data');
                        jQuery('.overall-target-box .overall-target').val(App.format(data.over_target)).attr("disabled", "disabled");
                        return false;
                    } else {
                        jQuery('.overall-target-box .overall-target').val("").removeAttr("disabled", "disabled");
                        jQuery('.overall-target-box').removeClass('exit-data');
                    }
                } else {
                    jQuery('#formSendProfile .leaderboard').attr('checked', false);
                    jQuery('.add_new_center').hide();
                    jQuery(".description").hide(); // unchecked
                    jQuery('#formSendProfile .group-description').val('');
                    jQuery('#formSendProfile .name').val('');
                    jQuery('#formSendProfile input#my-target').val('');
                    // jQuery('#formSendProfile .center_filter').removeAttr("disabled", "disabled");                    
                    jQuery("#formSendProfile .center_filter option:selected").removeAttr("selected");
                    jQuery('#formSendProfile .Picture').html('');
                }

            }
        },
        error: function () {
//            alert("an error occurred, please try again");
            location.reload();
        }
    });
}

/*
 * initAutoCheck
 * check data neu day du thi load initCheck
 */
App.initAutoCheck = function () {
    var initFormCenter = jQuery('#formSendCenter');
    // console.log(initFormCenter);
    if (initFormCenter.length > 0) {
        // alert(212);
        var cookiemail = App.readCookie('email_center');
        if (cookiemail) {
            var center_select = jQuery("#formSendCenter option:selected").val();
            // console.log(center_select);
            App.initCheck(cookiemail, center_select);
        } else {
            App.initCheck();
        }
        jQuery('#formSendCenter .email').on('focusin', function () {
            console.log("Saving value " + jQuery(this).val());
            jQuery(this).data('val', jQuery(this).val());
        });
        jQuery('#formSendCenter .email').on('change', function () {
            var prev = jQuery(this).data('val');
            var current = jQuery(this).val();
            var email = jQuery("#formSendCenter .email").val();
            var emailRegex = new RegExp(/^([\w\.\-]+)@([\w\-]+)((\.(\w){2,10})+)$/i);
            var isvalid = emailRegex.test(email);
            if (current.length == 0 && prev.length != 0) {
                return false;
            }
            if (prev.length == 0) {
                if (!isvalid) {
                    alert("Email address is not valid!");
                    jQuery(this).val('');
                    return false;
                } else {
                    App.initCheck(email, center_select);
                }
            } else {

                if (confirm("Are you sure you  want to change your email address? The email address functions as your user name. If you change it, the new address will start with a blank slate.")) {
                    if (!isvalid) {
                        alert("Email address is not valid!");
                        jQuery(this).val('');
                        return false;
                    } else {
                        jQuery('.info').css({"display": "none"});
                        var email = jQuery("#formSendCenter .email").val();
                        var center_select = jQuery("#formSendCenter option:selected").val();
                        App.initCheck(email, center_select);
                    }
                } else {
                    // location.reload();
                    return false;
                }
            }
        });
        jQuery("#formSendCenter .manis-done").keyup(function () {
            App.manisDone();

        });
    }

}

/*
Check Manis done
 */

App.manisDone = function () {
    var left_today = parseInt(jQuery("#formSendCenter .left_today_2").val().replace(/,/g, ""));
    var left_today2 = jQuery("#formSendCenter .left_today").val().replace(/,/g, "");
    var manis_done = parseInt(jQuery("#formSendCenter .manis-done").val().replace(/,/g, ""));
    var days_left = parseInt(jQuery("#formSendCenter .days_left").val().replace(/,/g, ""));
    var total_center = parseInt(jQuery("#formSendCenter .total_center").val().replace(/,/g, ""));
    var personal_target = parseInt(jQuery("#formSendCenter .personal_target").val().replace(/,/g, ""));
    var running_total_yesterday = parseInt(jQuery("#formSendCenter .running_total_yesterday").val().replace(/,/g, ""));
    var personal_starting_count = parseInt(jQuery("#formSendCenter .personal_starting_count").val().replace(/,/g, ""));
    var remaining_daily_check = jQuery("#formSendCenter .remaining_daily").val().replace(/,/g, "");
    var day_count_today = parseInt(jQuery("#formSendCenter .day_count_today").val().replace(/,/g, ""));
    var remaining = parseInt(jQuery("#formSendCenter .remaining_all").val().replace(/,/g, ""));
    var left_today_all = parseInt(jQuery("#formSendCenter .left_today_all").val().replace(/,/g, ""));
    var left_today_all2 = parseInt(jQuery("#formSendCenter .left_today_all_2").val().replace(/,/g, ""));
    var remaining_daily_all = parseInt(jQuery("#formSendCenter .remaining_daily_all").val().replace(/,/g, ""));
    var remaining_daily_all_2 = parseInt(jQuery("#formSendCenter .remaining_daily_all_2").val().replace(/,/g, ""));
    if (!manis_done) {
        manis_done = 0;
    }
    var sum = left_today - manis_done;
    if (left_today != "" && left_today2 != "DONE" && sum > 0) {
        jQuery('input.left_today').val(App.format(sum));
    } else {
        jQuery('input.left_today').val("DONE");
    }

    days_left = days_left - 1;
    remaining_daily = parseInt((personal_target - (day_count_today + running_total_yesterday + personal_starting_count + manis_done)) / (days_left)); //+1 de lam tron so
    if (remaining_daily > 0 && remaining_daily_check != "DONE") {
        jQuery('input.remaining_daily').val(App.format((remaining_daily)));
    } else {
        jQuery('input.remaining_daily').val("DONE");
    }

    remaining_all = remaining - manis_done;
    if (remaining_all > 0 && jQuery('input.remaining') != "DONE") {
        jQuery('input.remaining').val(App.format(remaining_all));
    } else {
        jQuery('input.remaining').val("DONE");
    }
    left_today_all_show = left_today_all2 - manis_done;
    if (left_today_all_show > 0 && jQuery('input.left_today_all') != "DONE") {
        jQuery('input.left_today_all').val(App.format(left_today_all_show));
    } else {
        jQuery('input.left_today_all').val("DONE");
    }
    remaining_daily_all_show = remaining_daily_all_2 - manis_done;
    if (remaining_daily_all_show > 0 && jQuery('input.remaining_daily_all') != "DONE") {
        jQuery('input.remaining_daily_all').val(App.format(remaining_daily_all_show));
    } else {
        jQuery('input.remaining_daily_all').val("DONE");
    }

    jQuery(".click-box .btn-count-big span").text(App.format(manis_done));
}

/*
 * initAutoCheckProfile
 * check data neu day du thi load initCheckProfile
 */
App.initAutoCheckProfile = function () {
    var initAutoCheckProfile = jQuery('#formSendProfile');
    if (initAutoCheckProfile.length > 0) {
        App.initCheckProfile();
        var cookiemail = App.readCookie('email_center');
        if (cookiemail) {
            App.initCheckProfile(cookiemail);
        }
        jQuery('#formSendProfile .email').on('focusin', function () {
            console.log("Saving value " + jQuery(this).val());
            jQuery(this).data('val', jQuery(this).val());
        });
        jQuery('#formSendProfile .email').on('change', function () {
            var prev = jQuery(this).data('val');
            var current = jQuery(this).val();
            var email = jQuery("#formSendProfile .email").val();
            var emailRegex = new RegExp(/^([\w\.\-]+)@([\w\-]+)((\.(\w){2,10})+)$/i);
            var isvalid = emailRegex.test(email);
            if (prev.length == 0) {
                if (!isvalid) {
                    alert("Email address is not valid!");
                    jQuery(this).val('');
                    return false;
                } else {
                    App.initCheckProfile(email);
                }
            } else {
                if (confirm("Are you sure you  want to change your email address? The email address functions as your user name. If you change it, the new address will start with a blank slate.")) {
                    if (!isvalid) {
                        alert("Email address is not valid!");
                        jQuery(this).val('');
                        return false;
                    } else {
                        App.initCheckProfile(email);
                    }
                } else {
                    // location.reload();
                    return false;
                }
            }
        });
//        return false;
//        jQuery("#formSendProfile .email").focusout(function() {
//            
//        })
//        jQuery("#formSendProfile .center_filter").change(function() {
//            var email = jQuery("#formSendProfile .email").val();
//            jQuery.ajax({
//                type: 'POST',
//                url: '/wp-admin/admin-ajax.php',
//                data: {action: 'checkUserExists', 'email': email},
//                success: function(response) {
//                    if (response) {
//                        var data = jQuery.parseJSON(response);
//                        if (data.err == 0) {
//                            if (confirm(data.msg)) {
//                                window.location.href = "/contact";
//                                return false;
//                            } else {
//                                return false;
//                            }
//                        }
//                    }
//                }
//            })
//        }),
        jQuery("#formSendCenter .manis-done").keyup(function () {
//        alert(121);
            var left_today = jQuery("#formSendCenter .left_today_2").val().replace(/,/g, "");
            var manis_done = jQuery("#formSendCenter .manis-done").val().replace(/,/g, "");
            if (left_today != "" && left_today != "DONE") {
                jQuery('input.left_today').val(App.format(left_today - manis_done));
            }
        });
    }

}

/*
 * initFormCenter
 * xu ly data khi count
 */
App.initFormCenter = function () {
    var initFormCenter = jQuery('#formSendCenter');
    if (initFormCenter.length > 0) {
        initFormCenter.submit(function (event) {
            event.preventDefault();
            // var center_select = jQuery("#formSendCenter option:selected").val();
            var center_select = jQuery("#formSendCenter .center_filter option:selected").val();
            var practice_filter = jQuery("#formSendCenter .practice_filter option:selected").val();
            var email = jQuery("#formSendCenter .email").val();
            var emailRegex = new RegExp(/^([\w\.\-]+)@([\w\-]+)((\.(\w){2,10})+)$/i);
            var isvalid = emailRegex.test(email);
            var manisdone = jQuery("#formSendCenter .manis-done").val();
            var total_user = jQuery('.total_user').val();
//            console.log(total_user)
//            return false;
            var total_center = jQuery('.total_center').val();
            var total_all_center = jQuery('.total_all_center').val();
            var id = jQuery('.id').val();
            if (email.length == 0) {
                jQuery(".email").focus();
                jQuery(".email").addClass("required");
                alert("Please enter your email address");
                return false;
            } else {
                if (!isvalid) {
                    jQuery(".email").focus();
                    jQuery(".email").addClass("required");
                    alert("Email address is not valid!");
                    return false;
                }
            }
            // if (center_select.length == 0) {
            //     alert("Please select your center");
            //     jQuery(".center_filter").addClass("required");
            //     return false;
            // }
            if (practice_filter.length == 0) {
                alert("Please select your practice");
                jQuery(".practice_filter").addClass("required");
                return false;
            }
            if (manisdone.length == 0 || manisdone.length == 1 && manisdone == 0) {
                jQuery(".manis-done").focus();
                jQuery(".manis-done").addClass("required");
                alert("Please enter your number");
                return false;
            }
            document.cookie = "email_center=" + email + " ; path=/";
            jQuery.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                data: {
                    action: 'addCenter',
                    center_select: center_select,
                    practice_filter: practice_filter,
                    'email': email,
                    'manisdone': manisdone,
                    'total_user': total_user,
                    'id': id,
                    'total_center': total_center,
                    'total_all_center': total_all_center
                },
                beforeSend: function () {
                    jQuery('#formSendCenter .submit.center').html('<a class="button" href="javascript:;">Processing...</a>');
                    jQuery('.info').css({"display": "none"});
                    jQuery('.btn-live').addClass('no-click');
                },
                success: function (response) {
//                    console.log(response)
//                    return false;
                    if (response) {
                        var data = jQuery.parseJSON(response);
                        if (data.err == 0) {
                            jQuery('input#manis-done').val('');
                            App.initCheck(email, center_select);
                            jQuery('#formSendCenter .submit.center').html('<a class="button" href="javascript:;" onclick="jQuery(\'#formSendCenter\').submit();">Count!</a>');
                            jQuery('.btn-live').removeClass('no-click');
                            jQuery('.value-count.count-1').click();
//                            jQuery('#formSendCenter .notice').html(data.msg).css({
//                                'text-align': 'center',
//                                'font-weight': 'bold',
//                                'color': 'blue'
//                            });
                            jQuery('html, body').animate({scrollTop: 0}, 800);
                            jQuery('.sharebox-count').fadeIn();
                            return false;
                        } else {
//                            alert("an error occurred, please try again");
                            location.reload();
                            return false;
                        }
                    }
                },
                error: function () {
//                    alert("an error occurred, please try again");
                    location.reload();
                }
            });
        });
    }
    return false;
};

/*
 * initRegistrationProfile
 * add center va practice cho user 
 */
App.initRegistrationProfile = function () {
    var initRegistrationProfile = jQuery('#formRegistrationProfile');
    if (initRegistrationProfile.length > 0) {
        initRegistrationProfile.submit(function (event) {
            event.preventDefault();
            var center_select = jQuery("#formRegistrationProfile option:selected").val();
            var practice_filter = jQuery("#formRegistrationProfile .practice_filter option:selected").val();
            var starting_count = jQuery("#formRegistrationProfile .starting_count").val();
            var id = jQuery("#formRegistrationProfile .id").val();
            // if (center_select.length == 0) {
            //     alert("Please select your center");
            //     jQuery(".center_filter").addClass("required");
            //     return false;
            // }
            if (center_select == 'add') {
                alert("Please add center or select your center!");
                return false;
            }
            if (practice_filter.length == 0) {
                alert("Please select your practice");
                jQuery(".practice_filter").addClass("required");
                return false;
            }
            if (practice_filter == 'add') {
                alert("Please add practices or select your practices!");
                return false;
            }
            if (starting_count.length == 0) {
                alert("Please enter the starting count value!");
                return false;
            }
            jQuery.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                data: {
                    action: 'RegistrationProfile',
                    center_select: center_select,
                    practice_filter: practice_filter,
                    starting_count: starting_count,
                    'id': id
                },
                beforeSend: function () {
                    jQuery('#formRegistrationProfile .submit.registration').html('<a class="button" href="javascript:;">Processing...</a>');
                },
                success: function (response) {
                    // console.log(response)
                    // return false;
                    if (response) {
                        var data = jQuery.parseJSON(response);
                        if (data.err == 0) {
                            jQuery('#formRegistrationProfile .submit.registration').html('<a class="button" href="javascript:;" onclick="jQuery(\'#formRegistrationProfile\').submit();">Update!</a>');
                            window.location.href = "/count";
                            return false;
                        } else {
//                            alert("an error occurred, please try again");
                            location.reload();
                            return false;
                        }
                    }
                },
                error: function () {
//                    alert("an error occurred, please try again");
                    location.reload();
                }
            });
        });
    }
    return false;
};

/*
 * initFormProfile
 * xu ly data page profile khi update
 */
App.initFormProfile = function () {
    var initFormProfile = jQuery('#formSendProfile');
    if (initFormProfile.length > 0) {
        initFormProfile.on('submit', (function (event) {
            event.preventDefault();
            var center_select = jQuery("#formSendProfile option:selected").val();
            var email = jQuery("#formSendProfile .email").val();
//            var target = jQuery("#formSendProfile .my-target").val().replace(/,/g, "");
            var name = jQuery("#formSendProfile .name").val();
            var file = jQuery("#formSendProfile .input-upload").val();
//            var pass = jQuery("#formSendProfile .password").val();
//            var pass2 = jQuery("#formSendProfile .repeat-password").val();
            var email = jQuery("#formSendProfile .email").val();
            var emailRegex = new RegExp(/^([\w\.\-]+)@([\w\-]+)((\.(\w){2,10})+)$/i);
            var isvalid = emailRegex.test(email);
            if (email.length == 0) {
                jQuery(".email").focus();
                jQuery(".email").addClass("required");
                alert("Please enter your email address");
                return false;
            } else {
                if (!isvalid) {
                    jQuery(".email").focus();
                    jQuery(".email").addClass("required");
                    alert("Email address is not valid!");
                    return false;
                }
            }
            document.cookie = "email_center=" + email + " ; path=/";
            if (name.length == 0) {
                jQuery("#formSendProfile .name").focus();
                jQuery("#formSendProfile .name").addClass("required");
                alert("Please enter your name");
                return false;
            }
            // if (center_select.length == 0) {
            //     alert("Please select your center");
            //     jQuery(".center_filter").addClass("required");
            //     return false;
            // }
            if (file) {
                var ext = file.split('.').pop().toLowerCase();
                if (jQuery.inArray(ext, ['gif', 'png', 'jpg', 'jpeg', 'tiff']) == -1) {
                    alert("You have selected a file that doesn't look like an image. For security reasons please only upload JPEG, GIF, PNG, or TIFF files. Thank you.");
                    return false;
                }
            }
//            if (pass.length < 6) {
//                alert("Password At least 6 characters");
//                jQuery(".password").focus();
//                jQuery(".password").addClass("required");
//                return false;
//            }
//            if (pass.length != 0) {
//                if (pass2.length == 0) {
//                    alert("Please enter repeat password");
//                    jQuery(".repeat-password").focus();
//                    jQuery(".repeat-password").addClass("required");
//                    return false;
//                }
//                if (pass != pass2) {
//                    alert("Password mismatch");
//                    jQuery(".repeat-password").focus();
//                    jQuery(".repeat-password").addClass("required");
//                    return false;
//                }
//            }
            var datas = new FormData(this);
            jQuery.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                contentType: false,
                processData: false,
                cache: false,
//                data: {action: 'upProfile', file: datas, center_select: center_select, 'email': email, 'target': target, 'name': name, 'id': id},
                data: datas,
                beforeSend: function () {
                    jQuery('#formSendProfile .submit.profile').html('<a class="button" href="javascript:;">Processing...</a>');
                },
                success: function (response) {
//                    console.log(response);
//                    return false;
                    if (response) {
                        var data = jQuery.parseJSON(response);
                        if (data.err == 0) {
                            App.initAutoCheckProfile(email);
                            jQuery('#formSendProfile .submit.profile').html('<a class="button" href="javascript:;" onclick="jQuery(\'#formSendProfile\').submit();">Update</a>');
                            jQuery('#formSendProfile .notice').html(data.msg).css({
                                'text-align': 'center',
                                'font-weight': 'bold',
                                'color': 'blue'
                            });
                            jQuery('html, body').animate({scrollTop: 0}, 800);
                            jQuery('#formSendProfile .password').val('');
                            jQuery('#formSendProfile .repeat-password').val('');
                            return false;
                        } else {
                            jQuery('#formSendProfile .notice').html(data.msg).css({
                                'text-align': 'center',
                                'font-weight': 'bold',
                                'color': 'red'
                            });
                            jQuery('html, body').animate({scrollTop: 0}, 800);
                            jQuery('#formSendProfile .submit.profile').html('<a class="button" href="javascript:;" onclick="jQuery(\'#formSendProfile\').submit();">Update</a>');
//                            alert("an error occurred, please try again");
//                            location.reload();
                            return false;
                        }
                    }
                },
                error: function () {
//                    alert("an error occurred, please try again");
                    location.reload();
                }
            });
        }));
    }
    return false;
};

/*
 * get data khi load page more public
 */
App.initFormMorePublic = function () {
    var initFormMore = jQuery('.center-box.more.more_func_custom');
    if (initFormMore.length > 0) {
        var email = App.readCookie("email_center");
        var center_more = App.readCookie("center_more");
        var Accumulation = jQuery('.accumulation_more').val();
        jQuery.ajax({
            type: 'POST',
            url: '/wp-admin/admin-ajax.php',
            data: {action: 'Accumulation_more_public', key: 2, id: Accumulation},
            beforeSend: function () {
//            window.alert(key);

                jQuery('.info').css({"display": "none"});
                jQuery('.et_pb_row.center-box.more.more_func .tab-content.et_pb_tabs').css({"display": "none"});
                jQuery('.your-practices').css({"display": "none"});
                jQuery('.image-loading').css({"display": "block"});

            },
            success: function (response) {
                if (response) {
//                window.alert(1);
//                console.log(response)
//                return false;remaining_daily_box display
                    var data = jQuery.parseJSON(response);
                    if (data.err == 0) {
                        if (data.type == 1) {
                            App.Accumulation_count(data);
                        }
                        if (data.type == 2) {
                            App.Accumulation_more(data);
                        }
                        if (data.type == 3) {
                            App.Accumulation_profile(data);
                        }
                        if (data.type == 4) {
                            App.Accumulation_practices(data);
                        }

                    }
                }
            },
            error: function () {
                location.reload();
            }
        });
    }
    return false;
};

/*
 * get data khi load page more
 */
App.initFormMore = function () {
    var initFormMore = jQuery('.center-box.more.more_func');
    if (initFormMore.length > 0) {
        var email = App.readCookie("email_center");
        var center_more = App.readCookie("center_more");
        var Accumulation = App.readCookie('accumulation');
        if (Accumulation) {
            App.AccumulationCheck();
        } else {

            jQuery.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                data: {action: 'getMore', 'email': email, 'center_more': center_more},
                beforeSend: function () {
                    jQuery('.et_pb_row.center-box.more.more_func .tab-content.et_pb_tabs').css({"display": "none"});
                    jQuery('.image-loading').css({"display": "block"});
                },
                success: function (response) {
                    jQuery('.et_pb_row.center-box.more.more_func .tab-content.et_pb_tabs').css({"display": "none"});
                    jQuery('.image-loading').css({"display": "block"});
                    // App.AccumulationCheck();

//                console.log(response);
//                return false;
                    if (response) {
                        var data = jQuery.parseJSON(response);
                        console.log(data);
                        if (data.err == 0) {
                            jQuery('.et_pb_row.center-box.more.more_func .tab-content.et_pb_tabs').css({"display": "block"});
                            jQuery('.image-loading').css({"display": "none"});
                            if (data.check == 1) {
                                if (data.total_user_target) {
                                    jQuery('.center-box.more .box-top input.mine').val(App.format(data.total_user) + ' of ' + App.format(data.total_user_target));
                                } else {
                                    jQuery('.center-box.more .box-top input.mine').val(App.format(data.total_user));
                                }
                                if (data.total_all_center_target) {
                                    jQuery('.center-box.more .box-top input.grand_total').val(App.format(data.total_all_center) + ' of ' + App.format(data.total_all_center_target));
                                } else {
                                    jQuery('.center-box.more .box-top input.grand_total').val(App.format(data.total_all_center));
                                }
                                if (data.total_user_center_target) {
                                    jQuery('.center-box.more .box-top input.myCenter').val(App.format(data.total_user_center) + ' of ' + App.format(data.total_user_center_target));
                                } else {
                                    jQuery('.center-box.more .box-top input.myCenter').val(App.format(data.total_user_center));
                                }
                                if (data.htmlCenter) {
                                    jQuery('.center-box.more #center .box-content').html(data.htmlCenter);
                                }
                                if (data.htmlUser) {
                                    jQuery('.center-box.more #person .box-content').html(data.htmlUser);
                                }
                                if (data.htmlPractic) {
                                    jQuery('.center-box.more #practice .box-content').html(data.htmlPractic);
                                }
                                if (data.htmlPracticTop) {
                                    jQuery('.center-box.more #practice .group-box.practice').html(data.htmlPracticTop);
                                }
                                if (data.number_top_user) {
                                    jQuery('.center-box.more #person .label.mine').html('mine (' + data.number_top_user + ')');
                                }
                                if (data.htmlCenterTop != '') {
                                    jQuery('.center-box.more #center .box-top .group-box.center-top ').html(data.htmlCenterTop);
                                }
                                if (data.number_top_center > 0) {
                                    jQuery('.center-box.more #center .label.center').html('my Center (' + data.number_top_center + ')');
                                }
                                if (data.my_region != '') {
                                    jQuery('.center-box.more #region .box-top .group-box.region ').html(data.my_region);
                                }
                                if (data.my_region_top != '') {
                                    jQuery('.center-box.more #region .box-top .group-box.region-top ').html(data.my_region_top);
                                }
//                            alert(data.htmlUserTop);
                                if (data.htmlUserTop != '') {
                                    jQuery('.center-box.more #person .box-top .persontop').html(data.htmlUserTop);
                                }
                                if (data.htmlRegion != '') {
                                    jQuery('.center-box.more #region .box-content').html(data.htmlRegion);
                                }
                                if (data.htmlUserCollections != '') {
                                    jQuery('.center-box.more #collections .box-content').html(data.htmlUserCollections);
                                }
                                if (data.number_collections_on_top) {
                                    jQuery('.center-box.more #collections .label.mine').html('mine (' + data.number_collections_on_top + ')');
                                }
                                return false;
                            }
                            if (data.check == 2) {
//                            alert(112);
                                if (data.total_user_target) {
                                    jQuery('.center-box.more .box-top input.mine').val(App.format(data.total_user) + ' of ' + App.format(data.total_user_target));
                                } else {
                                    jQuery('.center-box.more .box-top input.mine').val(App.format(data.total_user));
                                }
//                            alert(data.total_all_center_target);
                                if (data.total_all_center_target) {
                                    jQuery('.center-box.more .box-top input.grand_total').val(App.format(data.total_all_center) + ' of ' + App.format(data.total_all_center_target));
                                } else {
                                    jQuery('.center-box.more .box-top input.grand_total').val(App.format(data.total_all_center));
                                }
                                if (data.total_user_center_target) {
                                    jQuery('.center-box.more .box-top input.myCenter').val(App.format(data.total_user_center) + ' of ' + App.format(data.total_user_center_target));
                                } else {
                                    jQuery('.center-box.more .box-top input.myCenter').val(App.format(data.total_user_center));
                                }
                                if (data.htmlCenter) {
                                    jQuery('.center-box.more #center .box-content').html(data.htmlCenter);
                                }
                                if (data.htmlUser) {
                                    jQuery('.center-box.more #person .box-content').html(data.htmlUser);
                                }
                                if (data.number_top_user) {
                                    jQuery('.center-box.more #person .label.mine').html('mine (' + data.number_top_user + ')');
                                }
                                if (data.htmlCenterTop != '') {
                                    jQuery('.center-box.more #center .box-top .group-box.center-top ').html(data.htmlCenterTop);
                                }
                                if (data.number_top_center > 0) {
                                    jQuery('.center-box.more #center .label.center').html('my Center (' + data.number_top_center + ')');
                                }
                                if (data.my_region != '') {
                                    jQuery('.center-box.more #region .box-top .group-box.region ').html(data.my_region);
                                }
                                if (data.my_region_top != '') {
                                    jQuery('.center-box.more #region .box-top .group-box.region-top ').html(data.my_region_top);
                                }
//                            alert(data.htmlUserTop);
                                if (data.htmlUserTop != '') {
                                    jQuery('.center-box.more #person .box-top .persontop').html(data.htmlUserTop);
                                }
                                if (data.htmlRegion != '') {
                                    jQuery('.center-box.more #region .box-content').html(data.htmlRegion);
                                }
                                if (data.htmlUserCollections != '') {
                                    jQuery('.center-box.more #collections .box-content').html(data.htmlUserCollections);
                                }
                                if (data.number_collections_on_top) {
                                    jQuery('.center-box.more #collections .label.mine').html('mine (' + data.number_collections_on_top + ')');
                                }
                                return false;
                            }
                        }
                    }
                },
                error: function () {
//                alert("an error occurred, please try again");
                    location.reload();
                }
            });
        }
    }
    return false;
};

/*
 * format
 * Ham dinh dang tien te
 * App.format(1000);
 * =>1,000
 */
App.format = function (number) {

    var decimalSeparator = ".";
    var thousandSeparator = ",";
    // make sure we have a string
    var result = String(number);
    // split the number in the integer and decimals, if any
    var parts = result.split(decimalSeparator);
    // if we don't have decimals, add .00
//    if (!parts[1]) {
//      parts[1] = "00";
//    }
//  
    // reverse the string (1719 becomes 9171)
    result = parts[0].split("").reverse().join("");
    // add thousand separator each 3 characters, except at the end of the string
    result = result.replace(/(\d{3}(?!$))/g, "$1" + thousandSeparator);
    // reverse back the integer and replace the original integer
    parts[0] = result.split("").reverse().join("");
    // recombine integer with decimals
    return parts.join(decimalSeparator);
};

/*
 * pageMore
 * chuyen huong qua page more
 */
App.pageMore = function () {
    var email = jQuery("#formSendCenter .email").val();
    var center_select = jQuery("#formSendCenter option:selected").val();
    document.cookie = "center_more=" + center_select + " ; path=/";
    document.cookie = "email_more=" + email + " ; path=/";
    window.location.href = "/more";
};

/*
 * readCookie
 * Ham doc cookie
 * App.readCookie('key');
 */
App.readCookie = function (name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
};

/*
 * loadContact
 * Auto dien ten va email vao from lien he
 */
App.loadContact = function () {

    var cookiemail = App.readCookie('email_center');
//    alert(cookiemail);
    jQuery.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: {action: 'loadContact', 'email': cookiemail},
        success: function (response) {
            var $email = jQuery("input[name=et_pb_contact_email_1]");
            var $name = jQuery("input[name=et_pb_contact_name_1]");
            var data = jQuery.parseJSON(response);
            if (data) {
                if (data.name != null && data.email != null) {
                    $email.val(data.email);
                    $name.val(data.name);
                }
            }
        }
    });
};

/*
 * redirectNoCookie
 * Tu chuyen huong sang page welcome luc moi vao
 */
App.redirectNoCookie = function () {
    var cookiemail = App.readCookie('email_center');
    var initCookies = App.readCookie('init');
    if (cookiemail == null && initCookies == null) {
//        window.location.href = '/welcome';
//        document.cookie = "init= 1; path=/";
    }
};

/*
 * addNewCenter
 * chuyen huong sang add center tren page count
 */
App.addNewCenter = function (check) {
    if (check == 1) {
        if (confirm("To change your center, please click OK to go to Profile. To keep your center, please click CANCEL.")) {
            window.location.href = "/profile";
            return false;
        } else {
            var prev = jQuery('.center_filter').data('val');
            jQuery(".center_filter").val(prev);
            return false;
        }
    } else {
        var center_select = jQuery(".center_filter option:selected").val();
        if (center_select == "add") {
            jQuery('.add_new_center').show();
        } else {
            jQuery('.add_new_center').hide();
        }
    }

};

/*
 * ChangeLeaderboard
 * Change html user to Group
 */
App.ChangeLeaderboard = function () {
    if (jQuery(".leaderboard").is(':checked')) {
        jQuery(".description").show(); // checked
        jQuery("label.myname").html('my Group name');
    } else {
        jQuery(".description").hide(); // unchecked
        jQuery(".group-description").val('');
        jQuery("label.myname").html('my Displayname');
    }
}

/*
 * formAddCenter
 * add new center tu profile
 */
App.formAddCenter = function () {
    var region = jQuery(".region_filter option:selected").val();
    var center_name = jQuery(".center-name").val();
    var email = jQuery(".email").val();
    var emailRegex = new RegExp(/^([\w\.\-]+)@([\w\-]+)((\.(\w){2,10})+)$/i);
    var isvalid = emailRegex.test(email);
    if (email.length == 0) {
        jQuery(".email").focus();
        jQuery(".email").addClass("required");
        alert("Please enter your email address");
        return false;
    } else {
        if (!isvalid) {
            jQuery(".email").focus();
            jQuery(".email").addClass("required");
            alert("Email address is not valid!");
            return false;
        }
    }
    if (center_name.length == 0) {
        jQuery(".center-name").focus();
        jQuery(".center-name").addClass("required");
        alert("Please enter your center name");
        return false;
    }
    if (region.length == 0) {
        alert("Please select your region");
        jQuery(".region_filter").addClass("required");
        return false;
    }
    jQuery.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: {action: 'addNewCenter', region: region, 'center_name': center_name, 'email': email},
        beforeSend: function () {
            jQuery('#formAddCenter .submit.addcenter').html('<a class="button" href="javascript:;">Processing...</a>');
//            jQuery('.info').css({"display": "none"});
        },
        success: function (response) {
            if (response) {
                var data = jQuery.parseJSON(response);
                if (data.err == 0) {
                    jQuery('.center_filter').append(data.add);
                    jQuery('.center_filter').val(data.name);
                    jQuery('#formAddCenter .submit.addcenter').html('<a class="button" href="javascript:;" onclick="App.formAddCenter()">Add Center!</a>');
                    jQuery('.add_new_center').hide();
                    jQuery(".center-name").val('');
                    jQuery('.notice').html(data.msg).css({
                        'text-align': 'center',
                        'font-weight': 'bold',
                        'color': 'blue'
                    });
                    jQuery('html, body').animate({scrollTop: 0}, 800);
//                    location.reload();
                } else {
                    jQuery('#formAddCenter .submit.addcenter').html('<a class="button" href="javascript:;" onclick="App.formAddCenter()">Add Center!</a>');
                    jQuery('.notice').html(data.msg).css({
                        'text-align': 'center',
                        'font-weight': 'bold',
                        'color': 'red'
                    });
                    jQuery('html, body').animate({scrollTop: 0}, 800);
                    return false;
                }
            }
        },
        error: function () {
//                    alert("an error occurred, please try again");
            location.reload();
        }
    });
};

/*
 * formAddPractices
 * add new Practices tu profile
 */
App.formAddPractices = function () {
    var Practices = jQuery("#practices .practice_filter option:selected").val();
    if (Practices.length == 0) {
        alert("Please select your practices");
        return false;
    }
    if (Practices == 'add') {
        alert("Please add practices or select your practices!");
        return false;
    }
    jQuery.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: {action: 'formAddPractices', practices: Practices},
        beforeSend: function () {
            jQuery('.submit.addpractices').html('<a class="button" href="javascript:;">Processing...</a>');
        },
        success: function (response) {
            if (response) {
//                console.log(response)
//                return false;
                var data = jQuery.parseJSON(response);
                if (data.err == 0) {
                    jQuery('.your-practices').append(data.html);
//                    jQuery("#practices .practice_filter option:first").val();
                    jQuery('.submit.addpractices').html('<a class="button addpractices" href="javascript:;" onclick="App.formAddPractices()">Add</a>');
                    jQuery("html, body").animate({scrollTop: jQuery(document).height()}, 1000);
                    jQuery("#practices .practice_filter option:selected").removeAttr("selected");
                    App.editPractices(data.practices_name, data.count_practice, 0);
//                    jQuery('.add_new_center').hide();
//                    jQuery('.notice').html(data.msg).css({'text-align': 'center', 'font-weight': 'bold', 'color': 'blue'});
//                    jQuery('html, body').animate({scrollTop: 0}, 800);
//                    location.reload();
                    return false;
                } else {
                    alert(data.msg);
                    jQuery('.submit.addpractices').html('<a class="button addpractices" href="javascript:;" onclick="App.formAddPractices()">Add</a>');
                    return false;
                }
            }
        },
        error: function () {
//                    alert("an error occurred, please try again");
            location.reload();
        }
    });
};

/*
 * editPractices
 * edit Practices trong profile
 */
App.editPractices = function (name, key, count) {
    var acc = jQuery('.accumulation_filter').val();
    jQuery.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: {action: 'editPractices', name: name, key: key, count: count, acc: acc},
        beforeSend: function () {
            jQuery('.row .practices_button.details .load').css({"display": "none"});
            jQuery('.row .practices_button.details .details').css({"display": "inline-block"});
            jQuery('.row.' + key + ' .practices_button.details .details').hide();
            jQuery('.row.' + key + ' .practices_button.details .load').css({"display": "inline-block"});
        },
        success: function (response) {
            if (response) {
                // console.log(response)
                // return false;
                var data = jQuery.parseJSON(response);
                if (data.err == 0) {
                    jQuery('.your-practices .row .main-content').show();
                    jQuery('.your-practices .row .edit-content').slideUp("slow").html('');
                    jQuery('.your-practices .row.' + data.key + ' .main-content').hide();
                    jQuery('.your-practices .row.' + data.key + ' .edit-content').html(data.html).slideDown("slow");
                    return false;
                } else {
                    alert(data.msg);
//                    jQuery('.submit.addpractices').html('<a class="button addpractices" href="javascript:;" onclick="App.formAddPractices()">Add</a>');
                    return false;
                }
            }
        },
        error: function () {
//                    alert("an error occurred, please try again");
            location.reload();
        }
    });
};

/*
 * cancelPractices
 * Huy chinh sua practices
 */
App.cancelPractices = function (name, key, count) {
    jQuery('.row .practices_button.details .load').css({"display": "none"});
    jQuery('.row .practices_button.details .details').css({"display": "inline-block"});
    jQuery('.your-practices .row .main-content').show();
    jQuery('.your-practices .row .edit-content').slideUp("slow").html('');
    return false;
};

/*
 * deletePractices
 * xoa 1 Practices trong profile
 */
App.deletePractices = function (name, key, userid) {
    if (confirm("Are you sure you want to archive this practice? If you do, you cannot enter counts anymore.")) {
        var Accumulation_current = jQuery(".accumulation_filter option:selected").val();
        jQuery.ajax({
            type: 'POST',
            url: '/wp-admin/admin-ajax.php',
            data: {
                action: 'deletePractices',
                name: name,
                key: key,
                userid: userid,
                accumulation_current: Accumulation_current
            },
            beforeSend: function () {
                jQuery('.submit.delete').html('<a class="button" href="javascript:;">Processing...</a>');
            },
            success: function (response) {
                if (response) {
//                console.log(response)
//                return false;
                    var data = jQuery.parseJSON(response);
                    if (data.err == 0) {
                        if (data.html != '') {
                            jQuery('.your-practices').html(data.html);
                        }
                        return false;
                    } else {
                        alert(data.msg);
//                    jQuery('.submit.addpractices').html('<a class="button addpractices" href="javascript:;" onclick="App.formAddPractices()">Add</a>');
                        return false;
                    }
                }
            },
            error: function () {
//                    alert("an error occurred, please try again");
                location.reload();
            }
        });
    } else {
        // location.reload();
        return false;
    }
};


//  onChangeMyPracties
//  updatePractices
//  update data Practices trong profile

App.onChangeMyPracties = function (name, key, userid) {

    if (App.getHasChanges()) {
        if (confirm("Update the change now ?")) {
            App.updatePractices(name, key, userid);
        }
    } else {
        App.Accumulation(4);
    }
//    if (confirm("Update the change now ?")) {
//
//        var Practices = jQuery(".edit-practices-box .practice_filter option:selected").val();
//        var description = jQuery("#practices .practice-description").val();
//        var starting_count = jQuery("#practices .starting-count").val().replace(/,/g, "");
//        var target = jQuery("#practices .target-practice").val().replace(/,/g, "");
//        if (Practices.length == 0) {
//            alert("Please select your practices");
//            return false;
//        }
//        if (!jQuery.isNumeric(starting_count)) {
//            alert("Enter numeric, Please");
//            jQuery('#practices .starting-count').val('').focus();
//            return false;
//        }
//        if (!jQuery.isNumeric(target)) {
//            alert("Enter numeric, Please");
//            jQuery('#practices .target-practice').val('').focus();
//            return false;
//        }
//        var Accumulation = jQuery(".accumulation_filter2 option:selected").val();
//        var Accumulation_current = jQuery(".accumulation_filter option:selected").val();
//        jQuery.ajax({
//            type: 'POST',
//            url: '/wp-admin/admin-ajax.php',
//            data: {
//                action: 'updatePractices',
//                accumulation_current: Accumulation_current,
//                accumulation: Accumulation,
//                name: name,
//                key: key,
//                userid: userid,
//                description: description,
//                starting_count: starting_count,
//                target: target,
//                Practices: Practices
//            },
//            success: function (response) {
//                App.Accumulation(4);
//            },
//            error: function () {
////                    alert("an error occurred, please try again");
//                location.reload();
//            }
//        });
//    }
//    App.Accumulation(4);
//    return false;
}

/*
 * updatePractices
 * update data Practices trong profile
 * name prevacties_name
 */
App.updatePractices = function (name, key, userid) {
    var Accumulation = jQuery(".edit-practices-box .accumulation_filter2 option:selected").val();
    var Practices = jQuery(".edit-practices-box .practice_filter option:selected").val();
    var description = jQuery("#practices .practice-description").val();
    var starting_count = jQuery("#practices .starting-count").val().replace(/,/g, "");
    var target = jQuery("#practices .target-practice").val().replace(/,/g, "");
    if (Practices.length == 0) {
        alert("Please select your practices");
        return false;
    }
    if (!jQuery.isNumeric(starting_count)) {
        alert("Enter numeric, Please");
        jQuery('#practices .starting-count').val('').focus();
        return false;
    }
    if (!jQuery.isNumeric(target)) {
        alert("Enter numeric, Please");
        jQuery('#practices .target-practice').val('').focus();
        return false;
    }
    var Accumulation = jQuery(".accumulation_filter2 option:selected").val();//bo loc duoc chon
    // console.log("Loc dang selected " + Accumulation);
    var Accumulation_current = jQuery(".accumulation_filter option:selected").val();//bo loc hien tai
    // console.log("Loc current " + Accumulation_current);
    jQuery.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: {
            action: 'updatePractices',
            accumulation_current: Accumulation_current,
            accumulation: Accumulation,
            name: name,
            key: key,
            userid: userid,
            description: description,
            starting_count: starting_count,
            target: target,
            Practices: Practices
        },
        beforeSend: function () {
            jQuery('.practices_button.update').html('<a class="button" href="javascript:;">Processing...</a>');
        },
        success: function (response) {
            if (response) {
                // console.log(response)
                // return false;
                var data = jQuery.parseJSON(response);
                if (data.err == 0) {
                    if (data.html_practice != '') {
                        jQuery('.your-practices').html(data.html_practice);
                        App.Accumulation(3);
                    }
                    App.cancelPractices();
//                    if (data.html != '') {
//                        jQuery('.your-practices').html(data.html);
//                    }
                    return false;
                } else {
                    alert(data.msg);
                    App.cancelPractices();
//                    jQuery('.submit.addpractices').html('<a class="button addpractices" href="javascript:;" onclick="App.formAddPractices()">Add</a>');
                    return false;
                }
            }
        },
        error: function () {
//                    alert("an error occurred, please try again");
            location.reload();
        }
    });
};

/*
 * updateOverallTarget
 * update data Practices trong profile
 * name prevacties_name
 */
App.updateOverallTarget = function (name, key, userid) {
    var Accumulation = jQuery(".accumulations_select .accumulation_filter option:selected").val();
    var target = jQuery(".overall-target-box .overall-target").val().replace(/,/g, "");

    if (!jQuery.isNumeric(target) || target == 0) {
        alert("Enter numeric, Please");
        jQuery('.overall-target-box .overall-target').val('').focus();
        return false;
    }

    jQuery.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: {
            action: 'updateOverallTarget',
            accumulation: Accumulation,
            target: target,
        },
        beforeSend: function () {
            jQuery('.overall-target-box .update').html('<a class="button" href="javascript:;">Processing...</a>');
        },
        success: function (response) {
            if (response) {
                // console.log(response)
                // return false;
                var data = jQuery.parseJSON(response);
                if (data.err == 0) {
                    jQuery('.overall-target-box').addClass('exit-data');
                    jQuery('.overall-target-box .update').html('<a class="button update-overall-target" href="javascript:;" onclick="App.updateOverallTarget()">Update</a>');
                    jQuery('.overall-target-box .overall-target').attr("disabled", "disabled");
                    return false;
                } else {
                    jQuery('.overall-target-box .overall-target').removeAttr("disabled", "disabled");
                    jQuery('.overall-target-box').removeClass('exit-data');
                }
            }
        },
        error: function () {
//                    alert("an error occurred, please try again");
            location.reload();
        }
    });
};

/*
 * addNewPractice
 */
App.formAddPractice = function () {
    var practice_name = jQuery("#formAddPractice .practice-name").val();
    if (practice_name.length == 0) {
        jQuery("#formAddPractice .practice-name").focus();
        alert("Please enter your practice name");
        return false;
    }
    jQuery.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: {action: 'addNewPractice', practice_name: practice_name},
        beforeSend: function () {
            jQuery('#formAddPractice .submit.addnewpractice').html('<a class="button" href="javascript:;">Processing...</a>');
//            jQuery('.info').css({"display": "none"});
        },
        success: function (response) {
//            console.log(response)
//            return false;
            if (response) {
                var data = jQuery.parseJSON(response);
                if (data.err == 0) {
                    jQuery(".practice_filter").append(data.add);
                    jQuery(".practice_filter").val(data.practice_name);
                    jQuery("#formAddPractice .practice-name").val('');
                    jQuery('#formAddPractice .submit.addnewpractice').html('<a class="button" href="javascript:;" onclick="App.formAddPractice()">Add Practice!</a>');
                    jQuery('#formAddPractice').hide();
//                    jQuery('.notice').html(data.msg).css({'text-align': 'center', 'font-weight': 'bold', 'color': 'blue'});
                    jQuery('html, body').animate({scrollTop: 0}, 800);
//                    location.reload();
                } else {
                    alert(data.msg);
                    jQuery('#formAddPractice .submit.addnewpractice').html('<a class="button" href="javascript:;" onclick="App.formAddPractice()">Add Practice!</a>');
//                    jQuery('.notice').html(data.msg).css({'text-align': 'center', 'font-weight': 'bold', 'color': 'red'});
//                    jQuery('html, body').animate({scrollTop: 0}, 800);
                    return false;
                }
            }
        },
        error: function () {
//                    alert("an error occurred, please try again");
            location.reload();
        }
    });
};

//Save old data Practice on change
App.saveOldPractice = function () {
    jQuery(".practice_filter").focus(function () {
        jQuery(".old_data").val(jQuery(this).val());
    });
}

/*
 * showAddPractice
 * hien thi tab Practice khi chuyen huong sang profile
 */
App.showAddPractice = function () {
    var practices = jQuery(".practice_filter").val();
    var accumulation = jQuery(".accumulation_filter").val();
    var check_count = jQuery(".check_count").val();
    var old_data = jQuery('.old_data').val()
    var manisdone = jQuery('.manis-done').val()

    if (check_count == "input") {
        if (practices == "add") {
            jQuery('#formAddPractice').show();
        } else {
            jQuery('#formAddPractice').hide();
        }
        if (practices == "More") {
            window.location.href = "/profile/?tab=practices";
        } else if (practices == "") {
            var cookiemail = App.readCookie('email_center');
            var center_select = jQuery("#formSendCenter option:selected").val();
            // console.log(23123);
            App.initCheck(cookiemail, center_select);
        } else {
            App.loadCountPractice(practices, accumulation);
        }
    } else {
        if (manisdone > 0) {
            var r = confirm("The process of counting is going on. Do you want to save it?");
            if (r == true) {
                var data = jQuery('form#formSendCenter').serializeArray();
                App.AccumulationCountLiveChange(data, old_data);
                App.loadCountPractice(practices, accumulation);
            } else {
                jQuery(".practice_filter").val(old_data);
                return false;
            }
        } else {
            App.loadCountPractice(practices, accumulation);
        }
    }

};

App.showAddPractice_registration = function () {

    // jQuery(".show-count").show();
    var practices = jQuery(".practice_filter").val();
    if (practices == "add") {
        jQuery('#formAddPractice').show();
    } else {
        jQuery('#formAddPractice').hide();
    }
};

// load data in page count if onchange practice

App.loadCountPractice = function (practices, accumulation) {
    if (practices) {
        jQuery.ajax({
            type: 'POST',
            url: '/wp-admin/admin-ajax.php',
            data: {action: 'getOnchangeData', 'practices': practices, accumulation: accumulation},
            beforeSend: function () {
                jQuery('body').append('<div class="custom_loading"></div>');
            },
            success: function (response) {
                jQuery('.custom_loading').remove();
                if (response) {
                    var data = jQuery.parseJSON(response);
                    if (data.err == 0) {
                        jQuery('.remaining_daily_box').css({"display": "none"});
//                        jQuery('.sharebox-count').fadeIn();
                        if (data.center_select != '') {
                            jQuery('.center_filter').val(data.center_select);
                        }
                        jQuery('input.remaining_all').val('');
                        if (data.show_total_all > 0 && data.targetAll > 0) {
                            jQuery('.remaining_box').css({"display": "block"});
                            jQuery('.remaining_daily_box').addClass("dis-none");
                            if (data.total_all > 0) {
                                jQuery('input.remaining').val(App.format(data.total_all));
                                jQuery('input.remaining_all').val(App.format(data.total_all));
                            } else {
                                jQuery('input.remaining').val("DONE");
                            }
                        } else {
                            jQuery('.remaining_box').css({"display": "none"});
                            jQuery('.remaining_daily_box').removeClass("dis-none");
                        }
                        if (data.left_today > 0) {
                            jQuery('input.left_today').val(App.format(data.left_today));
                            jQuery('input.left_today_2').val(App.format(data.left_today));
                        } else {
                            jQuery('input.left_today').val("DONE");
                        }
                        if (data.show_days_left == 1) {
                            jQuery('.left_today_box').css({"display": "inline-block"});
                        } else {
                            // jQuery('.left_today_box').css({"display": "none"});
                            jQuery('.left_today_box').css({"display": "block"});
                        }
                        if (data.show_remaining_daily == 1 && data.remaining_daily > 0) {
                            console.log(data);
                            jQuery('.remaining_daily_box').css({"display": "inline-block"});
                            jQuery('input.remaining_daily').val(App.format(data.remaining_daily));
                            jQuery('input.personal_target').val(App.format(data.total_user_target));
                            jQuery('input.running_total_yesterday').val(App.format(data.running_total_yesterday));
                            jQuery('input.personal_starting_count').val(App.format(data.personal_starting_count));
                            jQuery('input.days_left').val(App.format(data.days_left));
                            jQuery('input.day_count_today').val(App.format(data.day_count_today));
                        } else {
                            jQuery('.remaining_daily_box').css({"display": "inline-block"});
                            jQuery('input.remaining_daily').val("DONE");

                        }
                        if (data.total_user_target > 0) {
                            jQuery('.remaining_daily_box').removeClass('w-100');
                            jQuery('.remaining_daily_box a').css({"display": "none"});
                            jQuery('.remaining_daily_box #remaining_daily,.remaining_daily_box .label').css({"display": "block"});
                        } else {
                            jQuery('.remaining_daily_box').css({"display": "inline-block"}).addClass('w-100');
                            jQuery('.remaining_daily_box a').css({"display": "inline-block"});
                            jQuery('.remaining_daily_box #remaining_daily,.remaining_daily_box .label,.left_today_box ').css({"display": "none"});
                        }
                        if (data.email != '') {
                            document.cookie = "email_center=" + data.email + " ; path=/";
                        }
//                    console.log(data.running_total_yesterday);
//                    console.log(data.daily_target);
                        jQuery('input.id').val(data.id);
                    } else {
                        if (data.left_today > 0) {
//                        jQuery('.left-today-box').css({"display": "block"});
                            jQuery('input.left_today').val(App.format(data.left_today));
                            jQuery('input.left_today_2').val(App.format(data.left_today));
                        } else {
                            jQuery('input.left_today').val("DONE");
                        }
                        //jQuery('.remaining_daily_box').css({"display": "none"});
                        jQuery('.info').css({"display": "none"});
                        jQuery('input.total_user').val('');
                        jQuery('input.total_center').val('');
                        jQuery('input.total_all_center').val('');
                        jQuery('input.id').val('');
                        if (data.center_select != '') {
                            //  jQuery('.center_filter').val(data.center_select).attr("disabled", "disabled");
                            jQuery('.center_filter').val(data.center_select);
//                        jQuery("option:selected").removeAttr("selected");
                        } else {
                            jQuery('.center_filter').val(data.center_select).removeAttr("disabled", "disabled");
                        }
                        if (data.practice_select != '') {
                            jQuery('.practice_filter').replaceWith(data.practice_select);
//                       jQuery("option:selected").removeAttr("selected");
                        }
                        if (data.show_days_left == 1) {
                            jQuery('.left_today_box').css({"display": "inline-block"});
                        } else {
                            jQuery('.left_today_box').css({"display": "block"});
                        }
                    }
                } else {
                    jQuery('.remaining_daily_box').css({"display": "none"});
                    jQuery('.info').css({"display": "none"});
                    jQuery('input.total_user').val('');
                    jQuery('input.total_center').val('');
                    jQuery('input.total_all_center').val('');
                    jQuery('input.id').val('');
                    if (data.show_remaining_daily == 1) {
                        jQuery('.show_remaining_box').css({"display": "inline-block"});
                        jQuery('input.remaining_daily').val(App.format(data.remaining_daily));
                    } else {
                        jQuery('.left_today_box').css({"display": "none"});
                    }
                    if (data.show_days_left == 1) {
                        jQuery('.left_today_box').css({"display": "block"});
                    } else {
                        // jQuery('.left_today_box').css({"display": "none"});
                    }
                    if (data.left_today > 0) {
//                        jQuery('.left-today-box').css({"display": "inline-block"});
                        jQuery('input.left_today').val(App.format(data.left_today));
                        jQuery('input.left_today_2').val(App.format(data.left_today));
                    } else {
                        jQuery('input.left_today').val("DONE");
                    }
                }
            },
            error: function () {
                location.reload();
            }
        });
    }
    return false;
};

/*
 * formCancelPractice
 * huy add Practice
 */
App.formCancelPractice = function () {
    jQuery("option:selected").removeAttr("selected");
    jQuery('#formAddPractice').hide();
};

/*
 * deleteDedication
 * Xoa bai viet tren Dedication
 */
App.deleteDedication = function (id, name) {

    if (confirm("Are you sure you want to delete this dedication?")) {
        jQuery.ajax({
            type: 'POST',
            url: '/wp-admin/admin-ajax.php',
            data: {action: 'deleteDedication', id: id, name: name},
            beforeSend: function () {
                jQuery('.btn-showupload.delete').text('Delete');
                jQuery('.btn-showupload.delete-' + id).text('Processing...');
            },
            success: function (response) {
                if (response) {
//                console.log(response)
//                return false;
                    var data = jQuery.parseJSON(response);
                    if (data.err == 0) {
                        if (data.html != '' && name == 1) {
                            jQuery('#list_public').html(data.html);
                            return false;
                        }
                        if (data.html != '' && name == 2) {

                            jQuery('#list_private').html(data.html);
                            return false;
                        }
                    } else {
                        location.reload();
//                        alert(data.msg);
//                    jQuery('.submit.addpractices').html('<a class="button addpractices" href="javascript:;" onclick="App.formAddPractices()">Add</a>');
                        return false;
                    }
                }
            },
            error: function () {
//                    alert("an error occurred, please try again");
                location.reload();
            }
        });
    } else {
        // location.reload();
        return false;
    }
};

/*
 * editDedication
 * Sua bai viet tren Dedication
 */
App.editDedication = function (id, name) {
    jQuery('.et_pb_all_tabs .et_pb_tab_1,.et_pb_all_tabs .et_pb_tab_0').css({"display": "none"});
    jQuery('.et_pb_all_tabs .et_pb_tab_2').css({"display": "block"});
    jQuery('.et_pb_tabs_controls  .et_pb_tab_0,.et_pb_tabs_controls  .et_pb_tab_1').removeClass('et_pb_tab_active');
    jQuery('.et_pb_tabs_controls  .et_pb_tab_2').addClass('et_pb_tab_active');
    jQuery('html, body').animate({scrollTop: 0}, 800);
    jQuery.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: {action: 'editDedication', id: id, name: name},
        beforeSend: function () {
//            jQuery('.btn-showupload.delete').text('Delete');
//            jQuery('.btn-showupload.delete-' + id).text('Processing...');
        },
        success: function (response) {
            if (response) {
//                console.log(response)
//                return false;
                var data = jQuery.parseJSON(response);
                if (data.err == 0) {
                    if (data.id != '') {
                        jQuery("#formDedications .id").val(data.id);
//                        return false;
                    }
                    if (data.type != '') {
                        jQuery("#formDedications .type").val(data.type);
//                        return false;
                    }
                    if (data.subject != '') {
                        jQuery("#formDedications .form-dedication-subject").val(data.subject);
//                        return false;
                    }
                    if (data.description != '') {
                        jQuery("#formDedications .dedication-description").val(data.description);
//                        return false;
                    }
                    if (data.date != '') {
                        jQuery("#formDedications .form-dedication-date").val(data.date);
//                        return false;
                    }
                    if (data.image != '') {
                        jQuery("#formDedications .Picture").html(data.image);
//                        return false;
                    }
                    if (data.visibilit != '') {
//                       jQuery("input[name='form-dedication-visibilit']").val(data.visibilit);
                        jQuery("input[name=form-dedication-visibilit][value=" + data.visibilit + "]").attr('checked', 'checked');
//                        return false;
                    }
                } else {
                    location.reload();
//                        alert(data.msg);
//                    jQuery('.submit.addpractices').html('<a class="button addpractices" href="javascript:;" onclick="App.formAddPractices()">Add</a>');
                    return false;
                }
            }
        },
        error: function () {
//                    alert("an error occurred, please try again");
            location.reload();
        }
    });
};

/*
 * initDedications
 * Add Dedications to post
 */
App.initDedications = function () {
    var initDedications = jQuery('#formDedications');
    if (initDedications.length > 0) {
        initDedications.on('submit', (function (event) {
            event.preventDefault();
            var subject = jQuery("#formDedications .form-dedication-subject").val();
            var description = jQuery("#formDedications .dedication-description").val();
            var date = jQuery("#formDedications .form-dedication-date").val();
            var type = jQuery("#formDedications .type").val();
            var file = jQuery("#formDedications .form-dedication-image").val();
            var visibilit = jQuery("input[name='form-dedication-visibilit']:checked").val()
            if (subject.length == 0) {
                jQuery("#formDedications .form-dedication-subject").focus();
                alert("Please enter your subject");
                return false;
            }
            if (description.length == 0) {
                jQuery("#formDedications .dedication-description").focus();
                alert("Please enter your description");
                return false;
            }
            if (date.length == 0) {
                jQuery("#formDedications .form-dedication-date").focus();
                alert("Please enter your date");
                return false;
            }
            if (!visibilit) {
                alert("Please choose one option Private or Public");
                return false;
            }
            if (file) {
                var ext = file.split('.').pop().toLowerCase();
                console.log(ext);
                if (jQuery.inArray(ext, ['gif', 'png', 'jpg', 'jpeg', 'tiff']) == -1) {
                    alert("You have selected a file that doesn't look like an image. For security reasons please only upload JPEG, GIF, PNG, or TIFF files. Thank you.");
                    return false;
                }
            }

            var datas = new FormData(this);
            jQuery.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                contentType: false,
                processData: false,
                cache: false,
                data: datas,
                beforeSend: function () {
                    jQuery('#formDedications .submit.dedication .add').text('Processing...');
                },
                success: function (response) {
//                    console.log(response);
//                    return false;
                    if (response) {
                        var data = jQuery.parseJSON(response);
                        if (data.err == 0) {
                            jQuery('#formDedications .submit.dedication .add').text('Update');
//                            alert(data.msg);
                            location.reload();
                            return false;
                        } else {
                            jQuery('#formDedications .submit.dedication .add').text('Update');
//                            alert(data.msg);
                            location.reload();
                            return false;
                        }
                    }
                },
                error: function () {
//                    alert("an error occurred, please try again");
                    location.reload();
                }
            });
        }));
    }
    return false;
};

/*
 * Accumulation
 * Xu ly get data cua bo loc tra ve
 * Accumulation = 1: loc o trang count
 * Accumulation = 2: loc o trang more
 * Accumulation = 3: loc o trang profile
 * Accumulation = 4: loc chi tiet practices
 */
App.Accumulation = function (key) {
    var practice_filter = jQuery("#formSendCenter .practice_filter option:selected").val();

    if (key == 4) {
        var Accumulation = jQuery(".accumulation_filter2 option:selected").val();
        var data = jQuery(".practice_data").val();
    } else {
        var Accumulation = jQuery(".accumulation_filter option:selected").val();
    }
    document.cookie = "accumulation=" + Accumulation + " ; path=/";
    jQuery.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: {action: 'Accumulation', key: key, id: Accumulation, data: data, practice_filter: practice_filter},
        beforeSend: function () {
            if (key != 4) {
                jQuery('.info').css({"display": "none"});
                jQuery('.et_pb_row.center-box.more.more_func .tab-content.et_pb_tabs').css({"display": "none"});
                jQuery('.your-practices,.overall-target-box').css({"display": "none"});
                jQuery('.image-loading').css({"display": "block"});
            }
        },
        success: function (response) {
            // console.log(response);
            // return false;
            if (response) {
                var data = jQuery.parseJSON(response);
                if (data.err == 0) {
                    if (data.type == 1) {
                        App.Accumulation_count(data);
                    }
                    if (data.type == 2) {
                        App.Accumulation_more(data);
                    }
                    if (data.type == 3) {
                        App.Accumulation_profile(data);
                    }
                    if (data.type == 4) {
                        App.Accumulation_practices(data);
                    }

                }
            }
        },
        error: function () {
            location.reload();
        }
    });
}

/*
 * AccumulationCheck
 * get data cua bo loc khi load page
 * Accumulation = 1: loc o trang count
 * Accumulation = 2: loc o trang more
 * Accumulation = 3: loc o trang profile
 * Accumulation = 4: loc chi tiet practices
 */
App.AccumulationCheck = function () {
    var Accumulation = App.readCookie('accumulation');
    if (!Accumulation) {
        Accumulation = 0;
    }
    var key = jQuery(".accumulation_filter").data('key');
//
    jQuery.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: {action: 'Accumulation', key: key, id: Accumulation},
        beforeSend: function () {
//            window.alert(key);
            if (key != 4) {
                jQuery('.info').css({"display": "none"});
                jQuery('.et_pb_row.center-box.more.more_func .tab-content.et_pb_tabs').css({"display": "none"});
                jQuery('.your-practices').css({"display": "none"});
                jQuery('.image-loading').css({"display": "block"});
            }
        },
        success: function (response) {
            if (response) {
//                window.alert(1);
//                console.log(response)
//                return false;
                var data = jQuery.parseJSON(response);
                if (data.err == 0) {
                    if (data.type == 1) {
                        App.Accumulation_count(data);
                    }
                    if (data.type == 2) {
                        App.Accumulation_more(data);
                    }
                    if (data.type == 3) {
                        App.Accumulation_profile(data);
                    }
                    if (data.type == 4) {
                        App.Accumulation_practices(data);
                    }

                }
            }
        },
        error: function () {
            location.reload();
        }
    });
//    }
}

/*
 * Accumulation_count
 * Get data loc page count
 */

App.Accumulation_count = function (data) {
    jQuery('.info').css({"display": "block"});
    jQuery('.image-loading').css({"display": "none"});
    if (data.total_user_target) {
        jQuery('input.total_user').val(App.format(data.total_user) + ' of ' + App.format(data.total_user_target));
    } else {
        jQuery('input.total_user').val(App.format(data.total_user));
    }


    if (data.html_practice != '') {
        // jQuery('.center_filter').val(data.center_select).attr("disabled", "disabled");
        jQuery('.html_practice').html(data.html_practice);
//                       jQuery("option:selected").removeAttr("selected");
    } else {
        jQuery('.html_practice').html('');
    }
    // if (data.total_all_center_target) {
    //     jQuery('input.total_all_center').val(App.format(data.total_all_center) + ' of ' + App.format(data.total_all_center_target));
    // } else {
    //     jQuery('input.total_all_center').val(App.format(data.total_all_center));
    // }
    jQuery('input.total_all_center').val(App.format(data.total_all_center));

    // if (data.total_center_target) {
    //     jQuery('input.total_center').val(App.format(data.total_center) + ' of ' + App.format(data.total_center_target));
    // } else {
    //     jQuery('input.total_center').val(App.format(data.total_center));
    // }
    jQuery('input.total_center').val(App.format(data.total_center));
    if (data.center_select != '') {
        // jQuery('.center_filter').val(data.center_select);
    }
    if (data.left_today > 0) {
        jQuery('input.left_today').val(App.format(data.left_today));
        jQuery('input.left_today_2').val(App.format(data.left_today));
    } else {
        jQuery('input.left_today').val("DONE");
    }
    if (data.show_days_left == 1) {
        jQuery('.left_today_box').css({"display": "block"});
    } else {
        jQuery('.left_today_box').css({"display": "block"});
    }
    if (data.show_remaining_daily == 1 && data.remaining_daily > 0) {
        jQuery('.remaining_daily_box').css({"display": "block"});
        jQuery('input.remaining_daily').val(App.format(data.remaining_daily));
        jQuery('input.personal_target').val(App.format(data.total_user_target1));
        jQuery('input.running_total_yesterday').val(App.format(data.running_total_yesterday));
        jQuery('input.personal_starting_count').val(App.format(data.personal_starting_count));
        jQuery('input.days_left').val(App.format(data.days_left));
        jQuery('input.day_count_today').val(App.format(data.day_count_today));
    } else {
        jQuery('.remaining_daily_box').css({"display": "block"});
        jQuery('input.remaining_daily').val("DONE");

    }
    if (data.total_user_target1 > 0) {
        jQuery('.remaining_daily_box').removeClass('w-100');
        jQuery('.remaining_daily_box a').css({"display": "none"});
        jQuery('.remaining_daily_box #remaining_daily,.remaining_daily_box .label,#left_today_box ').css({"display": "block"});
    } else {
        jQuery('.remaining_daily_box').css({"display": "inline-block"}).addClass('w-100');
        jQuery('.remaining_daily_box a').css({"display": "inline-block"});
        jQuery('.remaining_daily_box #remaining_daily,.remaining_daily_box .label,.left_today_box').css({"display": "none"});
    }
    if (data.email != '') {
        document.cookie = "email_center=" + data.email + " ; path=/";
    }
//                    console.log(data.running_total_yesterday);
//                    console.log(data.daily_target);
    jQuery('input.remaining_all').val('');
    if (data.show_total_all > 0 && data.targetAll > 0) {
        jQuery('.remaining_box').css({"display": "block"});
        jQuery('.remaining_daily_box').addClass("dis-none");
        if (data.total_all > 0) {
            jQuery('input.remaining').val(App.format(data.total_all));
            jQuery('input.remaining_all').val(App.format(data.total_all));
        } else {
            jQuery('input.remaining').val("DONE");
        }
    } else {
        jQuery('.remaining_box').css({"display": "none"});
        jQuery('.remaining_daily_box').removeClass("dis-none");
    }

    if (data.left_today_all != null) {
        if (data.left_today_all > 0) {
            jQuery('.left_today_all_box').css({"display": "block"});
            jQuery('input.left_today_all').val(App.format(data.left_today_all));
            jQuery('input.left_today_all_2').val(App.format(data.left_today_all));
        } else {
            jQuery('.left_today_all_box').css({"display": "block"});
            jQuery('input.left_today_all').val("DONE");

        }
    } else {
        jQuery('.left_today_all_box').css({"display": "none"});
    }

    if (data.remaining_daily_all != null) {

        if (data.remaining_daily_all > 0) {
            jQuery('.remaining_daily_all_box').css({"display": "block"});
            jQuery('input.remaining_daily_all').val(App.format(data.remaining_daily_all));
            jQuery('input.remaining_daily_all_2').val(App.format(data.remaining_daily_all));
        } else {
            jQuery('.remaining_daily_all_box').css({"display": "block"});
            jQuery('input.remaining_daily_all').val("DONE");

        }
    } else {
        jQuery('.remaining_daily_all_box').css({"display": "none"});

    }
    jQuery('input.id').val(data.id);

}

/*
 * Accumulation_more
 * Get data loc page more
 */
App.Accumulation_more = function (data) {
    jQuery('.et_pb_row.center-box.more.more_func .tab-content.et_pb_tabs').css({"display": "block"});
    jQuery('.image-loading').css({"display": "none"});
    if (data.total_user_target) {
        jQuery('.center-box.more .box-top input.mine').val(App.format(data.total_user) + ' of ' + App.format(data.total_user_target));
    } else {
        jQuery('.center-box.more .box-top input.mine').val(App.format(data.total_user));
    }
    if (data.total_all_center_target) {
        jQuery('.center-box.more .box-top input.grand_total').val(App.format(data.total_all_center) + ' of ' + App.format(data.total_all_center_target));
    } else {
        jQuery('.center-box.more .box-top input.grand_total').val(App.format(data.total_all_center));
    }
    if (data.total_user_center_target) {
        jQuery('.center-box.more .box-top input.myCenter').val(App.format(data.total_user_center) + ' of ' + App.format(data.total_user_center_target));
    } else {
        jQuery('.center-box.more .box-top input.myCenter').val(App.format(data.total_user_center));
    }
    if (data.htmlCenter) {
        jQuery('.center-box.more #center .box-content').html(data.htmlCenter);
    } else {
        jQuery('.center-box.more #center .box-content').html('');
    }
    if (data.htmlUser) {
        jQuery('.center-box.more #person .box-content').html(data.htmlUser);
    } else {
        jQuery('.center-box.more #person .box-content').html('');
    }
    if (data.htmlPractic) {
        jQuery('.center-box.more #practice .box-content').html(data.htmlPractic);
    } else {
        jQuery('.center-box.more #practice .box-content').html('');
    }
    if (data.htmlPracticTop) {
        jQuery('.center-box.more #practice .group-box.practice').html(data.htmlPracticTop);
    } else {
        jQuery('.center-box.more #practice .group-box.practice').html('');
    }
    if (data.number_top_user) {
        jQuery('.center-box.more #person .label.mine').html('mine (' + data.number_top_user + ')');
    } else {
        jQuery('.center-box.more #person .label.mine').html('mine ');
    }
    if (data.htmlCenterTop != '') {
        jQuery('.center-box.more #center .box-top .group-box.center-top ').html(data.htmlCenterTop);
    } else {
        jQuery('.center-box.more #center .box-top .group-box.center-top ').html('');
    }
    if (data.number_top_center > 0) {
        jQuery('.center-box.more #center .label.center').html('my Center (' + data.number_top_center + ')');
    } else {
        jQuery('.center-box.more #center .label.center').html('my Center');
    }
    if (data.my_region != '') {
        jQuery('.center-box.more #region .box-top .group-box.region ').html(data.my_region);
    } else {
        jQuery('.center-box.more #region .box-top .group-box.region ').html('');
    }
    if (data.my_region_top != '') {
        jQuery('.center-box.more #region .box-top .group-box.region-top ').html(data.my_region_top);
    } else {
        jQuery('.center-box.more #region .box-top .group-box.region-top ').html('');
    }
    if (data.htmlUserTop != '') {
        jQuery('.center-box.more #person .box-top .persontop').html(data.htmlUserTop);
    } else {
        jQuery('.center-box.more #person .box-top .persontop').html('');
    }
    if (data.htmlRegion != '') {
        jQuery('.center-box.more #region .box-content').html(data.htmlRegion);
    } else {
        jQuery('.center-box.more #region .box-content').html('');
    }
    if (data.htmlUserCollections != '') {
        jQuery('.center-box.more #collections .box-content').html(data.htmlUserCollections);
    } else {
        jQuery('.center-box.more #collections .box-content').html('');
    }
    if (data.number_collections_on_top) {
        jQuery('.center-box.more #collections .label.mine').html('mine (' + data.number_collections_on_top + ')');
    } else {
        jQuery('.center-box.more #collections .label.mine').html('mine ');
    }
    return false;
}

/*
 * Accumulation_profile
 * Get data loc page profile
 */
App.Accumulation_profile = function (data) {
    jQuery('.image-loading').css({"display": "none"});
    jQuery('.your-practices,.overall-target-box').css({"display": "block"});
    if (data.html_practice != '') {
        jQuery('.your-practices').html(data.html_practice);
    }
    if (data.over_target > 0) {
        jQuery('.overall-target-box').addClass('exit-data');
        jQuery('.overall-target-box .overall-target').val(App.format(data.over_target)).attr("disabled", "disabled");
        return false;
    } else {
        jQuery('.overall-target-box .overall-target').val("").removeAttr("disabled", "disabled");
        jQuery('.overall-target-box').removeClass('exit-data');
    }
}

/*
 * Accumulation_practices
 * Get data loc page profile chi tiet practices
 */

App.Accumulation_practices = function (data) {
    console.log(data);

    jQuery('.edit-practices-box .count_practice').val(data.sum);
    if (data.check != 1) {
        jQuery('.edit-practices-box .target-practice,.edit-practices-box .starting-count').attr('disabled', 'disabled');
    } else if (data.check == 1 && data.starting_count > 0) {
        jQuery('.edit-practices-box .target-practice').removeAttr('disabled');
        jQuery('.edit-practices-box .starting-count').attr('disabled', 'disabled');
    } else {
        jQuery('.edit-practices-box .target-practice,.edit-practices-box .starting-count').removeAttr('disabled');
    }
//    } else {
//        jQuery('.edit-practices-box .starting_count_label b').text('Accumulated so far');
//    }
    if (App.format(data.target_number) != 'null') {
        jQuery('.edit-practices-box .target-practice').val(App.format(data.target_number));
    } else {
        jQuery('.edit-practices-box .target-practice').val(0);
    }
    if (data.taget_all == 1) {
        jQuery('.edit-practices-box .target-practice').removeAttr('disabled');
    }
    if (data.taget_all == 1 && data.target_number > 0) {
        jQuery('.edit-practices-box .target-practice').attr('disabled', 'disabled');
    }
    jQuery('.edit-practices-box .starting-count').val(App.format(data.starting_count));


};
/*
 * cancelInput
 * thong bao khi start count > 0
 */
App.cancelInput = function (data) {
    if (data > 0) {
        window.alert('Sorry, this field can only be used once. Please use the count page to enter a count, or contact us if you need further assistance. Thank you!');
        return false;
    }
}

App.getHasChanges = function () {
    var hasChanges = false;

    jQuery("input[name=target-practice],input[name=practice-description],input[name=starting-count]").each(function () {
        if ((this.type == "text" || this.type == "textarea" || this.type == "hidden") && this.defaultValue != this.value) {
            hasChanges = true;
            return false;
        } else {
            if ((this.type == "radio" || this.type == "checkbox") && this.defaultChecked != this.checked) {
                hasChanges = true;
                return false;
            } else {
                if ((this.type == "select-one" || this.type == "select-multiple")) {
                    for (var x = 0; x < this.length; x++) {
                        if (this.options[x].selected != this.options[x].defaultSelected) {
                            hasChanges = true;
                            return false;
                        }
                    }
                }
            }
        }
    });

    return hasChanges;
}
App.setCountLive = function () {
    var initFormCenter = jQuery('#formSendCenter');
    if (initFormCenter.length > 0) {
        var btnChangeValue = jQuery('.box-count-live .box-select a');
        var btnBig = jQuery(".btn-count-big");
        var practice_filter = jQuery("#formSendCenter .practice_filter option:selected").val();
        if (practice_filter.length != 0) {
            var value_count_live = App.readCookie('value_count_live');
            if (value_count_live > 0) {
                var value = value_count_live;
                btnChangeValue.removeClass('active');
                jQuery(".count-" + value).addClass('active');
                jQuery(btnBig).data("value", value);
                var width = jQuery('body').width();
                if (width > 540) {
                    jQuery(".btn-count-big span").html("Click 1 to add <br>" + value);
                } else {
                    jQuery(".btn-count-big span").html("Tap to add <br>" + value);
                }
                if (value > 1) {
                    jQuery(".btn-count-big").removeClass('type108-count1').removeClass('type108-count2').removeClass('type108-count3').removeClass('type108-count4');
                    jQuery(".btn-count-big").removeClass('type1-count1').removeClass('type1-count2').removeClass('type1-count3').removeClass('type1-count4');
                    jQuery(".btn-count-big").addClass('type108-count0');
                } else {
                    jQuery(".btn-count-big").removeClass('type108-count1').removeClass('type108-count2').removeClass('type108-count3').removeClass('type108-count4');
                    jQuery(".btn-count-big").removeClass('type1-count1').removeClass('type1-count2').removeClass('type1-count3').removeClass('type1-count4');
                    jQuery(".btn-count-big").addClass('type1-count0');
                }
            }
        }
    }
}

App.initCounLive = function () {
    var btnLive = jQuery('.btn-live .count-live');
    var btnLiveDone = jQuery('.btn-live .done-count-live');
    var btnChangeValue = jQuery('.box-count-live .box-select a');
    var btnBig = jQuery(".btn-count-big");
    var btnShowCount = jQuery(".show-box-count");
    btnLive.click(function (event) {
        jQuery('.accumulations_select,.info,.count-live,.submit.center,.show-count').addClass('hide');
        jQuery('.done-count-live,.box-count-live').removeClass('hide');
        jQuery('.box-change-count .left').addClass('full');
        jQuery('.box-count-home').addClass('click-box');
        jQuery('.check_count').val('button');
        if (jQuery(document).width() < 540) {
            var button = jQuery('.box-count-home').outerWidth();
            jQuery('.box-count-home.click-box .btn-count-big').css({"height": button + "px"});
        }

    });

    btnLiveDone.click(function (event) {
        var data = jQuery('form#formSendCenter').serializeArray();
        var practice_filter = App.getValueCountLive('practice_filter', data[4]);
        var email = App.getValueCountLive('email', data[0]);
        var emailRegex = new RegExp(/^([\w\.\-]+)@([\w\-]+)((\.(\w){2,10})+)$/i);
        var isvalid = emailRegex.test(email);
        var manisdone = App.getValueCountLive('manis-done', data[5]);

        if (email.length == 0) {
            jQuery(".email").focus();
            jQuery(".email").addClass("required");
            alert("Please enter your email address");
            return false;
        } else {
            if (!isvalid) {
                jQuery(".email").focus();
                jQuery(".email").addClass("required");
                alert("Email address is not valid!");
                return false;
            }
        }
        if (practice_filter.length == 0) {
            alert("Please select your practice");
            jQuery(".practice_filter").addClass("required");
            return false;
        }
        if (manisdone.length == 0 || manisdone.length == 1 && manisdone == 0) {
            jQuery(".manis-done").focus();
            jQuery(".manis-done").addClass("required");
            alert("Please enter your number");
            return false;
        }
        // jQuery('.accumulations_select,.info,.submit.center,.count-live').removeClass('hide');
        // jQuery('.done-count-live,.box-count-live').addClass('hide');
        // jQuery('.check_count').val('input');
        App.AccumulationCountLive(data);
    });

    btnChangeValue.click(function (event) {
        var practice_filter = jQuery("#formSendCenter .practice_filter option:selected").val();
        if (practice_filter.length == 0) {
            alert("Please select your practice");
            jQuery(".practice_filter").addClass("required");
            return false;
        }
        var value = jQuery(this).data("value");
        btnChangeValue.removeClass('active');
        jQuery(this).addClass('active');
        jQuery(btnBig).data("value", value);
        var count = parseInt(jQuery(btnBig).data("count"));
        var type = parseInt(jQuery(btnBig).data("type"));
        var old_class = "type" + type + "-count" + count;
        var new_class = "type" + value + "-count" + count;
        console.log(old_class);
        console.log(new_class);
        jQuery(btnBig).data("type", value);
        jQuery(".btn-count-big").removeClass('type108-count0').removeClass('type108-count1').removeClass('type108-count2').removeClass('type108-count3').removeClass('type108-count4');
        jQuery(".btn-count-big").removeClass('type1-count0').removeClass('type1-count1').removeClass('type1-count2').removeClass('type1-count3').removeClass('type1-count4');
        jQuery(".btn-count-big").addClass(new_class);
        document.cookie = "value_count_live=" + value + " ; path=/";
        var manis_done = parseInt(jQuery("#formSendCenter .manis-done").val().replace(/,/g, ""));
        if (!manis_done) {
            manis_done = 0;
        }
        if (manis_done > 0) {
            jQuery(".btn-count-big span").text(App.format(manis_done));
        } else {
            var width = jQuery('body').width();
            if (width > 540) {
                jQuery(".btn-count-big span").html("Click 1 to add <br>" + value);
            } else {
                jQuery(".btn-count-big span").html("Tap to add <br>" + value);
            }
        }
        // jQuery(".btn-count-big span").html("Click to add <br>" + value);
    });

    btnBig.click(function (event) {
        var count = parseInt(jQuery(this).data("count"));
        var type = parseInt(jQuery(this).data("type"));
        if (count == 4) {
            var old_class = "type" + type + "-count" + count;
            var new_class = "type" + type + "-count0";
            jQuery(".btn-count-big").removeClass('type108-count0').removeClass('type108-count1').removeClass('type108-count2').removeClass('type108-count3').removeClass('type108-count4');
            jQuery(".btn-count-big").removeClass('type1-count0').removeClass('type1-count1').removeClass('type1-count2').removeClass('type1-count3').removeClass('type1-count4');
            jQuery(".btn-count-big").addClass(new_class);
            jQuery(this).data("count", 0);
        } else {
            var old_class = "type" + type + "-count" + count;
            var new_class = "type" + type + "-count" + (count + 1);
            jQuery(".btn-count-big").removeClass('type108-count0').removeClass('type108-count1').removeClass('type108-count2').removeClass('type108-count3').removeClass('type108-count4');
            jQuery(".btn-count-big").removeClass('type1-count0').removeClass('type1-count1').removeClass('type1-count2').removeClass('type1-count3').removeClass('type1-count4');
            jQuery(".btn-count-big").addClass(new_class);
            jQuery(this).data("count", count + 1);
        }

        var practice_filter = jQuery("#formSendCenter .practice_filter option:selected").val();
        if (practice_filter.length == 0) {
            alert("Please select your practice");
            jQuery(".practice_filter").addClass("required");
            return false;
        }
        var value = parseInt(jQuery(this).data("value"));
        var manis_done = parseInt(jQuery("#formSendCenter .manis-done").val().replace(/,/g, ""));
        if (!manis_done) {
            manis_done = 0;
        }
        jQuery("#formSendCenter .manis-done").val(App.format(value + manis_done));
        jQuery(".btn-count-big span").text(App.format(value + manis_done));
        // var snd = new  Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");
        // snd.play();
        App.manisDone();
    });

    btnShowCount.click(function (event) {
        if (jQuery('.show-count').hasClass("hide")) {
            jQuery('.show-count').removeClass('hide');
            jQuery('.show-count').addClass('show');
        } else {
            jQuery('.show-count').addClass('hide');
        }
    });
    jQuery('.box-count-home .show-count #manis-done').focusout(function (event) {
        if (jQuery('.show-count').hasClass('show')) {
            jQuery('.show-count').addClass('hide');
            jQuery('.show-count').removeClass('show');
            App.manisDone();
        }
    });


};

// Xu ly count live

App.AccumulationCountLive = function (data_count, practice) {
// console.log(data_count);
// return false;
    var initFormCenter = jQuery('#formSendCenter');
    if (initFormCenter.length > 0) {
        var center_select = App.getValueCountLive('center_filter', data_count[1]);
        var practice_filter = App.getValueCountLive('practice_filter', data_count[4]);
        var email = App.getValueCountLive('email', data_count[0]);
        var emailRegex = new RegExp(/^([\w\.\-]+)@([\w\-]+)((\.(\w){2,10})+)$/i);
        var isvalid = emailRegex.test(email);
        var manisdone = App.getValueCountLive('manis-done', data_count[5]);
        var total_user = App.getValueCountLive('total_user', data_count[9]);
//            console.log(total_user)
//            return false;
        var total_center = App.getValueCountLive('total_center', data_count[10]);
        var total_all_center = App.getValueCountLive('total_all_center', data_count[10]);
        var id = App.getValueCountLive('id', data_count[8]);
        if (practice) {
            practice_filter = practice;
        }
        document.cookie = "email_center=" + email + " ; path=/";
        jQuery.ajax({
            type: 'POST',
            url: '/wp-admin/admin-ajax.php',
            data: {
                action: 'addCenter',
                center_select: center_select,
                practice_filter: practice_filter,
                'email': email,
                'manisdone': manisdone,
                'total_user': total_user,
                'id': id,
                'total_center': total_center,
                'total_all_center': total_all_center
            },
            beforeSend: function () {
                jQuery('.accumulations_select,.info,.submit.center,.count-live,.show-count').removeClass('hide');
                jQuery('.done-count-live,.box-count-live').addClass('hide');
                jQuery('.check_count').val('input');
                jQuery('.box-change-count .left').removeClass('full');
                jQuery('#formSendCenter .submit.center').html('<a class="button" href="javascript:;">Processing...</a>');
                jQuery('.info').css({"display": "none"});
                jQuery('.btn-live').addClass('no-click');
            },
            success: function (response) {
                // console.log(response);
                // return false;
                if (response) {
                    var data = jQuery.parseJSON(response);
                    if (data.err == 0) {
                        jQuery('input#manis-done').val('');
                        App.setCountLive();
                        jQuery(".btn-count-big").data("count", 0);
                        jQuery('.value-count.count-1').click();
                        jQuery('.box-count-home').removeClass('click-box');
                        App.initCheck(email, center_select);
                        jQuery('#formSendCenter .submit.center').html('<a class="button" href="javascript:;" onclick="jQuery(\'#formSendCenter\').submit();">Count!</a>');
                        jQuery('.btn-live').removeClass('no-click');
//                            jQuery('#formSendCenter .notice').html(data.msg).css({
//                                'text-align': 'center',
//                                'font-weight': 'bold',
//                                'color': 'blue'
//                            });
                        jQuery('html, body').animate({scrollTop: 0}, 800);
                        jQuery('.sharebox-count').fadeIn();
                        return false;
                    } else {
//                            alert("an error occurred, please try again");
                        location.reload();
                        return false;
                    }
                }
            },
            error: function () {
//                    alert("an error occurred, please try again");
                location.reload();
            }

        });
    }
    return false;
}

App.AccumulationCountLiveChange = function (data_count, practice) {

    var initFormCenter = jQuery('#formSendCenter');
    if (initFormCenter.length > 0) {
        var center_select = App.getValueCountLive('center_filter', data_count[1]);
        var practice_filter = App.getValueCountLive('practice_filter', data_count[4]);
        var email = App.getValueCountLive('email', data_count[0]);
        var emailRegex = new RegExp(/^([\w\.\-]+)@([\w\-]+)((\.(\w){2,10})+)$/i);
        var isvalid = emailRegex.test(email);
        var manisdone = App.getValueCountLive('manis-done', data_count[5]);
        var total_user = App.getValueCountLive('total_user', data_count[9]);
//            console.log(total_user)
//            return false;
        var total_center = App.getValueCountLive('total_center', data_count[10]);
        var total_all_center = App.getValueCountLive('total_all_center', data_count[10]);
        var id = App.getValueCountLive('id', data_count[8]);
        if (practice) {
            practice_filter = practice;
        }
        document.cookie = "email_center=" + email + " ; path=/";
        jQuery.ajax({
            type: 'POST',
            url: '/wp-admin/admin-ajax.php',
            data: {
                action: 'addCenter',
                center_select: center_select,
                practice_filter: practice_filter,
                'email': email,
                'manisdone': manisdone,
                'total_user': total_user,
                'id': id,
                'total_center': total_center,
                'total_all_center': total_all_center
            },
            beforeSend: function () {
                // jQuery('.accumulations_select,.info,.submit.center,.count-live').removeClass('hide');
                // jQuery('.box-count-live').addClass('hide');
                jQuery('.check_count').val('button');
                jQuery('#formSendCenter .done-count-live').text('...');
                jQuery('.info,.accumulations_select').css({"display": "none"});
            },
            success: function (response) {
                if (response) {
                    var data = jQuery.parseJSON(response);
                    if (data.err == 0) {
                        jQuery('input#manis-done').val('');
                        App.initCheck(email, center_select);
                        jQuery('#formSendCenter .done-count-live').text('Finish live counting');
                        jQuery('.btn-live').css({"display": "inline-block"});
                        jQuery('#manis-done').val(0);
//                            jQuery('#formSendCenter .notice').html(data.msg).css({
//                                'text-align': 'center',
//                                'font-weight': 'bold',
//                                'color': 'blue'
//                            });
                        alert('Data saved successfully');
                        jQuery('html, body').animate({scrollTop: 0}, 800);
                        jQuery('.sharebox-count').fadeIn();
                        return false;
                    } else {
//                            alert("an error occurred, please try again");
                        location.reload();
                        return false;
                    }
                }
            },
            error: function () {
//                    alert("an error occurred, please try again");
                location.reload();
            }

        });
    }
    return false;
}

App.getValueCountLive = function (name, data) {
    if (data.name == name) {
        return data.value;
    } else {
        return '';
    }
}

App.checkMovePage = function () {
    jQuery(window).bind("beforeunload", function (event) {
        var manis_done = parseInt(jQuery("#formSendCenter .manis-done").val().replace(/,/g, ""));
        var check_count = jQuery(".check_count").val();
        if (manis_done > 0 && check_count == "button") {
            return "You have unsaved changes";

        }
    });
}
