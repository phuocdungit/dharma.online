var AppCM = {
    changePositionLaunchRemove: function() {
        if (jQuery('.launchRemoveWraper').length) {
            jQuery('.launchRemoveWraper').each(function() {
                var $parent = jQuery(this).parents('.entry-content');
                var $pbtext = $parent.find('#entry_content .et_pb_row:first-of-type .et_pb_text:last-of-type');

                if ($pbtext.length) {
                    $pbtext.append(jQuery(this));
                }
                jQuery(this).css({'display': 'block'});
            });
        }
    },
    toggleTabs: function() {
        jQuery('.et_pb_tabs_controls .tab').on('click', function() {
            var $_this = jQuery(this),
                    $active = $_this.data('active');
            jQuery('.et_pb_tabs_controls .tab').removeClass('et_pb_tab_active');
            $_this.addClass('et_pb_tab_active');
            jQuery('.et_pb_all_tabs .et_pb_tab').removeClass('et_pb_active_content');
            jQuery('.tab-active-' + $active).addClass('et_pb_active_content');
        });
    },
    tableData: {
        table: null,
        initData: function() {
            if (jQuery('.dataTable').length) {
                this.table = jQuery('.dataTable')
                        .DataTable({
                            "aoColumnDefs": [{bSortable: false, aTargets: [-1]}],
                            "language": {
                                "paginate": {
                                    "previous": "<<",
                                    "next": ">>"
                                },
                            }
                        });
                jQuery(".paginate_button").click(function() {
                    App.tableData.editRow();
                });
            }
        }
    },
    getCookie: function(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ')
                c = c.substring(1);
            if (c.indexOf(name) == 0)
                return c.substring(name.length, c.length);
        }
        return "";
    },
    tabcontent: function() {
        if (jQuery('.tabcontent').length) {

            var $uid = jQuery('.tabcontent').data('uid');
            var cname = 'uid' + $uid;
            var $tabcookie = AppCM.getCookie(cname);
            if (jQuery('.tabcontent').data('fixwidth')) {
                var $width = jQuery('.tabcontent').width() - 2;
                var $liwidth = $width / jQuery('.tabcontent > li').length - 2;
                jQuery('.tabcontent > li').width($liwidth);
            }

            var $archos = jQuery('.tabcontent > li > a');

            $archos.click(function() {
                if (!jQuery(this).hasClass('active')) {
                    var $forclass = jQuery(this).data('forclass');
                    var $idactive = jQuery(this).data('idactive');
                    if (jQuery($forclass).length) {
                        jQuery($forclass).removeClass('active').hide();
                    } else {
                        jQuery('.' + $forclass).removeClass('active').hide();
                    }
                    if (jQuery($idactive).length) {
                        jQuery($idactive).addClass('active').slideDown();
                    } else {
                        jQuery('#' + $idactive).addClass('active').slideDown(300);
                    }
                    console.log(jQuery('.tabcontent > li > a'));
                    jQuery('.tabcontent > li > a').removeClass('active');
                    jQuery(this).addClass('active');
                }
                if (typeof ($uid) != 'undefined' && parseInt($uid)) {
                    App.setCookie(cname, $idactive, 30);
                }
            });
            if (jQuery.trim($tabcookie) != '') {
                jQuery('#tab_' + $tabcookie).trigger('click');
            }
        }
    },
};
jQuery(document).ready(function() {
    AppCM.changePositionLaunchRemove();
    AppCM.toggleTabs();
    AppCM.tableData.initData();
    AppCM.tabcontent();
});