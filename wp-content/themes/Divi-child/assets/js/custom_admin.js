var App = App = App || {
    isLog: true,
    initTest: function() {

    }
};
App.AdminAccumulation = function(key, user_id, center) {
    if (!user_id) {
        user_id = -1;
    }
    if (!center) {
        center = ''
    }
    var Accumulation = jQuery(".accumulation_filter option:selected").val();
    document.cookie = "accumulation=" + Accumulation + " ; path=/";
    jQuery.ajax({
        type: 'POST',
        url: '/wp-admin/admin-ajax.php',
        data: {action: 'Accumulation_Admin', key: key, id: Accumulation, user_id: user_id, center: center},
        beforeSend: function() {
            jQuery('body').append('<div class="custom_loading"></div>');
            jQuery('.target_totals_user').css({"display": "none"});
        },
        success: function(response) {
            jQuery('.custom_loading').remove();
            if (response) {
                var data = jQuery.parseJSON(response);
                if (data.err == 0) {
                    if (data.type == 1) {
                        App.Accumulation_admin_profile(data);
                    } else if (data.type == 2) {
                        App.Accumulation_admin_center(data);
                    }
                }
            }
        },
        error: function() {
            location.reload();
        }
    });
}
App.Accumulation_admin_profile = function(data) {
    if (data.html_practice != '') {
        jQuery('.target_totals_user').css({"display": "block"});
        jQuery('.target_totals_user').html(data.html_practice);
    }
}
App.Accumulation_admin_center = function(data) {
    jQuery('.table-responsive').html(data.html_practice);
}

App.admin_editPractices = function() {
    jQuery('.form-table .edit_button').on('click', function() {
        var Accumulation = jQuery(".accumulation_filter option:selected").val();
        var box = jQuery(this).parent().parent();
        var practice_name = box.find('.practice_name').text();
        var total_count = box.find('.total_count').text();
        var description = box.find('.description').text();
        var target = box.find('.target').text();
        var starting_count = box.find('.starting_count').text();
        jQuery('input[name="practices_name"]').attr('value', practice_name);
        jQuery('input[name="total_count"]').attr('value', total_count);
        jQuery('input[name="description"]').attr('value', description);
        jQuery('input[name="target"]').attr('value', target);
        jQuery('input[name="starting_count"]').attr('value', starting_count);
        jQuery('#myModal').modal('show');
        jQuery('#save_edit_practices').on('click', function() {
            jQuery('#myModal').modal('hide');
            jQuery('body').append('<div class="custom_loading"></div>');
            jQuery.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                data: {action: 'editPractices', practice_name: practice_name, accumulation: Accumulation, total_count: total_count, description: description, target: target, starting_count: starting_count},
                complete: function() {
                    return false;
                },
                error: function() {
                    alert('Error');
                    window.location.reload();
                }
            });

        });

    });
};
App.choosenMedia = function() {
    var loc_multi_image_file;
    jQuery('body').on('click.OpenMediaManager', '.choosen-media', function(e) {
        e.preventDefault();
        var $_this = jQuery(this);

        var parent = $_this.parent('.input-group'),
                inputField = parent.find(".input-media");
        // If the frame already exists, re-open it.
        if (loc_multi_image_file) {
            loc_multi_image_file.open();
            return;
        }

        loc_multi_image_file = wp.media.frames.loc_multi_image_file = wp.media({
            title: "Add image",
            button: {
                text: "Select image",
            },
            editing: true,
            className: 'media-frame loc_multi_image_file',
            frame: 'select', //Allow Select Only
            multiple: false, //Disallow Mulitple selections
            library: {
                type: 'image' //Only allow images type: 'image'
            },
        });
        loc_multi_image_file.on('select', function() {
            // Grab our attachment selection and construct a JSON representation of the model.
            var attachment = loc_multi_image_file.state().get('selection').first().toJSON();
            if (attachment.subtype == "pdf" || attachment.subtype == "doc" || attachment.subtype == "docx" || attachment.subtype == "txt")
            {
                alert("Please Insert only image");
                return;
            }

            if (typeof (attachment.sizes.thumbnail) != "undefined")
                var thum_url = attachment.sizes.thumbnail.url;
            else
                var thum_url = attachment.sizes.full.url;
            var thumb_id = attachment.id;
            // Send the attachment URL to our custom input field via jQuery.
            var loc_url = attachment.url;
            var locurls = loc_url.substr((loc_url.lastIndexOf('.') + 1));
            if (locurls != 'pdf' && locurls != 'zip' && locurls != 'rar' && locurls != "doc" && locurls != "docx" && locurls != "txt")
            {
                inputField.val(loc_url);
            } else {
                alert('Please add only image');
            }
        });
        // Now that everything has been set, let's open up the frame.
        loc_multi_image_file.open();
    });
};
App.choosenSelect2 = function() {
    jQuery('.choosen-select2').select2();
    jQuery('.show-settings').on('click', function() {
        jQuery('#screen-meta').find('#screen-options-wrap').removeClass('hidden');
    });
};
jQuery(document).ready(function() {
    App.admin_editPractices();
    App.choosenMedia();
    App.choosenSelect2();
});