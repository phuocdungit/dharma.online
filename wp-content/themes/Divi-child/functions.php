<?php
error_reporting(E_ERROR | E_PARSE);
date_default_timezone_set('Asia/Ho_Chi_Minh');

add_action('wp_enqueue_scripts', 'wp_enqueue_custom_scripts');

function wp_enqueue_custom_scripts()
{
    wp_enqueue_script('child-custom-js', get_stylesheet_directory_uri() . '/assets/js/custom.js', array('jquery'), '1.6.1.71');
}

//get image default 
function get_image_default_uri()
{
    return get_stylesheet_directory_uri() . '/assets/img/default.jpg';
}

//init session
function ur_theme_start_session()
{
    if (!session_id())
        session_start();
}

add_action("init", "ur_theme_start_session", 1);
//remove custom form Divi to child theme

require_once(get_stylesheet_directory() . '/includes/custom/api.php');
// for all calculator
require_once(get_stylesheet_directory() . '/calculator_funciton.php');
// config mail function
require_once(get_stylesheet_directory() . '/mail_function.php');

// xu ly data page count
require_once(get_stylesheet_directory() . '/page_count_function.php');

// xu ly data page more

require_once(get_stylesheet_directory() . '/page_more_function.php');

// xu ly data page profile
require_once(get_stylesheet_directory() . '/page_profile_function.php');

// xu ly data  practices (add,delete,edit trong profile  hoặc toàn page )

require_once(get_stylesheet_directory() . '/practices_function.php');

// xu ly data  center (add,delete,edit trong chi tiết profile hoặc toàn page)

require_once(get_stylesheet_directory() . '/center_function.php');

// config setting update
require_once(get_stylesheet_directory() . '/includes/custom/settings.php');

//admin function

require_once(get_stylesheet_directory() . '/includes/custom/admin_function.php');

/*
 * array_sort
 * ham sap xep theo value ben trong array
 * $array: mang can sap xep
 * $on: key ben trong array can sap xep
 * $order: kieu sap xep
 */

function array_sort($array, $on, $order = SORT_ASC)
{
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
                break;
            case SORT_DESC:
                arsort($sortable_array);
                break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}

/*
 * load_admin_styles
  add css ben trong admin
 */
add_action('admin_enqueue_scripts', 'load_admin_styles');

function load_admin_styles()
{
    wp_register_style('bootstrap-style-admin', get_stylesheet_directory_uri() . '/assets/vendor/bootstrap/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap-style-admin');
    wp_register_script('bootstrap-admin', get_stylesheet_directory_uri() . '/assets/vendor/bootstrap/js/bootstrap.min.js', array(), false, true);
    wp_enqueue_script('bootstrap-admin');
}

/*
 * stheme_add_boostrap
 * add boostrap
 */

function stheme_add_boostrap()
{
    wp_register_style('bootstrap-style', get_stylesheet_directory_uri() . '/assets/vendor/bootstrap/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap-style');
    wp_register_script('bootstrap', get_stylesheet_directory_uri() . '/assets/vendor/bootstrap/js/bootstrap.min.js', array(), false, true);
    wp_enqueue_script('bootstrap');
}

/*
 * mv_my_theme_styles
  add customm css
 */
add_action('wp_enqueue_scripts', 'stheme_add_boostrap');

function mv_my_theme_styles()
{

    wp_enqueue_style('my-custom-style', get_stylesheet_directory_uri() . '/assets/css/custom.css', false, time(), 'all');
//    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', false, '1.1', 'all');
}

add_action('wp_enqueue_scripts', 'mv_my_theme_styles');

function wpb_adding_scripts()
{
//    wp_register_script('my_amazing_script', get_template_directory_uri() . '/js/common.js', array('jquery'), '1.1', true);
    wp_enqueue_script('my_amazing_script');
}

add_action('wp_enqueue_scripts', 'wpb_adding_scripts');

/*
 * get_all_region
 * get all region xuat ra html ben ngoai
 */

function get_all_region($value = '')
{
    $args = array(
        'numberposts' => -1,
        'post_type' => 'region',
        'post_status' => 'private',
        'orderby' => 'name',
        'order' => 'ASC',
    );
    $postslist = get_posts($args);
    ?>
    <select name="region_filter" class="region_filter">
        <option value=""><?php _e('Please choose...', 'Divi'); ?></option>
        <?php
        foreach ($postslist as $term) {
            ?>
            <option <?php echo ($value == $term->post_title) ? 'selected' : '' ?>
                    value="<?php echo $term->post_title ?>"><?php echo $term->post_title ?></option>
            <?php
        }
        ?>
    </select>
    <?php
}

/*
 * get_all_region_html
 * get all region xuat ra html ben trong shortcode
 */

function get_all_region_html($value = '')
{
    $args = array(
        'numberposts' => -1,
        'post_type' => 'region',
        'post_status' => 'private',
        'orderby' => 'name',
        'order' => 'ASC',
    );
    $postslist = get_posts($args);
    $html = '<select name="region_filter" class="region_filter">
        <option value="">' . __('Please choose...', 'Divi') . '</option>';
    foreach ($postslist as $term) {
        $select = ($value == $term->post_title) ? 'selected' : '';
        $html .= '<option ' . $select . ' value="' . $term->post_title . '">' . $term->post_title . '</option>';
    }
    $html .= '</select>';
    return $html;
}

/*
 * get_all_center
 * get all center xuat ra html ben ngoai
 */

function get_all_center($value = '')
{
    $args = array(
        'numberposts' => -1,
        'post_type' => 'list-center',
        'post_status' => 'private',
        'orderby' => 'name',
        'order' => 'ASC',
    );
    $postslist = get_posts($args);
    ?>
    <select name="center_filter" class="center_filter">
        <option value=""><?php _e('Please choose...', 'Divi'); ?></option>
        <?php
        foreach ($postslist as $term) {
            ?>
            <option <?php echo ($value == $term->post_title) ? 'selected' : '' ?>
                    value="<?php echo $term->post_title ?>"><?php echo $term->post_title ?></option>
            <?php
        }
        ?>
    </select>
    <?php
}

/*
 * get_all_center_html
 * get all center xuat ra html ben trong shortcode
 */

function get_all_center_html($value = '', $check = 0, $add = 1, $hide = '')
{
    $args = array(
        'numberposts' => -1,
        'post_type' => 'list-center',
        'post_status' => 'private',
        'orderby' => 'name',
        'order' => 'ASC',
    );
    $postslist = get_posts($args);
    $html = '<select name="center_filter" class="center_filter ' . $hide . '" onchange="App.addNewCenter(' . $check . ')">
        <option value="">' . __('Please choose...', 'Divi') . '</option>';
    foreach ($postslist as $term) {
        $select = ($value == $term->post_title) ? 'selected' : '';
        $html .= '<option ' . $select . ' value="' . $term->post_title . '">' . $term->post_title . '</option>';
    }
    if ($add == 1) {
        $html .= '<option value="add">' . __('Add center...', 'Divi') . '</option>';
    }
    $html .= '</select>';
    $center = get_option('center_page') ? get_option('center_page') : 0;

    $html .= '<a class="button-info" href="' . get_page_link($center) . '"><img src="' . get_stylesheet_directory_uri() . '/info.png" alt=""></a>';
    return $html;
}

/*
 * get_all_practice
 * get all practice xuat ra html ben ngoai
 */

function get_all_practice($value = '')
{
    $args = array(
        'numberposts' => -1,
        'post_type' => 'practic',
        'post_status' => 'private',
        'orderby' => 'name',
        'order' => 'ASC',
    );
    $postslist = get_posts($args);
    ?>
    <select name="practic_filter" class="practic_filter">
        <option value=""><?php _e('Please choose...', 'Divi'); ?></option>
        <?php
        foreach ($postslist as $term) {
            ?>
            <option <?php echo ($value == $term->post_title) ? 'selected' : '' ?>
                    value="<?php echo $term->post_title ?>"><?php echo $term->post_title ?></option>
            <?php
        }
        ?>
    </select>
    <?php
}

/*
 * get_all_practice_html
 * get all practice xuat ra html ben trong shortcode
 */

function get_all_practice_html($value = '', $key = 0)
{
    $args = array(
        'numberposts' => -1,
        'post_type' => 'practic',
        'post_status' => 'private',
        'orderby' => 'name',
        'order' => 'ASC',
    );
    $postslist = get_posts($args);
    if ($key == 1):
        $html = '<select name="practice_filter" class="practice_filter" onchange="App.showAddPractice_registration()">
        <option value="">' . __('Please choose...', 'Divi') . '</option>';
    elseif ($key == 2):
        $html = '<select name="practice_filter" class="practice_filter" onchange="App.showAddPractice_registration()">'
            . '<option value="Chenrezig">Chenrezig</option>';
    else:
        $html = '<select name="practice_filter" class="practice_filter" onchange="App.showAddPractice()">
        <option value="">' . __('Please choose...', 'Divi') . '</option>';
    endif;

    foreach ($postslist as $term) {
        $select = ($value == $term->post_title) ? 'selected' : '';
        $html .= '<option ' . $select . ' value="' . $term->post_title . '">' . $term->post_title . '</option>';
    }
    if ($key == 1) {
        $html .= '<option  value="add">Add practice…</option>';
    }
    $html .= '</select>';
    $practices = get_option('practices_page') ? get_option('practices_page') : 0;

    $html .= '<a class="button-info" href="' . get_page_link($practices) . '"><img src="' . get_stylesheet_directory_uri() . '/info.png" alt=""></a>';
    return $html;
}

/*
 * get_all_practice_html_by_user
 * get all practice cua user trong shortcode
 */

function get_all_practice_html_by_user($user_id = 0, $current_practice = '')
{

    if ($user_id != 0 && $user_id == 1) {
        $current_user = wp_get_current_user();
        if (0 != $current_user->ID) {
            $user_id = $current_user->ID;
        }
    }

    if ($user_id && $user_id != 0) {
        global $wpdb;
        $get_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $user_id . " AND `meta_key`='user_meta_current_practice'");
        foreach ($get_practice as $key => $value) {
            $name[] = unserialize($value->value);
        }
        $name = array_sort($name, 'practices_name', SORT_ASC);
        $html = '<select name="practice_filter" class="practice_filter" onchange="
            App.showAddPractice();
            ">';
        if ($name and count($name) == 1):
            $html .= '<option value="' . reset($name)['practices_name'] . '" selected>' . reset($name)['practices_name'] . '</option>';
        else:
            $html .= '<option value="">' . __('Select practice to count...', 'Divi') . '</option>';
            foreach ($name as $term):
                $select = (($value == $term['practices_name']) or ($term['practices_name'] == $current_practice)) ? 'selected' : '';
                $html .= '<option ' . $select . ' value="' . $term['practices_name'] . '">' . $term['practices_name'] . '</option>';
            endforeach;
        endif;

        $html .= '<option value="More">More...</option>';
        $html .= '</select>';
        $practices = get_option('practices_page') ? get_option('practices_page') : 0;

        $html .= '<a target="_blank" class="button-info show-on" href="' . get_page_link($practices) . '"><img src="' . get_stylesheet_directory_uri() . '/info.png" alt=""></a>';
        return $html;
    } else {
        $html = '<select name="practice_filter" class="practice_filter" onchange="App.showAddPractice()">';
        if ($current_practice):
            $html .= '<option value="' . $current_practice . '">' . $current_practice . '</option>';
        endif;
        if ($current_practice != 'Chenrezig'):
            $html .= '<option value="Chenrezig">Chenrezig</option>';
        endif;
        $html .= '<option value="More">More...</option>';

        $html .= '</select>';
        $practices = get_option('practices_page') ? get_option('practices_page') : 0;

        $html .= '<a target="_blank" class="button-info show-on" href="' . get_page_link($practices) . '"><img src="' . get_stylesheet_directory_uri() . '/info.png" alt=""></a>';

        return $html;
    }
}

/*
 * my_menu_pages -> config_manis
 * add menu config backend
 */
add_action('admin_menu', 'my_menu_pages');

function my_menu_pages()
{
//    add_submenu_page('users.php', 'User Not Active', 'User Not Active', 'manage_options', 'user-not-active', 'user_not_active');
    add_menu_page('Config Manis', 'Config Manis', 'manage_options', 'config-manis', 'config_manis', "dashicons-admin-tools", 25);
}

function config_manis()
{
//    echo '<pre>';
//    print_r($_POST);
//    exit;
    if (strpos($_POST['Save'], 'Save') !== false) {
        $options = array(
            'total_target',
            'start_date',
            'end_date',
            'update_profile',
            'add_profile',
            'err_update_profile',
            'change_center_profile',
            'add_center',
            'add_user_center',
            'teachers',
            'center_page',
            'practices_page',
            'accumulations_page',
//            'image_share_count',
//            'link_share_count',
//            'description_share_count',
//            'share_button_text',
//            'dedicate_button_text'
        );

        foreach ($options as $option):
            if (isset($_POST[$option])):
                if (get_option($option) !== false):
                    update_option($option, $_POST[$option]);
                else:
                    add_option($option, $_POST[$option], null, 'no');
                endif;
            endif;
        endforeach;
        $_POST['Save'] = "Save";
        wp_redirect(home_url() . '/wp-admin/admin.php?page=config-manis');
        exit;
    }
    ?>
    <div id="wpbody" role="main">

        <div id="wpbody-content" aria-label="Config Manis" tabindex="0">
            <div class="wrap">
                <h1><?php _e('Variables global', 'Divi'); ?></h1>
                <?php
                $start_date = get_option('start_date');
                $end_date = get_option('end_date');

                if (strtotime($end_date) >= strtotime(date('d-m-Y')) && strtotime(date('d-m-Y')) >= strtotime($start_date)) {
                    $datediff = abs(strtotime(date('d-m-Y')) - strtotime($end_date));
                    $days_left = floor($datediff / (60 * 60 * 24)) + 1;
                }
                $args = array(
                    'numberposts' => -1,
                    'post_type' => 'manis-history',
                    'post_status' => 'private',
                    'orderby' => 'name',
                    'order' => 'ASC',
                );
                $postslist = get_posts($args);
                $id_post = $running_total_yesterday = $day_count_today = $total_all_center = 0;
                foreach ($postslist as $key => $post) {
                    $meta_post = get_post_meta($post->ID);
                    $total_all_center += $meta_post['wpcf-manis-done'][0];
                    if (strtotime(date('d-m-Y')) > $meta_post['wpcf-date-center'][0]) {
                        $running_total_yesterday += $meta_post['wpcf-manis-done'][0];
                    } else {
                        $day_count_today += $meta_post['wpcf-manis-done'][0];
                    }
                }
                ?>
                <form method="post" enctype="multipart/form-data" action="<?php the_permalink(); ?>">
                    <table class="form-table">
                        <tbody>
                        <tr>
                            <th scope="row"><label for="total_target"><?php _e('Total target', 'Divi'); ?></label></th>
                            <td><input name="total_target" type="text" id="total_target"
                                       value="<?php echo(get_option('total_target') ? get_option('total_target') : "") ?>"
                                       class="regular-text"></td>
                        </tr>
                        <tr>
                            <th scope="row"><label for="start_date"><?php _e('Start date', 'Divi'); ?></label></th>
                            <td><input name="start_date" type="text" id="start_date"
                                       value="<?php echo(get_option('start_date') ? get_option('start_date') : "") ?>"
                                       class="regular-text"></td>
                            <script type="text/javascript">

                                jQuery(document).ready(function () {
                                    jQuery("#start_date").datepicker({
                                        dateFormat: "dd-mm-yy"
                                    });
                                });</script>
                        </tr>
                        <tr>
                            <th scope="row"><label for="end_date"><?php _e('End date', 'Divi'); ?></label></th>
                            <td><input name="end_date" type="text" id="end_date"
                                       value="<?php echo(get_option('end_date') ? get_option('end_date') : "") ?>"
                                       class="regular-text"></td>
                            <script type="text/javascript">

                                jQuery(document).ready(function () {
                                    jQuery("#end_date").datepicker({
                                        dateFormat: "dd-mm-yy"
                                    });
                                });</script>
                        </tr>
                        <tr>
                            <th scope="row"><label for="days_left"><?php _e('Duration', 'Divi'); ?></label></th>
                            <td><input name="days_left" type="text" id="days_left"
                                       value="<?php echo ($days_left) ? $days_left : "" ?>" class="regular-text"
                                       readonly></td>
                        </tr>
                        <tr>
                            <th scope="row"><label
                                        for="day_count_global"><?php _e('Day count global', 'Divi'); ?></label></th>
                            <td><input name="day_count_global" type="text" id="day_count_global"
                                       value="<?php echo $running_total_yesterday ?>" class="regular-text" readonly>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><label for="day_count_today"><?php _e('Day count today', 'Divi'); ?></label>
                            </th>
                            <td><input name="day_count_today" type="text" id="day_count_today"
                                       value="<?php echo $day_count_today ?>" class="regular-text" readonly></td>
                        </tr>
                        <tr>
                            <th scope="row"><label
                                        for="running_total_global"><?php _e('Running total global', 'Divi'); ?></label>
                            </th>
                            <td><input name="running_total_global" type="text" id="running_total_global"
                                       value="<?php echo $total_all_center ?>" class="regular-text" readonly></td>
                        </tr>

                        </tbody>
                    </table>
                    <h1><?php _e('Dedications config', 'Divi'); ?></h1>
                    <table class="form-table">
                        <tbody>
                        <tr>
                            <th scope="row"><label for="teachers"><?php _e('Teachers', 'Divi'); ?></label></th>
                            <td><textarea rows="4" cols="50" class="teachers"
                                          name="teachers"><?php echo(get_option('teachers') ? get_option('teachers') : "") ?></textarea>

                                <p><?php _e('Acounts are separated by a ";"', 'Divi'); ?></p>

                                <p><?php _e('Example: mail1@example.com;mail2@example.com', 'Divi'); ?></p>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                    <h1><?php _e('Messages config', 'Divi'); ?></h1>
                    <table class="form-table">
                        <tbody>
                        <tr>
                            <th scope="row"><label
                                        for="update_profile"><?php _e('Update Profile Successful', 'Divi'); ?></label>
                            </th>
                            <td><input name="update_profile" type="text" id="update_profile"
                                       value="<?php echo(get_option('update_profile') ? get_option('update_profile') : "") ?>"
                                       class="regular-text"></td>
                        </tr>
                        <tr>
                            <th scope="row"><label
                                        for="add_profile"><?php _e('Add Profile Successful', 'Divi'); ?></label></th>
                            <td><input name="add_profile" type="text" id="add_profile"
                                       value="<?php echo(get_option('add_profile') ? get_option('add_profile') : "") ?>"
                                       class="regular-text"></td>
                        </tr>
                        <tr>
                            <th scope="row"><label
                                        for="err_update_profile"><?php _e('Error Profile', 'Divi'); ?></label></th>
                            <td><input name="err_update_profile" type="text" id="err_update_profile"
                                       value="<?php echo(get_option('err_update_profile') ? get_option('err_update_profile') : "") ?>"
                                       class="regular-text"></td>
                        </tr>
                        <tr>
                            <th scope="row"><label
                                        for="change_center_profile"><?php _e('Change Center Profile', 'Divi'); ?></label>
                            </th>
                            <td><input name="change_center_profile" type="text" id="change_center_profile"
                                       value="<?php echo(get_option('change_center_profile') ? get_option('change_center_profile') : "") ?>"
                                       class="regular-text"></td>
                        </tr>
                        <tr>
                            <th scope="row"><label
                                        for="add_center"><?php _e('Add Center Successful', 'Divi'); ?></label></th>
                            <td><input name="add_center" type="text" id="add_center"
                                       value="<?php echo(get_option('add_center') ? get_option('add_center') : "") ?>"
                                       class="regular-text"></td>
                        </tr>
                        <tr>
                            <th scope="row"><label
                                        for="add_user_center"><?php _e('Add User Center Successful', 'Divi'); ?></label>
                            </th>
                            <td><input name="add_user_center" type="text" id="add_user_center"
                                       value="<?php echo(get_option('add_user_center') ? get_option('add_user_center') : "") ?>"
                                       class="regular-text"></td>
                        </tr>
                        <tr>
                            <th scope="row"><label for="err_center"><?php _e('Error Center', 'Divi'); ?></label></th>
                            <td>
                                <input name="err_center" type="text" id="err_center"
                                       value="<?php echo(get_option('err_center') ? get_option('err_center') : "") ?>"
                                       class="regular-text">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <h1><?php _e('Add Help Page', 'Divi');
                        ?></h1>
                    <table class="form-table">
                        <tbody>
                        <tr>
                            <th scope="row"><?php _e('Center Page', 'Divi');
                                ?></th>
                            <td>
                                <?php
                                $center = get_option('center_page') ? get_option('center_page') : 0;
                                echo get_all_page($center, "center_page") ?>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><?php _e('Practices Page', 'Divi');
                                ?></th>
                            <td>
                                <?php
                                $practices_page = get_option('practices_page') ? get_option('practices_page') : 0;
                                echo get_all_page($practices_page, "practices_page") ?>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><?php _e('Accumulations Page', 'Divi');
                                ?></th>
                            <td>
                                <?php
                                $accumulations_page = get_option('accumulations_page') ? get_option('accumulations_page') : 0;
                                echo get_all_page($accumulations_page, "accumulations_page") ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <p class="submit"><input type="submit" name="Save" id="submit" class="button button-primary"
                                             value="Save"></p>
                </form>

            </div>


            <div class="clear"></div>
        </div>
        <!-- wpbody-content -->
        <div class="clear"></div>
    </div>
    <?php
}

//get all page
function get_all_page($id = 0, $name = "")
{
    $args = array(
        'sort_order' => 'asc',
        'sort_column' => 'post_title',
        'hierarchical' => 1,
        'child_of' => 0,
        'parent' => -1,
        'offset' => 0,
        'post_type' => 'page',
        'post_status' => 'publish'
    );
    $pages = get_pages($args);
    $html[] = '<select name="' . $name . '" id="' . $name . '"><option value="">Please choose...</option>';
    foreach ($pages as $item) {
        $active = ($id == $item->ID) ? 'selected' : '';
        $html[] = '<option ' . $active . ' value="' . $item->ID . '">' . $item->post_title . '</option>';
    }
    $html[] = ' </select > ';
    return implode('', $html);
}


/*
 * datepicker
 * add datepicker
 */

function add_e2_date_picker()
{
//jQuery UI date picker file
    wp_enqueue_script('jquery - ui - datepicker');
//jQuery UI theme css file
    wp_enqueue_style('e2b - admin - ui - css', '//ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/base/jquery-ui.css', false, "1.9.0", false);
}

add_action('admin_enqueue_scripts', 'add_e2_date_picker');

/*
 * checkUserExists
 * Kiem tra email nhap vao co ton tai trong user
 */
add_action('wp_ajax_checkUserExists', 'checkUserExists');
add_action('wp_ajax_nopriv_checkUserExists', 'checkUserExists');

function checkUserExists()
{
    $user_id = username_exists($_POST['email']);
    if ($user_id) {
        echo json_encode(array(
            'msg' => (get_option('change_center_profile') ? get_option('change_center_profile') : ""),
            'err' => 0,
        ));
        exit;
    } else {
        echo json_encode(array(
            'msg' => 'User not exists, please try again.',
            'err' => 1,
        ));
        exit;
    }
}

/*
 * loadContact
 * Auto dien ten va email vao from lien he
 */
add_action('wp_ajax_loadContact', 'loadContact');
add_action('wp_ajax_nopriv_loadContact', 'loadContact');

function loadContact()
{
    $current_user = wp_get_current_user();
    if (0 != $current_user->ID) {
        $data = array(
            'name' => $current_user->display_name,
            'email' => $current_user->user_login
        );
    }
    die(json_encode($data));
}

/*
 * my_edit_manis_history_columns
 * add them cot trong bang muc post manis-history
 */
add_filter('manage_edit-manis-history_columns', 'my_edit_manis_history_columns');

function my_edit_manis_history_columns($columns)
{

    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => __('Email'),
        'count' => __('Count '),
        'center' => __('Center'),
        'practices' => __('Practices'),
        'datecreate' => __('Date Create'),
        'date' => __('Date')
    );

    return $columns;
}

/*
 * my_manage_manis_history_columns
 * load data cho cac muc add them cot trong bang muc post manis-history
 */
add_action('manage_manis-history_posts_custom_column', 'my_manage_manis_history_columns', 10, 2);

function my_manage_manis_history_columns($column, $post_id)
{
    global $post;
    switch ($column) {
        case 'center':
            $center = get_post_meta($post->ID, 'wpcf-my-center', TRUE);
            if (empty($center)) {
                echo __('Unknown');
            } else {
                printf(__('%s'), $center);
            }
            break;
        /* If displaying the 'count' column. */
        case 'practices':
            $center = get_post_meta($post->ID, 'wpcf-my-practice', TRUE);
            if (empty($center)) {
                echo __('Unknown');
            } else {
                printf(__('%s'), $center);
            }
            break;
        /* If displaying the 'count' column. */
        case 'count' :

            /* Get the post meta. */
            $count = get_post_meta($post->ID, 'wpcf-manis-done', true);

            /* If no duration is found, output a default message. */
            if (empty($count))
                echo __('Unknown');

            /* If there is a duration, append 'minutes' to the text string. */
            else
                printf(__('%s'), number_format($count));

            break;

        /* If displaying the 'genre' column. */
        case 'datecreate' :

            /* Get the post meta. */
            $date = get_post_meta($post->ID, 'wpcf-date-center', true);

            /* If no duration is found, output a default message. */
            if (empty($date))
                echo __('Unknown');

            /* If there is a duration, append 'minutes' to the text string. */
            else
                printf(__('%s'), date('d/m/Y H:i', ($date)));

            break;

        /* If displaying the 'genre' column. */
        default :
            break;
    }
}

/*
 * my_manis_history_sortable_columns
 * them cot loc trong muc post manis-history
 */
add_filter('manage_edit-manis-history_sortable_columns', 'my_manis_history_sortable_columns');

function my_manis_history_sortable_columns($columns)
{

    $columns['count'] = 'count';
    $columns['datecreate'] = 'datecreate';
    $columns['center'] = 'center';
    $columns['practices'] = 'practices';

    return $columns;
}

/* Only run our customization on the 'edit.php' page in the admin. */
add_action('load-edit.php', 'my_edit_manis_history_load');

function my_edit_manis_history_load()
{
    add_filter('request', 'my_sort_manis_history');
}

/*
 * my_sort_manis_history
 * loc data theo cot trong muc post manis-history
 */

function my_sort_manis_history($vars)
{

    /* Check if we're viewing the 'movie' post type. */
    if (isset($vars['post_type']) && 'manis-history' == $vars['post_type']) {

        /* Check if 'orderby' is set to 'duration'. */
        if (isset($vars['orderby']) && 'count' == $vars['orderby']) {

            /* Merge the query vars with our custom variables. */
            $vars = array_merge(
                $vars, array(
                    'meta_key' => 'wpcf-manis-done',
                    'orderby' => 'meta_value_num'
                )
            );
        }
        if (isset($vars['orderby']) && 'center' == $vars['orderby']) {

            /* Merge the query vars with our custom variables. */
            $vars = array_merge(
                $vars, array(
                    'meta_key' => 'wpcf-my-center',
                    'orderby' => 'meta_value_num'
                )
            );
        }
        if (isset($vars['orderby']) && 'practices' == $vars['orderby']) {

            /* Merge the query vars with our custom variables. */
            $vars = array_merge(
                $vars, array(
                    'meta_key' => 'wpcf-my-practice',
                    'orderby' => 'meta_value_num'
                )
            );
        }
        if (isset($vars['orderby']) && 'datecreate' == $vars['orderby']) {

            /* Merge the query vars with our custom variables. */
            $vars = array_merge(
                $vars, array(
                    'meta_key' => 'wpcf-date-center',
                    'orderby' => 'meta_value_num'
                )
            );
        }
    }

    return $vars;
}

/*
 * pippin_add_user_id_column
 * Them cot trong table user
 */
add_filter('manage_users_columns', 'pippin_add_user_id_column');

function pippin_add_user_id_column($columns)
{
    $columns['display_name'] = 'Display name';
    $columns['center'] = 'Center';
    $columns['count'] = 'Total count';
    return $columns;
}

function my_column_register_sortable($columns)
{
    $columns['display_name'] = 'display_name';
    return $columns;
}

add_filter("manage_edit-page_sortable_columns", "my_column_register_sortable");
/*
 * pippin_show_user_id_column_content
 * load data cho cac cot trong table user
 */
add_action('manage_users_custom_column', 'pippin_show_user_id_column_content', 10, 3);

function pippin_show_user_id_column_content($value, $column_name, $user_id)
{
    $user_info = get_userdata($user_id);
    if ('display_name' == $column_name) {
        return $user_info->display_name;
    }
    if ('center' == $column_name) {
        return get_the_author_meta('user_meta_center_filter', $user_id);
    }
    if ('count' == $column_name) {
        global $wpdb;
        $my_center_target_user = get_the_author_meta('user_meta_center_filter', $user_id);
        $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $user_info->user_login . "' AND b.meta_value='" . $my_center_target_user . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
        foreach ($manis_done as $key => $value) {
            $total_user += get_post_meta($value->ID, 'wpcf-manis-done', true);
        }
    }
    return number_format($total_user);
}

function customTinyMCE($in)
{
    $in['menubar'] = false;
    unset($in['toolbar2']);
    return $in;
}

function custom_tinymce_buttons($buttons)
{
    return apply_filters('custom_tinymce_buttons', array());
}

function custom_tinymce_plugins($plugins = array())
{
    return apply_filters('custom_tinymce_plugins', array());
}

function custom_tinymce_css($mce_css)
{

    if (!empty($mce_css))
        $mce_css .= ',';

    $mce_css .= get_template_directory_uri() . '/css/custom_editor.css';
    return $mce_css;
}

function user_sortable_columns($columns)
{
    $columns['center'] = 'Center';
    return $columns;
}

add_filter('manage_users_sortable_columns', 'user_sortable_columns', 1, 10);

function user_column_orderby($vars)
{
    if (isset($vars['orderby']) && 'center' == $vars['orderby']) {
        $vars = array_merge($vars, array(
            'meta_key' => '',
            'orderby' => 'meta_value',
            'order' => 'asc'
        ));
    }
    return $vars;
}

add_filter('request', 'user_column_orderby');

/*
 * auto_redirect_after_logout
 * chuyen luong ve trang logout
 */
add_action('wp_logout', 'auto_redirect_after_logout');

function auto_redirect_after_logout()
{
    wp_redirect(home_url() . '/logout');
    exit();
}

/*
 * Begin
 * Dong bo display_name tu member sang user profile khi user login
 */

$current_user = wp_get_current_user();
if (0 != $current_user->ID) {
    $user_mail = $current_user->user_login;
    $user_id = $current_user->ID;
    global $wpdb;
    $data = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "swpm_members_tbl a INNER JOIN " . $wpdb->prefix . "swpm_form_builder_custom b WHERE a.user_name ='" . $user_mail . "' AND a.member_id = b.user_id");
    if ($data) {
        foreach ($data as $value) {
            if ($value->value != '' && $current_user->display_name != $value->value) {
                wp_update_user(array('ID' => $current_user->ID, 'display_name' => $value->value));
            }
        }
    }
    if ($_SERVER["REQUEST_URI"] == '/') {
        wp_redirect(home_url() . '/count');
        exit();
    }
}
add_action('wp_login', 'auto_redirect_after_login');
/*
 * End
 * Dong bo display_name tu member sang user profile khi user login
 */

/*
 * auto_redirect_after_login
 * khi user login  kiem tra neu nguoi dung chua add center va practice thi chuyen sang page de nguoi dung add
 * or da add thi chuyen huong ve page count
 */

function auto_redirect_after_login($email)
{
    global $wpdb;
    $current_user = wp_get_current_user();
    $userInfo = get_user_by('email', $email);
    $user_id = $userInfo->data->ID;
    /*$user_data = get_userdata($user_id);
    $api = mc4wp_get_api();
    $merge_vars = array(
        'FNAME' => $userInfo->data->display_name,
        'LNAME' => ''
    );
    if (!$api->list_has_subscriber('3aa030d659', $email)):
        $api->subscribe('3aa030d659', $userInfo->data->user_email, $merge_vars);
    endif;
    $api->unsubscribe('e93b3ce6c8', $user_data->data->user_email);*/
    //get center user
    $center_select = get_the_author_meta('user_meta_center_filter', $user_id);
    //get practices user
    $get_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $user_id . " AND `meta_key`='user_meta_current_practice'");
    if ($get_practice):
        $practice_name = array();
        foreach ($get_practice as $practice):
            $value = unserialize($practice->value);
            $practice_name[] = $value['practices_name'];
        endforeach;
        if (!in_array('Chenrezig', $practice_name)):
            $data = array(
                'practices_name' => 'Chenrezig',
                'practice_description' => '',
                'starting_count' => 0,
                'target' => 0
            );
            add_user_meta($user_id, 'user_meta_current_practice', $data);
        endif;
    else:
        $data = array(
            'practices_name' => 'Chenrezig',
            'practice_description' => '',
            'starting_count' => 0,
            'target' => 0
        );
        add_user_meta($user_id, 'user_meta_current_practice', $data);
    endif;

    if (!$center_select && !$get_practice) {
        wp_redirect(home_url() . '/post-registration-profile');
        exit();
    } else {
        wp_redirect(home_url() . '/count');
        exit();
    }
}

/*
 * registration_profile_func
 * add shortcode from registration-profile add center va practice
 */

function registration_profile_func()
{
    global $wpdb;
    $current_user = wp_get_current_user();
    $user_id = $current_user->ID;
    update_user_meta($user_id, 'user_meta_leaderboard_list', 2);
//get center user
    $center_select = get_the_author_meta('user_meta_center_filter', $user_id);
//get practices user
    $get_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $user_id . " AND `meta_key`='user_meta_current_practice'");
    $data = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "swpm_members_tbl a INNER JOIN " . $wpdb->prefix . "swpm_form_builder_custom b WHERE a.user_name ='" . $current_user->user_login . "' AND a.member_id = b.user_id");
    foreach ($data as $value) {
        $fname = $value->value;
    }
    $api = mc4wp_get_api();
    $merge_vars = array(
        'FNAME' => $fname,
        'LNAME' => ''
    );
//    $api->subscribe('10b1af0d94', , $merge_vars);
    $api->subscribe('10b1af0d94', $current_user->user_login, $merge_vars, 'html', true);
    //$api->unsubscribe('e93b3ce6c8', $current_user->user_login);
    if ($center_select && $get_practice) {
        wp_redirect(home_url() . '/count');
        exit();
    }

    $html = '<form id="formRegistrationProfile" class="formRegistrationProfile" name="formRegistrationProfile" method="POST" >
        <p class="notice"></p>';
    $html .= '<label class="label">' . __('Select or add your center', 'Divi') . '</label>' . get_all_center_html('', 0, 1);
    $html .= '<div class="add_new_center display">' . formAddCenter() . '</div>';
//    $html.= '<label class="label">' . __('my Practice to count', 'Divi') . '</label>' . get_all_practice_html('', 1);
    $html .= ' <label class="label">' . __('Select or add a practice to count', 'Divi') . '</label>
            <div class="group-box">' .
        get_all_practice_html('', 2)
        .
        formAddPractice()
        . '
        </div>';
    $html .= '<div class="show-count"><label class="label">' . __('Starting Count', 'Divi') . '</label>';
    $html .= '<input type="text" name="starting_count" class="starting_count" value=""/></div>';
    $html .= '
        <input type="hidden" name="id" class="id" value="' . $user_id . '"/>
        <input type="hidden" name="email" class="email" value="' . $current_user->user_login . '"/>
        <div class="submit registration">
            <a class="button" href="javascript:;" onclick="jQuery(\'#formRegistrationProfile\').submit();">' . __('Update!', 'Divi') . '</a>
        </div>
    </form>
    </div>';
    return $html;
}

add_shortcode('registration-profile', 'registration_profile_func');

/*
 * RegistrationProfile
 * add center va practice cho user
 */
add_action('wp_ajax_RegistrationProfile', 'RegistrationProfile');
add_action('wp_ajax_nopriv_RegistrationProfile', 'RegistrationProfile');

function RegistrationProfile()
{
    global $wpdb;
    update_user_meta($_POST['id'], 'user_meta_center_filter', $_POST['center_select']);
    $get_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $_POST['id'] . " AND `meta_key`='user_meta_current_practice'");
    $practice_name = array();
    if ($get_practice):
        foreach ($get_practice as $practice):
            $value = unserialize($practice->value);
            $practice_name[] = $value['practices_name'];
        endforeach;
    endif;
    if ($_POST['practice_filter'] != "Chenrezig") {
        if (!$practice_name or ($practice_name and !in_array('Chenrezig', $practice_name))):
            $data = array(
                'practices_name' => 'Chenrezig',
                'practice_description' => '',
                'starting_count' => 0,
                'target' => 0
            );
            add_user_meta($_POST['id'], 'user_meta_current_practice', $data);
        endif;
        $meta_acc = get_post_meta(check_current_accumulations());
        $check_save_practice = check_save_count($meta_acc, $_POST['practice_filter']);

        if (count($check_save_practice) == 0 && $_POST['starting_count'] > 0) {
            $data = array(
                'practices_name' => $_POST['practice_filter'],
                'practice_description' => '',
                'starting_count' => $_POST['starting_count'],
                'target' => 0
            );
            add_user_meta($_POST['id'], 'user_meta_current_practice', $data);
            $wpdb->insert(
                'manis_user_practice_starting_count_date',
                array(
                    'accumulation' => check_current_accumulations(),
                    'user_id' => $_POST['id'],
                    'practices' => $_POST['practice_filter'],
                    'starting_count' => $_POST['starting_count'],
                    'date_save' => date('Y-m-d'),
                    'date_modify' => date('Y-m-d'),
                ),
                array(
                    '%s',
                    '%s',
                    '%s',
                    '%s',
                    '%s',
                    '%s'
                )
            );
        }
    } else {
        if (!$practice_name or ($practice_name and !in_array('Chenrezig', $practice_name))):
            $data = array(
                'practices_name' => 'Chenrezig',
                'practice_description' => '',
                'starting_count' => $_POST['starting_count'],
                'target' => 0
            );
            add_user_meta($_POST['id'], 'user_meta_current_practice', $data);
        endif;
        $meta_acc = get_post_meta(check_current_accumulations());
        $check_save_practice = check_save_count($meta_acc, $_POST['practice_filter']);

        if (count($check_save_practice) == 0 && $_POST['starting_count'] > 0) {
            $wpdb->insert(
                'manis_user_practice_starting_count_date',
                array(
                    'accumulation' => check_current_accumulations(),
                    'user_id' => $_POST['id'],
                    'practices' => $_POST['practice_filter'],
                    'starting_count' => $_POST['starting_count'],
                    'date_save' => date('Y-m-d'),
                    'date_modify' => date('Y-m-d'),
                ),
                array(
                    '%s',
                    '%s',
                    '%s',
                    '%s',
                    '%s',
                    '%s'
                )
            );
        }
    }
    echo json_encode(array(
        'msg' => '',
        'err' => 0,
    ));
    exit;
}

/*
 * getStartingAll
 * get start count practices all user
 * $id: neu co $id thi sum theo bo loc dua vao $id
 *      khong co $id thi get all
 */

function getStartingAll($email = '', $id = 0)
{
    global $wpdb;
    $sumAll = 0;
    $get_practice_all = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `meta_key`='user_meta_current_practice'");
    foreach ($get_practice_all as $key => $value) {
        $data = unserialize($value->value);
        if ($id != 0) {
            $sumAll += $data['starting_count_' . $id];
        }
    }
    if ($id == 0) {
        $sumAll = sum_all_starting_count();
    }
    return $sumAll;
}

/*
 * getStartingUser
 * get start count practices of user
 * $email: neu co email thi get theo email khong thi lay email hien tai of user login
 * $id: neu co $id thi sum theo bo loc dua vao $id khong co $id thi get all
 */

function getStartingUser($email = '', $id = 0)
{
    global $wpdb;
    if ($email == "") {
        $current_user = wp_get_current_user();
    } else {
        $current_user = get_user_by('email', $email);
    }
    $user_id = $current_user->ID;
    $sumUser = 0;
    $get_practice_user = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $user_id . " AND `meta_key`='user_meta_current_practice'");
    foreach ($get_practice_user as $key => $value) {
        $data = unserialize($value->value);
        if ($id != 0) {
            $sumUser += $data['starting_count_' . $id];
        } else {
            $sumUser += get_starting_count($data['practices_name']);
        }
    }
    return $sumUser;
}

/*
 * getStartingPractices
 * get start count practices of cac practices
 * $Practices: ten Practices can sum
 * $meta_data: mang cac meta cua bo loc
 * $id: neu co $id thi sum theo bo loc dua vao $id khong co $id thi get all
 */

function getStartingPractices($Practices = '', $meta_data = array(), $id = 0)
{
    global $wpdb;
    $sumPractices = $target = 0;
    $get_all_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `meta_key`='user_meta_current_practice'");
    foreach ($get_all_practice as $key => $value) {
        $data = unserialize($value->value);
        if ($data['practices_name'] == $Practices) {
            if (!$meta_data == array()) {
                $sumPractices += $data['starting_count_' . $id];
                $target += $data['date_target_' . $id];
            } else {
                $sumPractices += sum_all_starting_count($data);
                $target += sum_all_target_count($data);
            }
        }
    }
    $practices['target'] = $target;
    $practices['sum'] = $sumPractices;

    return $practices;
}

/*
 * getStartingCenter
 * get start count practices of center
 * $center_select: ten center_select can sum
 * $id: neu co $id thi sum theo bo loc dua vao $id khong co $id thi get all
 */

function getStartingCenter($center_select = '', $id = 0)
{
    global $wpdb;
    if ($center_select == "") {
        $current_user = wp_get_current_user();
        $center_select = get_the_author_meta('user_meta_center_filter', $current_user->ID);
    }
    $sumCenter = 0;
    $get_practice_id = $wpdb->get_results("SELECT user_id AS id FROM " . $wpdb->prefix . "usermeta WHERE meta_value='" . $center_select . "'");
    foreach ($get_practice_id as $key => $value) {
        $get_practice_center_user = $wpdb->get_results("SELECT `meta_value` AS value FROM " . $wpdb->prefix . "usermeta WHERE user_id = " . $current_user->id . " AND `meta_key`='user_meta_current_practice'");
    }
    foreach ($get_practice_center_user as $key => $value) {
        $data = unserialize($value->value);
        if ($id != 0) {
            $sumCenter += $data['starting_count_' . $id];
        } else {
            $sumCenter += sum_all_starting_count_center($data['practices_name']);

        }
    }
    return $sumCenter;
}

/*
 * getTargetUser
 * get target practices of user
 * $email: neu co email thi get theo email khong thi lay email hien tai of user login
 * $id: neu co $id thi sum theo bo loc dua vao $id khong co $id thi get all
 */

function getTargetUser($email = '', $id = 0)
{
    global $wpdb;
    if ($email == "") {
        $current_user = wp_get_current_user();
    } else {
        $current_user = get_user_by('email', $email);
    }
    $user_id = $current_user->ID;
    $sumUser = 0;
    $get_practice_user = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $user_id . " AND `meta_key`='user_meta_current_practice'");
    foreach ($get_practice_user as $key => $value) {
        $data = unserialize($value->value);
        if ($id != 0) {
            $sumUser += $data['date_target_' . $id];
        } else {
            $sumUser += $data['target'];
        }
    }
    return $sumUser;
}

/*
 * modify_post_title
 * Cap nhat lai all ten cua list-center va practic khi duoc thay doi
 */
add_filter('wp_insert_post_data', 'modify_post_title', '99', 2); // Grabs the inserted post data so you can modify it.

function modify_post_title($data)
{
    global $wpdb;
//    $title_old = get_the_title($post_id);
    $title_old = str_replace('&#8211;', '-', get_the_title($post_id));
    $title_new = str_replace('&#8211;', '-', $data['post_title']);
    if ($data['post_type'] == 'list-center') {
        $wpdb->get_results('UPDATE ' . $wpdb->prefix . 'postmeta SET meta_value="' . $title_new . '" WHERE meta_value="' . $title_old . '" AND `meta_key`="wpcf-my-center"');
        $wpdb->get_results('UPDATE ' . $wpdb->prefix . 'usermeta SET meta_value="' . $title_new . '" WHERE meta_value="' . $title_old . '" AND `meta_key`="user_meta_center_filter"');
//        echo '<pre>';
//        print_r($wpdb);
//        exit;
    }
    if ($data['post_type'] == 'practic') {
        $wpdb->get_results('UPDATE ' . $wpdb->prefix . 'postmeta SET meta_value="' . $title_new . '" WHERE meta_value="' . $title_old . '" AND `meta_key`="wpcf-my-practice"');
        $get_practice_all = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "usermeta WHERE `meta_key`='user_meta_current_practice'  AND `meta_value` LIKE '%" . $title_old . "%'");
        if ($get_practice_all) {
            foreach ($get_practice_all as $value) {
                $datauser = unserialize($value->meta_value);
                if ($datauser['practices_name'] == $title_old) {
                    $datauser['practices_name'] = $title_new;
                    $wpdb->get_results("UPDATE " . $wpdb->prefix . "usermeta SET meta_value='" . serialize($datauser) . "' WHERE umeta_id='" . $value->umeta_id . "'");
                }
            }
        }
    }
    return $data;
}

/*
 * list_public_func
 * add shortcode list_public muc dedication
 */

function list_public_func()
{
    $args = array(
        'numberposts' => -1,
        'post_type' => 'dedications',
        'orderby' => 'date',
        'order' => 'DESC',
        'post_status' => 'private', //array(, 'private'),
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'wpcf-dedication-visibilit',
                'value' => 2,
                'compare' => '=',
            ),
        )
    );
    $postslist = get_posts($args);
    $html = "<div id='list_public' class='list_public'>";
//    $listteachers = explode(';', get_option('teachers'));
//    $user_data = get_userdata(get_current_user_id());
    foreach ($postslist as $post) {
//        $check_only_owner_teacher = get_post_meta($value->ID, 'wpcf-dedication-visibilit', true);
//        if ($check_only_owner_teacher == 1 and ($value->post_author == get_current_user_id() || in_array($user_data->user_login, $listteachers))):
//            $html .= htlm_dedication($value, 1);
//        elseif ($check_only_owner_teacher == 2):
//
//        endif;
        $html .= htlm_dedication($post, 1);
    }
    $html .= "</div>";

    return $html;
}

add_shortcode('list_public', 'list_public_func');
/*
 * list_private_func
 * add shortcode list_private muc dedication
 */

function list_private_func()
{
    $args = array(
        'numberposts' => -1,
        'post_type' => 'dedications',
        'orderby' => 'date',
        'order' => 'DESC',
        'post_status' => 'private', //array('private', ),
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'wpcf-dedication-visibilit',
                'value' => 1,
                'compare' => '=',
            ),
        )
    );
    $postslist = get_posts($args);
    $html = "<div id='list_private' class='list_private'>";
    $listteachers = explode(';', get_option('teachers'));
    $user_data = get_userdata(get_current_user_id());
    foreach ($postslist as $post) {
//        $check_only_owner_teacher = get_post_meta($value->ID, 'wpcf-dedication-visibilit', true);
        if (($post->post_author == get_current_user_id() || in_array($user_data->user_login, $listteachers))) {
            $html .= htlm_dedication($post, 2);
        }
    }
    $html .= "</div>";
    return $html;
}

add_shortcode('list_private', 'list_private_func');

/*
 * htlm_dedication
 * Html conten dedication
 */

function htlm_dedication($value, $class)
{
    $user_info = get_userdata($value->post_author);
    $html = "<div class='box-dedication'>
    <div class='box-image col-md-4 col-sm-4 col-xs-12 nopadding'>
        " . get_the_post_thumbnail($value->ID, 'full') . "

    </div>
    <div class='box-info col-md-8 col-sm-8 col-xs-12 padding_10'>
        <div class='box-subject col-md-12 col-sm-12 col-xs-12 padding_10'>
            <label class='label'  for='dedication-subject'>" . __('Dedication Subject', 'Divi') . "</label>
                <textarea rows='2' cols='50' readonly id='dedication-subject' name='dedication-subject' class='dedication-subject'>" . $value->post_title . "</textarea>
        </div>
        <div class='box-date col-md-6 col-sm-6 col-xs-12 padding_10'>
            <label class='label'  for='dedication-date'>" . __('When?', 'Divi') . "</label>
            <input type='text' readonly id='dedication-date' name='dedication-date' class='dedication-date' value='" . get_post_meta($value->ID, 'wpcf-dedication-date', true) . "'/>
        </div>
        <div class='box-subject col-md-6 col-sm-6 col-xs-12 padding_10 hidden-xs'>
            <label class='label'  for='dedication-subject'>" . __('Requested by', 'Divi') . "</label>
            <input type='text' readonly id='dedication-subject' name='dedication-subject' class='dedication-subject' value='" . $user_info->display_name . "'/>
        </div>
        <div class='box-description col-md-12 col-sm-12 col-xs-12 padding_10'>
            <label class='label'  for='dedication-description'>" . __('Description', 'Divi') . "</label>
            <textarea rows='5' cols='50' readonly class='dedication-description' name='dedication-description'>" . preg_replace('/\n\s+/', '\n', rtrim(html_entity_decode(strip_tags($value->post_content)))) . "</textarea>
        </div>
        <div class='box-subject col-md-6 col-sm-6 col-xs-12 padding_10 visible-xs-12 visible-sm-12 visible-xs'>
            <label class='label'  for='dedication-subject'>" . __('Requested by', 'Divi') . "</label>
            <input type='text' readonly id='dedication-subject' name='dedication-subject' class='dedication-subject' value='" . $user_info->display_name . "'/>
        </div>
    </div>";
    $listteachers = explode(';', get_option('teachers'));
    $user_data = get_userdata(get_current_user_id());

    if ($value->post_author == get_current_user_id() || in_array($user_data->user_login, $listteachers)) {
        $html .= "<div class='box-buton'>
        <div class='box-image col-md-4 col-sm-4 col-xs-12 nopadding'>
            <a href='javascript:void(0)' class='btn-showupload edit edit-" . $value->ID . " col-md-5 col-sm-12 col-xs-12' onclick='App.editDedication(" . $value->ID . ',' . $class . ")'>" . __('Edit', 'Divi') . "</a>
            <a href='javascript:void(0)' class='btn-showupload delete delete-" . $value->ID . " col-md-5 col-sm-12 col-xs-12' onclick='App.deleteDedication(" . $value->ID . ',' . $class . ")'>" . __('Delete', 'Divi') . "</a>

        </div>
        <div class='box-info col-md-8 col-sm-8 col-xs-12 padding_10'>
        </div>
    </div>";
    }

    $html .= "</div>";
    return $html;
}

/*
 * deleteDedication
 * Xoa bai viet tren Dedication
 */
add_action('wp_ajax_deleteDedication', 'deleteDedication');
add_action('wp_ajax_nopriv_deleteDedication', 'deleteDedication');

function deleteDedication()
{
    $my_post = array();
    $my_post['ID'] = $_POST['id'];
    $my_post['post_status'] = 'trash';

// Update the post into the database
    wp_update_post($my_post);
    if ($_POST['name'] == 1):
        $html = list_public_func();
    elseif ($_POST['name'] == 2):
        $html = list_private_func();
    endif;
    echo json_encode(array(
        'html' => $html,
        'msg' => '',
        'err' => 0,
    ));
    exit;
}

/*
 * editDedication
 * Sua bai viet tren Dedication
 */
add_action('wp_ajax_editDedication', 'editDedication');
add_action('wp_ajax_nopriv_editDedication', 'editDedication');

function editDedication()
{
    $post = get_post($_POST['id']);
    echo json_encode(array(
        'id' => $post->ID,
        'subject' => $post->post_title,
        'description' => preg_replace('/\n\s+/', '\n', rtrim(html_entity_decode(strip_tags($post->post_content)))),
        'date' => get_post_meta($post->ID, 'wpcf-dedication-date', true),
        'visibilit' => get_post_meta($post->ID, 'wpcf-dedication-visibilit', true),
        'image' => get_the_post_thumbnail($post->ID, 'medium'),
        'type' => 'update',
        'err' => 0,
    ));
    exit;
}

/*
 * add_dedications_func
 * add shortcode dedications_from form add dedication
 */

function add_dedications_func()
{
    $html = '<div id="add_dedications_func">
                <form id="formDedications" class="formDedications" name="formDedications" method="POST"  enctype="multipart/form-data">
                        <p class="notice"></p>
                        <input type="hidden" name="action" value="addDedications">
                        <input type="hidden" class="type" name="type" value="save">
                        <input type="hidden" class="id" name="id" value="0">
                        <div class="group-box">
                            <label class="label" for="form-dedication-subject">' . __('Dedication Subject', 'Divi') . '</label>
                            <textarea rows="2" cols="50" name="form-dedication-subject" id="form-dedication-subject" class="form-dedication-subject"></textarea>
                        </div>
                        <div class="group-box">
                            <label class="label" for="form-dedication-date">' . __('When?', 'Divi') . '</label>
                            <input type="text" name="form-dedication-date" id="form-dedication-date" class="form-dedication-date" value=""/>
                        </div>
                        <div class="group-box">
                            <label class="label" for="form-dedication-description">' . __('Description', 'Divi') . '</label>
                            <textarea rows="5" cols="50" class="dedication-description" name="form-dedication-description"></textarea>
                        </div>

                        <div class="group-box">
                            <label class="radio-inline"> <input type="radio" name="form-dedication-visibilit" id="form-dedication-visibilit" value="1"> ' . __('Private', 'Divi') . ' </label>
                            <label class="radio-inline"> <input type="radio" name="form-dedication-visibilit" id="form-dedication-visibilit" value="2"> ' . __('Public', 'Divi') . ' </label>
                        </div>';

    $html .= '<div class="group-box">
                            <label class="label">' . __('Image', 'Divi') . '</label>
                            <div class="Picture">

                            </div>
                            <div class="control-image">
                                <div class="img-dedication">
                                   <div class="upload-image">
                                        <input type="file" name="form-dedication-image" accept="image/*" class="form-dedication-image">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="submit dedication text-center">
                            <a class="button" href="javascript:;" onclick="location.reload();">' . __('Cancel', 'Divi') . '</a>
                            <a class="button add" href="javascript:;" onclick="jQuery(\'#formDedications\').submit();">' . __('Update', 'Divi') . '</a>
                        </div>
                    </form>
            </div>';
    return $html;
}

add_shortcode('dedications_from', 'add_dedications_func');

/*
 * addDedications
 * Add Dedications to post
 */
add_action('wp_ajax_addDedications', 'addDedications');
add_action('wp_ajax_nopriv_addDedications', 'addDedications');

function addDedications()
{
    if ($_POST && $_POST['type'] == 'save'):
        $post_id = wp_insert_post(array(
//            'post_author' => get_current_user_id(),
            'post_title' => $_POST['form-dedication-subject'],
            'post_content' => $_POST['form-dedication-description'],
            'post_type' => 'dedications',
            'post_status' => 'pending',
//            'post_status' => ($_POST['form-dedication-visibilit'] == 2 ? 'private' : 'private'),
        ));
        if ($_FILES['form-dedication-image']['name']):
            require_once(ABSPATH . '/wp-admin/includes/media.php');
            require_once(ABSPATH . '/wp-admin/includes/file.php');
            require_once(ABSPATH . '/wp-admin/includes/image.php');
            $attachmentId = media_handle_upload('form-dedication-image', $post_id);
            set_post_thumbnail($post_id, $attachmentId);
        endif;
        add_post_meta($post_id, 'wpcf-dedication-date', $_POST['form-dedication-date']);
        add_post_meta($post_id, 'wpcf-dedication-visibilit', $_POST['form-dedication-visibilit']);
        sendMailDedication($_POST);
        echo json_encode(array(
            'msg' => 'post ok',
            'err' => 0,
        ));
        exit;
    elseif ($_POST && $_POST['type'] == 'update'):
        $my_post = array(
            'ID' => $_POST['id'],
            'post_title' => $_POST['form-dedication-subject'],
            'post_content' => $_POST['form-dedication-description'],
        );
        wp_update_post($my_post);
        if ($_FILES['form-dedication-image']['name']):
            require_once(ABSPATH . '/wp-admin/includes/media.php');
            require_once(ABSPATH . '/wp-admin/includes/file.php');
            require_once(ABSPATH . '/wp-admin/includes/image.php');
            $attachmentId = media_handle_upload('form-dedication-image', $_POST['id']);
            set_post_thumbnail($_POST['id'], $attachmentId);
        endif;
        update_post_meta($_POST['id'], 'wpcf-dedication-date', $_POST['form-dedication-date']);
        update_post_meta($_POST['id'], 'wpcf-dedication-visibilit', $_POST['form-dedication-visibilit']);
        sendMailDedication($_POST);
        echo json_encode(array(
            'msg' => 'update ok',
            'err' => 0,
        ));
        exit;
    else:
        echo json_encode(array(
            'msg' => 'post errs',
            'err' => 1,
        ));
    endif;
}

/*
 * get_all_accumulations_html
 * get all region xuat ra html ben ngoai
 */

function get_all_accumulations_html($key = 0, $practices_name = 0, $post_key = 0, $user_id = 0)
{
    $args = array(
        'numberposts' => -1,
        'post_type' => 'accumulations',
        'post_status' => 'private',
        'meta_key' => 'wpcf-acc-start-date',
        'orderby' => 'meta_value',
    );
    $postslist = get_posts($args);
    $list_current = check_current_accumulations();

    if ($_COOKIE['accumulation'] || $_COOKIE['accumulation'] == "" && isset($_COOKIE['accumulation'])) {
        $value = $_COOKIE['accumulation'];
    } else {
        if ($list_current):
            $value = $list_current;
            setcookie(accumulation, $value, time() + (86400 * 30), "/"); // 86400 = 1 day
        else:
            $list_current = '';
            $value = $postslist[0]->ID;
            setcookie(accumulation, $value, time() + (86400 * 30), "/"); // 86400 = 1 day
        endif;
    }
    $name = 'accumulation_filter';
    if ($key == 4):
        $name = 'accumulation_filter2';

        $html = '<select data-key="' . $key . '" name="' . $name . '" class="' . $name . '" onchange="
            App.Accumulation(' . $key . ');
        ">';
    elseif ($key == 3 && is_admin()):
        $html = '<select data-key="' . $key . '" name="' . $name . '" class="' . $name . '" onchange="App.AdminAccumulation(' . $key . ',' . $user_id . ')">';
    else:
        $html = '<select data-key="' . $key . '" name="' . $name . '" class="' . $name . '" onchange="App.Accumulation(' . $key . ')">';
    endif;
    foreach ($postslist as $key => $term) {
        $str_startdate = get_post_meta($term->ID, 'wpcf-acc-start-date', true);
        $str_enddate = get_post_meta($term->ID, 'wpcf-acc-end-date', true);
        $start_date = date('Y-m-d', $str_startdate);
        $end_date = date('Y-m-d', $str_enddate);
        $current_date = date('Y-m-d');

        if ($start_date <= $current_date and $current_date <= $end_date):
            $days = (strtotime($end_date) - strtotime($current_date)) / (60 * 60 * 24);
            $days += 1;
            if ($days == 1) {
                $curren = '(today)';
            } elseif ($days == 2) {
                $curren = '(tomorrow)';
            } else {
                $curren = '(' . $days . ' days)';
            }
        else:
            $curren = '';
        endif;
        $title_select = get_post($value);
        $select = ($title_select->post_title == $term->post_title) ? 'selected' : '';
        $html .= '<option id="' . $term->ID . '" ' . $select . ' value="' . $term->ID . '">' . $term->post_title . ' ' . $curren . '</option>';
    }
//    if ($key == 1) {
//        $html.='<option  value="add">Add practice…</option>';
//    }
    if ($value) {
        if ($_COOKIE['accumulation'] == "" && isset($_COOKIE['accumulation'])) {

            $html .= '<option selected value="">' . __('All', 'Divi') . '</option></select>';
        } else {

            $html .= '<option value="">' . __('All', 'Divi') . '</option></select>';
        }
    } else {

        $html .= '<option selected value="">' . __('All', 'Divi') . '</option></select>';
    }
    $accumulations = get_option('accumulations_page') ? get_option('accumulations_page') : 0;

    $html .= '<a target="_blank" class="button-info show-on" href="' . get_page_link($accumulations) . '"><img src="' . get_stylesheet_directory_uri() . '/info.png" alt=""></a>';
    return $html;
}

/*
 * Accumulation
 * Xu ly get data cua bo loc tra ve
 * Accumulation = 1: loc o trang count
 * Accumulation = 2: loc o trang more
 * Accumulation = 3: loc o trang profile
 * Accumulation = 4: loc chi tiet practices
 */
add_action('wp_ajax_Accumulation', 'Accumulation');
add_action('wp_ajax_nopriv_Accumulation', 'Accumulation');

function Accumulation()
{
    if ($_POST['id'] == 0 && $_POST['id'] != ''):
        $_POST['id'] = check_current_accumulations();
    endif;
    if ($_POST['key'] == 1):
        if ($_POST['id']):
            Accumulation_count($_POST);
        else:
            getAllCount();
        endif;
    elseif ($_POST['key'] == 2):
        if ($_POST['id']):
            Accumulation_more($_POST);
        else:
            getMore();
        endif;
    elseif ($_POST['key'] == 3):
        if ($_POST['id']):
            Accumulation_profile($_POST);
        else:
            CheckProfile();
        endif;
    elseif ($_POST['key'] == 4):
        Accumulation_practices($_POST);
    endif;
    exit;
}

/*
 * Accumulation_count
 * Get data loc page count
 */

function Accumulation_count($post_value)
{
    $meta_data = get_post_meta($post_value['id']);

    global $wpdb;
    $current_user = wp_get_current_user();
    if (0 != $current_user->ID) {
        $user_id = $current_user->ID;
    }
    $center_select = '';
    if ($user_id) {
        $center_select = get_the_author_meta('user_meta_center_filter', $user_id);
        $personal_target = getTargetUser($current_user->user_login, $post_value['id']);
        //get practices user
        $get_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $user_id . " AND `meta_key`='user_meta_current_practice'");
        $html_practice = '';
        foreach ($get_practice as $key => $value) {
            $name[] = unserialize($value->value);
        }
        $a = $check_target = 0;
        $name = array_sort($name, 'practices_name', SORT_ASC);
        foreach ($name as $key => $value) {
            $total_user_practice = 0;
            $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $current_user->user_login . "' AND b.meta_value='" . $value['practices_name'] . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");

            foreach ($manis_done as $practice) {
                $date_manis = get_post_meta($practice->ID, 'wpcf-date-center', true);
                $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
                if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'][0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
                    $total_user_practice += get_post_meta($practice->ID, 'wpcf-manis-done', true);
                }
            }
            $a += $total_user_practice;
//            if ($total_user_practice > 0) {
            $target = ($value['date_target_' . $post_value['id']] > 0) ? " of " . number_format($value['date_target_' . $post_value['id']]) : "";
            if ($value['date_target_' . $post_value['id']] == null && $check_target == 0) {
                $check_target = 1;
            }
            $get_stating_count = get_starting_count($value['practices_name'], $post_value['id']);
            $starting_count = ($get_stating_count > 0) ? $get_stating_count : 0;
            if ($total_user_practice + $starting_count > 0) {
                $html_practice .= '<div class="group-box col-md-12">
            <label class="label">my ' . $value['practices_name'] . '</label>
            <input type="text" class="my_total" value="' . number_format($total_user_practice + $starting_count) . $target . '" readonly>
        </div>';
            }
            $total_user_practice = 0;
            $target = '';
        }
    }

    $remaining_daily = $days_left = $total_user = $total_user_center = 0;
    $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $current_user->user_login . "' AND b.meta_value='" . $center_select . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
    $manis_done_center = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE b.meta_value='" . $center_select . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
//    $total_user_center = $wpdb->get_results("SELECT SUM(b.`meta_value`)AS sum FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $_REQUEST['email'] . "' AND b.meta_key='wpcf-manis-done' AND a.`ID` = b.`post_id` ");

    foreach ($manis_done as $key => $value) {
        $date_manis = get_post_meta($value->ID, 'wpcf-date-center', true);
        $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
        if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'][0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
            $total_user += get_post_meta($value->ID, 'wpcf-manis-done', true);
        }
    }
    foreach ($manis_done_center as $key => $value) {
        $date_manis = get_post_meta($value->ID, 'wpcf-date-center', true);
        $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
        if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'][0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
            $total_user_center += get_post_meta($value->ID, 'wpcf-manis-done', true);
        }
    }
    $args = array(
        'numberposts' => -1,
        'post_type' => 'manis-history',
        'post_status' => 'private',
        'orderby' => 'name',
        'order' => 'ASC',
    );
    $postslist = get_posts($args);
    $id_post = $running_total_yesterday = $day_count_today = $total_all_center = 0;
    foreach ($postslist as $key => $post) {
        $meta_post = get_post_meta($post->ID);
        $date_manis = get_post_meta($post->ID, 'wpcf-date-center', true);
        $date_manis_interval = strtotime(date('Y-m-d', $date_manis));
        if ($date_manis_interval >= $meta_data['wpcf-acc-start-date'][0] && $date_manis_interval <= $meta_data['wpcf-acc-end-date'][0]) {
            $total_all_center += $meta_post['wpcf-manis-done'][0];
        }
    }
    $getpost = get_page_by_title($center_select, OBJECT, 'list-center');
    $center_target = get_post_meta($getpost->ID, 'wpcf-center_target', true);
    $total_target = $meta_data['wpcf-total-target'][0];
    $aa = getLeftToday($_POST['practice_filter'], $_POST['id'], $total_user + getStartingUser('', $post_value['id']));
    $personal_target = ($check_target == 0) ? $personal_target : "";
    if ($user_id && $center_select) {
        echo json_encode(array(
            'msg' => 'send',
            'err' => 0,
            'type' => 1,
            'total_center' => $total_user_center + getStartingCenter('', $post_value['id']),
            'total_center_target' => $center_target,
            'total_all_center' => $total_all_center + getStartingAll('', $post_value['id']),
            'total_all_center_target' => $total_target,
            'name' => $post->post_title,
            'total_user' => $total_user + getStartingUser('', $post_value['id']),
            'total_user_target' => $personal_target,
            'email' => $current_user->user_login,
            'html_practice' => $html_practice,
            'left_today' => $aa['left_today'],
            'daily_target' => $aa['daily_target'],
            'running_total_yesterday' => $aa['running_total_yesterday'],
            'personal_starting_count' => $aa['personal_starting_count'],
            'day_count_today' => $aa['day_count_today'],
            'days_left' => $aa['days_left'],
            'show_days_left' => $aa['show_days_left'],
            'remaining_daily' => $aa['remaining_daily'],
            'show_remaining_daily' => $aa['show_remaining_daily'],
            'total_user_target1' => $aa['total_user_target'],
            'over_target' => $over_target,
            'remaining_daily_all' => $aa['remaining_daily_all'],
            'left_today_all' => $aa['left_today_all'],

        ));
        exit;
    }
}

/*
 * my_edit_accumulations_columns
 * Them cot trong bang thong tin accumulation
 */
add_filter('manage_edit-accumulations_columns', 'my_edit_accumulations_columns');

function my_edit_accumulations_columns($columns)
{

    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => __('Accumulation'),
        'start' => __('Start '),
        'end' => __('End'),
        'target' => __('Target'),
        'total' => __('Total'),
        'description' => __('Description')
    );

    return $columns;
}

/*
 * my_manage_accumulations_columns
 * get data cac cot trong bang thong tin accumulation
 */
add_action('manage_accumulations_posts_custom_column', 'my_manage_accumulations_columns', 10, 2);

function my_manage_accumulations_columns($column, $post_id)
{
    global $post;
    switch ($column) {
        case 'start':
            $start = get_post_meta($post->ID, 'wpcf-acc-start-date', TRUE);
            if (empty($start)) {
                echo __('Unknown');
            } else {
                printf(__('%s'), date('d/m/Y', ($start)));
            }
            break;
        /* If displaying the 'count' column. */
        case 'end':
            $end = get_post_meta($post->ID, 'wpcf-acc-end-date', TRUE);
            if (empty($end)) {
                echo __('Unknown');
            } else {
                printf(__('%s'), date('d/m/Y', ($end)));
            }
            break;
        /* If displaying the 'count' column. */
        case 'target' :

            /* Get the post meta. */
            $target = get_post_meta($post->ID, 'wpcf-total-target', true);

            /* If no duration is found, output a default message. */
            if (empty($target))
                echo __('0');

            /* If there is a duration, append 'minutes' to the text string. */
            else
                printf(__('%s'), number_format($target));

            break;
        /* If displaying the 'genre' column. */
        case 'total' :
            $meta_data = get_post_meta($post->ID);
            $args = array(
                'numberposts' => -1,
                'post_type' => 'manis-history',
                'post_status' => 'private',
                'orderby' => 'name',
                'order' => 'ASC',
            );
            $postslist = get_posts($args);
            $total_all_center = 0;
            foreach ($postslist as $key => $item) {
                $meta_post = get_post_meta($item->ID);
                $date_manis = get_post_meta($item->ID, 'wpcf-date-center', true);
                if ($date_manis > $meta_data['wpcf-acc-start-date'][0] && $date_manis < $meta_data['wpcf-acc-end-date'][0]) {
                    $total_all_center += $meta_post['wpcf-manis-done'][0];
                }
            }
            /* If no duration is found, output a default message. */
            if ($total_all_center <= 0)
                echo __('0');

            /* If there is a duration, append 'minutes' to the text string. */
            else
                printf(__('%s'), number_format($total_all_center + getStartingAll('', $post->ID)));

            break;
        case 'description' :
            /* If no duration is found, output a default message. */
            if (empty($post->post_content))
                echo __('Unknown');

            /* If there is a duration, append 'minutes' to the text string. */
            else
                printf(__('%s'), wp_trim_words(preg_replace('/\n\s+/', '\n', rtrim(html_entity_decode(strip_tags($post->post_content)))), 10));

            break;
        /* If displaying the 'genre' column. */
        default :
            break;
    }
}

/*
 * my_accumulations_sortable_columns
 * tao cot loc data cac cot trong bang thong tin accumulation
 */
add_filter('manage_edit-accumulations_sortable_columns', 'my_accumulations_sortable_columns');

function my_accumulations_sortable_columns($columns)
{

    $columns['start'] = 'start';
    $columns['end'] = 'end';
    $columns['target'] = 'target';

    return $columns;
}

/* Only run our customization on the 'edit.php' page in the admin. */
add_action('load-edit.php', 'my_edit_accumulations_load');

function my_edit_accumulations_load()
{
    add_filter('request', 'my_sort_accumulations');
}

/*
 * my_sort_accumulations
 * loc data cac cot trong bang thong tin accumulation
 */

function my_sort_accumulations($vars)
{

    /* Check if we're viewing the 'movie' post type. */
    if (isset($vars['post_type']) && 'accumulations' == $vars['post_type']) {

        /* Check if 'orderby' is set to 'duration'. */
        if (isset($vars['orderby']) && 'start' == $vars['orderby']) {

            /* Merge the query vars with our custom variables. */
            $vars = array_merge(
                $vars, array(
                    'meta_key' => 'wpcf-acc-start-date',
                    'orderby' => 'meta_value_num'
                )
            );
        }
        if (isset($vars['orderby']) && 'end' == $vars['orderby']) {

            /* Merge the query vars with our custom variables. */
            $vars = array_merge(
                $vars, array(
                    'meta_key' => 'wpcf-acc-end-date',
                    'orderby' => 'meta_value_num'
                )
            );
        }
        if (isset($vars['orderby']) && 'target' == $vars['orderby']) {

            /* Merge the query vars with our custom variables. */
            $vars = array_merge(
                $vars, array(
                    'meta_key' => 'wpcf-total-target',
                    'orderby' => 'meta_value_num'
                )
            );
        }
    }

    return $vars;
}

/*
 * check_current_accumulations
 * Tra ve ID cua accumulation moi nhat
 */

function check_current_accumulations()
{
    $current_datetime = strtotime(date('Y-m-d'));
    global $wpdb;
    $list_accumulations = $wpdb->get_results
    ("
    SELECT P1.ID
        FROM
    " . $wpdb->prefix . "posts as P1
    LEFT JOIN " . $wpdb->prefix . "postmeta as M1 ON P1.ID = M1.post_id
            LEFT JOIN " . $wpdb->prefix . "postmeta as M2 ON P1.ID = M2.post_id WHERE P1.post_type = 'accumulations' AND P1.post_status = 'private' AND M1.meta_value <='" . $current_datetime . "' AND M1.meta_key ='wpcf-acc-start-date' AND M2.meta_value >='" . $current_datetime . "' AND M2.meta_key ='wpcf-acc-end-date' ORDER BY M2.meta_value ASC LIMIT 1
    ", ARRAY_N);
    if ($list_accumulations):
        return array_shift(array_shift($list_accumulations));
    else:
        return -1;
    endif;
}

/*
 * Begin
 * boot_add_post_meta
 * Add custom post meta cho cac muc post
 */
add_action('add_meta_boxes', 'boot_add_post_meta');

function boot_add_post_meta()
{
    add_meta_box('Count center info', 'Count center info', 'count_center_post_meta', 'list-center');
    add_meta_box('Count region info', 'Count region info', 'count_region_post_meta', 'region');
    add_meta_box('History info', 'History info', 'list_history_post_meta', 'manis-history');
    add_meta_box('User access list', 'User access list', 'list_user_access_post_meta', 'practic');
    add_meta_box('Count practic info', 'Count practic info', 'count_practic_post_meta', 'practic');
    add_meta_box('Set target Accumulation', 'Set target Accumulation', 'set_target_post_meta', 'accumulations');
}

function set_target_post_meta()
{
    global $post;

    $args = array(
        'to' => $post->ID,
    );
    $postslist = p2p_get_connections('practic_to_acc', $args);
    $connect_target = get_post_meta($post->ID, 'name-connect-target', true);
    $name_connect_target = get_post_meta($post->ID, 'connect-target', true);

    foreach ($postslist as $value) {
        $key = array_search($value->p2p_from, $name_connect_target);
        ?>
        <div class="form-item form-item-textfield wpt-form-item wpt-form-item-textfield">
            <label class="wpt-form-label wpt-form-textfield-label"><?php echo get_the_title(get_post($value->p2p_from)) ?></label>
            <input type="hidden" name="name_connect_target[]" value="<?php echo $value->p2p_from ?>"
                   class="wpt-form-textfield form-textfield textfield" data-wpt-type="textfield">
            <input type="text" name="connect_target[]" value="<?php echo $connect_target[$key] ?>"
                   class="wpt-form-textfield form-textfield textfield"
                   data-wpt-type="textfield">
        </div>
        <?php
        echo '<input type="hidden" name="boot_custom_meta_box_nonce" value= "' . wp_create_nonce(basename(__FILE__)) . '"/>';
    }
}

function count_center_post_meta()
{
    global $post;
    $args = array(
        'numberposts' => -1,
        'post_type' => 'manis-history',
        'orderby' => 'name',
        'order' => 'ASC',
        'post_status' => 'private',
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'wpcf-my-center',
                'value' => $post->post_title,
                'compare' => '=',
            ),
        )
    );
    $postslist = get_posts($args);

    $result = day_count($postslist);
    ?>
    <div class="form-item form-item-textfield wpt-form-item wpt-form-item-textfield">
        <label class="wpt-form-label wpt-form-textfield-label"><?php _e('Region', 'Divi'); ?></label>
        <?php get_all_region(get_post_meta($post->ID, 'my-region', true)) ?>
    </div>
    <div class="form-item form-item-textfield wpt-form-item wpt-form-item-textfield">
        <label class="wpt-form-label wpt-form-textfield-label"><?php _e('Day count center', 'Divi'); ?></label>
        <input type="text" id="day_count_center" name="day_count_center"
               value="<?php echo number_format($result['day_count_center'] + getStartingCenter($post->post_title)) ?>"
               class="wpt-form-textfield form-textfield textfield" data-wpt-type="textfield" readonly>
    </div>
    <div class="form-item form-item-textfield wpt-form-item wpt-form-item-textfield">
        <label class="wpt-form-label wpt-form-textfield-label"><?php _e('Day count center today', 'Divi'); ?></label>
        <input type="text" id="day_count_center_today" name="day_count_center_today"
               value="<?php echo number_format($result['day_count_today']) ?>"
               class="wpt-form-textfield form-textfield textfield" data-wpt-type="textfield" readonly>
    </div>
    <div class="form-item form-item-textfield wpt-form-item wpt-form-item-textfield">
        <label class="wpt-form-label wpt-form-textfield-label"><?php _e('Running total center', 'Divi'); ?></label>
        <input type="text" id="running_total_center" name="running_total_center"
               value="<?php echo number_format($result['running_total_center'] + getStartingCenter($post->post_title)) ?>"
               class="wpt-form-textfield form-textfield textfield" data-wpt-type="textfield" readonly>
    </div>
    <?php
}

function count_region_post_meta()
{
    global $post;
    $args = array(
        'numberposts' => -1,
        'post_type' => 'list-center',
        'orderby' => 'name',
        'order' => 'ASC',
        'post_status' => 'private',
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'my-region',
                'value' => $post->post_title,
                'compare' => '=',
            ),
        )
    );
    $postslist = get_posts($args);
    $results = array();
    foreach ($postslist as $key => $simplePost) {

        $argruments = array(
            'numberposts' => -1,
            'post_type' => 'manis-history',
            'orderby' => 'name',
            'post_status' => 'private',
            'order' => 'ASC',
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => 'wpcf-my-center',
                    'value' => $simplePost->post_title,
                    'compare' => '=',
                ),
            )
        );
        $historyPost = get_posts($argruments);
        $results[] = day_count($historyPost);
        $startcount = getStartingCenter($simplePost->post_title);
    }
//    var_dump('<pre>', $results);
//    exit();
    $running_total_center = $day_count_center = $day_count_today = 0;
    foreach ($results as $simpleResult) {
        $day_count_center += $simpleResult['day_count_center'] + $startcount;
        $day_count_today += $simpleResult['day_count_today'];
        $running_total_center += $simpleResult['running_total_center'] + $startcount;
    }
    ?>
    <div class="form-item form-item-textfield wpt-form-item wpt-form-item-textfield">
        <label class="wpt-form-label wpt-form-textfield-label"><?php _e('Region target', 'Divi'); ?></label>
        <input type="text" id="region_target" name="region_target"
               value="<?php echo get_post_meta($post->ID, 'region_target', true) ?>"
               class="wpt-form-textfield form-textfield textfield" data-wpt-type="textfield">
    </div>
    <div class="form-item form-item-textfield wpt-form-item wpt-form-item-textfield">
        <label class="wpt-form-label wpt-form-textfield-label"><?php _e('Day count region', 'Divi'); ?></label>
        <input type="text" id="day_count_center" name="day_count_center"
               value="<?php echo number_format($day_count_center) ?>"
               class="wpt-form-textfield form-textfield textfield" data-wpt-type="textfield" readonly>
    </div>
    <div class="form-item form-item-textfield wpt-form-item wpt-form-item-textfield">
        <label class="wpt-form-label wpt-form-textfield-label"><?php _e('Day count region today', 'Divi'); ?></label>
        <input type="text" id="day_count_center_today" name="day_count_center_today"
               value="<?php echo number_format($day_count_today) ?>"
               class="wpt-form-textfield form-textfield textfield"
               data-wpt-type="textfield" readonly>
    </div>
    <div class="form-item form-item-textfield wpt-form-item wpt-form-item-textfield">
        <label class="wpt-form-label wpt-form-textfield-label"><?php _e('Running total region', 'Divi'); ?></label>
        <input type="text" id="running_total_center" name="running_total_center"
               value="<?php echo number_format($running_total_center) ?>"
               class="wpt-form-textfield form-textfield textfield" data-wpt-type="textfield" readonly>
    </div>
    <?php
}

function count_practic_post_meta()
{
    global $post;
    $args = array(
        'numberposts' => -1,
        'post_type' => 'manis-history',
        'orderby' => 'name',
        'order' => 'ASC',
        'post_status' => 'private',
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'wpcf-my-practice',
                'value' => $post->post_title,
                'compare' => '=',
            ),
        )
    );
    $postslist = get_posts($args);

    $result = day_count($postslist);
    $a = getStartingPractices($post->post_title);
    $number_day_count_center = $result['day_count_center'] + $a['sum'];
    $number_running_total_center = $result['running_total_center'] + $a['sum'];
    ?>
    <div class="form-item form-item-textfield wpt-form-item wpt-form-item-textfield">
        <label class="wpt-form-label wpt-form-textfield-label"><?php _e('Day count practice', 'Divi'); ?></label>
        <input type="text" id="day_count_center" name="day_count_center"
               value="<?php echo number_format($number_day_count_center); ?>"
               class="wpt-form-textfield form-textfield textfield" data-wpt-type="textfield" readonly>
    </div>
    <div class="form-item form-item-textfield wpt-form-item wpt-form-item-textfield">
        <label class="wpt-form-label wpt-form-textfield-label"><?php _e('Day count practice today', 'Divi'); ?></label>
        <input type="text" id="day_count_center_today" name="day_count_center_today"
               value="<?php echo number_format($result['day_count_today']) ?>"
               class="wpt-form-textfield form-textfield textfield" data-wpt-type="textfield" readonly>
    </div>
    <div class="form-item form-item-textfield wpt-form-item wpt-form-item-textfield">
        <label class="wpt-form-label wpt-form-textfield-label"><?php _e('Running practice center', 'Divi'); ?></label>
        <input type="text" id="running_total_center" name="running_total_center"
               value="<?php echo number_format($number_running_total_center); ?>"
               class="wpt-form-textfield form-textfield textfield" data-wpt-type="textfield" readonly>
    </div>

    <?php
}

function list_history_post_meta()
{
    global $post;
    echo '<label class="wpt-form-label wpt-form-textfield-label">' . __('my Group', 'Divi') . '</label>';
    get_all_center(get_post_meta($post->ID, 'wpcf-my-center', true));
    echo '<label class="wpt-form-label wpt-form-textfield-label">' . __('my LifeLine', 'Divi') . '</label>';
    get_all_practice(get_post_meta($post->ID, 'wpcf-my-practice', true));
}

function list_user_access_post_meta()
{
    global $wpdb;
    global $post;
    $get_list_user = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "usermeta WHERE `meta_key`='user_meta_current_practice'  AND `meta_value` LIKE '%" . $post->post_title . "%'");
    echo '<ol>';
    foreach ($get_list_user as $value) {
        $data = unserialize($value->meta_value);
        if ($post->post_title == $data['practices_name']) {
            $user_data = get_userdata($value->user_id);
            echo '<li><a href="' . get_edit_user_link($value->user_id) . '">' . $user_data->user_login . '</a></li>';
        }
    }
    echo '</ol>';
}


function boot_save_custom_meta($post_id)
{
    global $custom_meta_fields;
// verify nonce

    if (!wp_verify_nonce($_POST['boot_custom_meta_box_nonce'], basename(__FILE__)))
        return $post_id;
    $name_connect_target = $_POST['name_connect_target'];
    $connect_target = $_POST['connect_target'];
    array_unshift($name_connect_target, 0);
    array_unshift($connect_target, 0);
    update_post_meta($post_id, 'name-connect-target', $connect_target);
    update_post_meta($post_id, 'connect-target', $name_connect_target);
}

add_action('save_post', 'boot_save_custom_meta');

function save_post_type_status_private($status)
{
    global $post;
    if ($status != 'trash') {
        $ID = ($post->ID) ? $post->ID : $post['ID'];
//        $list = array('accumulations', 'dedications', 'list-center', 'manis-history', 'practic', 'region', 'theme-mail');
        $list = array('accumulations', 'list-center', 'manis-history', 'practic', 'region', 'theme-mail');
        if (in_array(get_post_type($ID), $list) || $status == "private") {
            $status = 'private';
        }
        if (get_post_type($ID) == 'dedications' && $status == "publish") {
            $status = 'private';
        }
    } else {
        $status = 'trash';
    }

    return $status;
}

add_filter('status_save_pre', 'save_post_type_status_private', 10, 1);


/*add_action('user_register','check_api',10,1);
function check_api($user_id){
    $user_data = get_userdata($user_id);
    $api = mc4wp_get_api();
    $merge_vars = array(
        'FNAME' => $_POST['swpm-130'],
        'LNAME' => ''
    );
    $api->subscribe('3aa030d659', $user_data->data->user_email, $merge_vars, 'html', false);
    $api->unsubscribe('e93b3ce6c8', $user_data->data->user_email);
}
*/
//
