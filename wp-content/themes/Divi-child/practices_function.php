<?php
/**
 * Created by PhpStorm.
 * User: ANH
 * Date: 4/28/2016
 * Time: 9:36 AM
 */

/*
 * formAddPractices
 * add new Practices tu profile
 */
add_action('wp_ajax_formAddPractices', 'formAddPractices');
add_action('wp_ajax_nopriv_formAddPractices', 'formAddPractices');

function formAddPractices()
{
//    $user_id = username_exists($_COOKIE['email_center']);
    $current_user = wp_get_current_user();
    if (0 != $current_user->ID) {
        $user_id = $current_user->ID;
    }
    if ($user_id) {
        $data = array(
            'practices_name' => $_POST['practices'],
            'practice_description' => '',
            'starting_count' => 0,
            'target' => 0
        );
        global $wpdb;
        $check = 0;
        $check_practice = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $user_id . " AND `meta_key`='user_meta_current_practice' AND `meta_value` LIKE '%" . $_POST['practices'] . "%'");
        $count_practice = $wpdb->get_results("SELECT count(user_id) as id FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $user_id . " AND `meta_key`='user_meta_current_practice'");
        $count_practice = ($count_practice[0]->id >= 0) ? $count_practice[0]->id : 0;
        foreach ($check_practice as $value) {
            $name = unserialize($value->meta_value);
            if ($name['practices_name'] == $_POST['practices']) {
                $check = 1;
            }
        }
        if ($check == 0) {
            add_user_meta($user_id, 'user_meta_current_practice', $data);
            $html = '<div class="group-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="row ' . $count_practice . '">
                        <div class="main-content">
                            <div class="text-left col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="row">
                                   <p class="title">' . $_POST['practices'] . '</p>
                                 </div>
                            </div>
                            <div class="text-right col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="row">
                                    <div class="practices_button details">
                                       <a class="button details" href="javascript:;" onclick="App.editPractices(\'' . $data['practices_name'] . '\',' . $count_practice . ',0)">' . __('details', 'Divi') . '</a>
                                       <a class="button load display" href="javascript:;">Load...</a>
                                     </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="row">
                                    <input type="text" class="count_practice" value="0" readonly>
                                </div>
                            </div>
                         </div>
                         <div class="edit-content">
                         </div>
                    </div>
              </div>';
//        echo '<pre>';
//        print_r($html);
//        exit;
            echo json_encode(array(
                'html' => $html,
                'practices_name' => $data['practices_name'],
                'count_practice' => $count_practice,
                'msg' => '',
                'err' => 0,
            ));
            exit;
        } else {
            echo json_encode(array(
                'msg' => 'Practices is registered',
                'err' => 1,
            ));
            exit;
        }
    } else {
        echo json_encode(array(
//            'msg' => (get_option('add_center') ? get_option('add_center') : ""),
            'msg' => 'User does not exist',
            'err' => 1,
        ));
        exit;
    }
}

/*
 * editPractices
 * edit Practices trong profile
 */
add_action('wp_ajax_editPractices', 'editPractices');
add_action('wp_ajax_nopriv_editPractices', 'editPractices');

function editPractices()
{
    $idcheck = check_current_accumulations();
    if (isset($_COOKIE['accumulation'])) {
        $accumulation = $_COOKIE['accumulation'];
        $meta_data = get_post_meta($accumulation);
    }
    $current_user = wp_get_current_user();
    if (0 != $current_user->ID) {
        $user_id = $current_user->ID;
    }
    if ($user_id) {
        global $wpdb;
        $check_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $user_id . " AND `meta_key`='user_meta_current_practice' AND `meta_value` LIKE '%" . $_POST['name'] . "%'");
        $data = unserialize($check_practice[0]->value);
        $post = get_page_by_title($data['practices_name'], OBJECT, 'practic');
        $postMeta = get_post_meta($post->ID, 'wpcf-mantra', true);
        $practice_description = ($data['practice_description'] == '') ? $postMeta : $data['practice_description'];
        $practice_starting_count = '';
        $starting_count = get_starting_count($data['practices_name'], $accumulation);

        if (strtotime('today') > $meta_data['wpcf-acc-start-date'][0] && strtotime('today') > $meta_data['wpcf-acc-end-date'][0]):
//            $practice_starting_count = 'disabled="disabled"';
            $practice_starting_count = '';
        elseif ($starting_count > 0):
            $practice_starting_count = 'disabled="disabled"';
        endif;
        $read = "";
//        $practice_target = ($data['target'] > 0) ? 'readonly' : '';
        if ($data['date_target_' . $accumulation] > 0) {
            $target = $data['date_target_' . $accumulation];
        }
        if ($_POST['acc'] == "") {
            $target = $data['target_practice_all'];
            if ($target > 0) {
                $read = 'disabled';
            }
        }
//        $target = ($data['target'] > 0) ? 'readonly' : '';
        $html = '<div class="edit-practices-box">';
        $html .= "<input type='hidden' name='practice_data' class='practice_data' value='" . $check_practice[0]->value . "'/>";
        $html .= '<div class="group-box">' .
            get_all_practice_html($data['practices_name'], 0)
            . '<div class="submit delete">
                              <a class="button delete" href="javascript:;" onclick="App.deletePractices(\'' . $data['practices_name'] . '\',' . $_POST['key'] . ',' . $user_id . ')">' . __('Archive', 'Divi') . '</a>
                         </div>
                         <p><b>' . __('Mantra / practice description', 'Divi') . '</b></p>
                            <textarea rows="4" cols="50" class="practice-description" name="practice-description">' . $practice_description . '</textarea>
                            <p class="starting_count_label"><b>' . __('Accumulated before joining (Starting Count)', 'Divi') . '</b></p>
                             <input type="text"  onclick="App.cancelInput(' . $starting_count . ')" name="starting-count" ' . $practice_starting_count . ' class="starting-count" value="' . number_format($starting_count) . '">
                                 <script type="text/javascript">$(".starting-count").maskMoney();</script>
                             <div class="accumulations_select">
                            <p><b>' . __('Accumulation', 'Divi') . '</b></p>
                            ' . get_all_accumulations_html(4, $data['practices_name'], $_POST['key'], $user_id) . '
                            </div>
                            <div class="image-loading loading display"></div>
                                 <div class="your-practices">
                            </div>
                            <p><b>' . __('Accumulated (Count)', 'Divi') . '</b></p>
                            <input type="text" class="count_practice" value="' . $_POST['count'] . '" readonly>
                            
                             
                             <p class="target_label"><b>' . __('Target', 'Divi') . '</b></p>
                             <input type="text" name="target-practice" class="target-practice" ' . $read . ' value="' . number_format($target) . '">
                                 <script type="text/javascript">$(".target-practice").maskMoney();</script>
                             <div class="submit edit-practices text-center">
                             <div class="practices_button cancel">
                                <a class="button cancel" href="javascript:;" onclick="App.cancelPractices(\'' . $data['practices_name'] . '\',' . $_POST['key'] . ',' . $user_id . ')">' . __('Cancel', 'Divi') . '</a>
                              </div>
                              <div class="practices_button update">
                                <a class="button update" href="javascript:;" onclick="App.updatePractices(\'' . $data['practices_name'] . '\',' . $_POST['key'] . ',' . $user_id . ')">' . __('Update', 'Divi') . '</a>
                              </div>
                         </div>
                       </div>
                    </div>';

        echo json_encode(array(
            'html' => $html,
            'key' => $_POST['key'],
            'msg' => '',
            'err' => 0,
        ));
        exit;
    } else {
        echo json_encode(array(
//            'msg' => (get_option('add_center') ? get_option('add_center') : ""),
            'msg' => 'User does not exist',
            'err' => 1,
        ));
        exit;
    }
}

/*
 * deletePractices
 * xoa 1 Practices trong profile
 */
add_action('wp_ajax_deletePractices', 'deletePractices');
add_action('wp_ajax_nopriv_deletePractices', 'deletePractices');

function deletePractices()
{
    if ($_POST['userid']) {
        $current_user = wp_get_current_user();
        if (0 != $current_user->ID) {
            $user_mail = $current_user->user_login;
        }
        global $wpdb;
        if (isset($_COOKIE['accumulation'])) {
            $accumulation = $_COOKIE['accumulation'];
            $meta_data = get_post_meta($accumulation);
        }
        $check_practice = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $_POST['userid'] . " AND `meta_key`='user_meta_current_practice' AND `meta_value` LIKE '%" . $_POST['name'] . "%'");
        if ($check_practice[0]->umeta_id) {
            if ($wpdb->delete($wpdb->prefix . "usermeta", array('umeta_id' => $check_practice[0]->umeta_id))):
//                $wpdb->get_results("DELETE a,b FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b WHERE a.`post_title`='" . $user_mail . "' AND b.meta_value='" . $_POST['name'] . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private' AND b.`meta_key`='wpcf-my-practice'");
                $get_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $_POST['userid'] . " AND `meta_key`='user_meta_current_practice'");
                $html_practice = '';
                foreach ($get_practice as $key => $value) {
                    $name[] = unserialize($value->value);
                }

                $name = array_sort($name, 'practices_name', SORT_ASC);

                foreach ($name as $key => $value) {
//                    echo '<pre>';
//                    print_r($value);
//                    exit;
                    $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $user_mail . "' AND b.meta_value='" . $value['practices_name'] . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
                    foreach ($manis_done as $practice) {
                        $date_manis = get_post_meta($practice->ID, 'wpcf-date-center', true);
                        if ($date_manis > $meta_data['wpcf-acc-start-date'][0] && $date_manis < $meta_data['wpcf-acc-end-date'][0]) {
                            $total_user_practice += get_post_meta($practice->ID, 'wpcf-manis-done', true);
                        }
                    }
                    $target = ($value['target'] > 0) ? " of " . number_format($value['target']) : "";
                    $starting_count = get_starting_count($value['practices_name'], $accumulation);
                    $html_practice .= '<div class="group-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="row ' . $key . '">
                        <div class="main-content">
                            <div class="text-left col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="row practices_name">
                                   <p class="title">' . $value['practices_name'] . '</p>
                                 </div>
                            </div>
                            <div class="text-right col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="row practices_button details">
                                   <a class="button details" href="javascript:;" onclick="App.editPractices(\'' . $value['practices_name'] . '\',' . $key . ',\'' . number_format($total_user_practice + $starting_count) . $target . '\')">' . __('details', 'Divi') . '</a>
                                   <a class="button load display" href="javascript:;">Load...</a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="row">
                                    <input type="text" class="count_practice" value="' . number_format($total_user_practice + $starting_count) . $target . '" readonly>
                                </div>
                            </div>
                         </div>
                         <div class="edit-content">
                         </div>
                    </div>
              </div>';
                    $total_user_practice = 0;
                    $target = '';
                }
//                exit;
                echo json_encode(array(
                    'html' => $html_practice,
                    'msg' => '',
                    'err' => 0,
                ));
                exit;
            endif;
        }
    } else {
        echo json_encode(array(
//            'msg' => (get_option('add_center') ? get_option('add_center') : ""),
            'msg' => 'User does not exist',
            'err' => 1,
        ));
        exit;
    }
}

/*
 * updatePractices
 * update data Practices trong profile
 */
add_action('wp_ajax_updatePractices', 'updatePractices');
add_action('wp_ajax_nopriv_updatePractices', 'updatePractices');

function updatePractices()
{
    if ($_POST['userid']) {
        global $wpdb;
        $current_user = wp_get_current_user();
        if (0 != $current_user->ID) {
            $user_mail = $current_user->user_login;
        }
        $check = $sumCount = 0;
        $args = array(
            'numberposts' => -1,
            'post_type' => 'accumulations',
            'post_status' => 'private',
            'meta_key' => 'wpcf-acc-start-date',
            'orderby' => 'meta_value',
        );
        $postslist = get_posts($args);
        $check_practice = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $_POST['userid'] . " AND `meta_key`='user_meta_current_practice' AND `meta_value` LIKE '%" . $_POST['name'] . "%'");
        if ($check_practice[0]->umeta_id) {
            $data_value = unserialize($check_practice[0]->meta_value);
            foreach ($postslist as $key => $item) {
                //check co ton tai bo loc hay khong
                if ($_POST['accumulation'] == $item->ID) {
                    $data_value['starting_count_' . $item->ID] = $_POST['starting_count'];
                    $data_value['starting_count_' . $key] = $_POST['starting_count'];
                    $sumCount += $_POST['starting_count'];
                    $data_value['date_target_' . $item->ID] = $_POST['target'];
                    $sumTarget += $_POST['target'];
                } else {
//                    $data_value['starting_count_' . $item->ID] = 0;
//                    $data_value['starting_count_' . $key] = 0;
//                    $data_value['date_target_' . $item->ID] = 0;
//                    $data_value['date_target_' . $key] = 0;
                }
            }
            if ($data_value['practices_name'] != $_POST['Practices']) {
                $check_practice_curent = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $_POST['userid'] . " AND `meta_key`='user_meta_current_practice' AND `meta_value` LIKE '%" . $_POST['Practices'] . "%'");
                foreach ($check_practice_curent as $value) {
                    $name = unserialize($value->meta_value);
                    if ($name['practices_name'] == $_POST['Practices']) {
                        $check = 1;
                    }
                }
                if ($check == 1) {
                    echo json_encode(array(
                        'msg' => 'Practices is registered',
                        'err' => 1,
                    ));
                    exit;
                } else {
                    $wpdb->get_results("UPDATE " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b SET b.meta_value='" . $_POST['Practices'] . "' WHERE a.`post_title`='" . $user_mail . "' AND b.meta_value='" . $data_value['practices_name'] . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
                    $data_value['practices_name'] = $_POST['Practices'];
                    $data_value['practice_description'] = $_POST['description'];
//                    $data_value['starting_count'] = $sumCount;
//                    $data_value['target'] = $sumTarget;
                    $wpdb->get_results("UPDATE " . $wpdb->prefix . "usermeta SET meta_value='" . serialize($data_value) . "' WHERE umeta_id=" . $check_practice[0]->umeta_id . " and `user_id`=" . $_POST['userid'] . " AND `meta_key`='user_meta_current_practice'");
                }
            } else {
                $wpdb->get_results("UPDATE " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b SET b.meta_value='" . $_POST['Practices'] . "' WHERE a.`post_title`='" . $user_mail . "' AND b.meta_value='" . $data_value['practices_name'] . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
                $data_value['practices_name'] = $_POST['Practices'];
                $data_value['practice_description'] = $_POST['description'];
//                $data_value['starting_count'] = $sumCount;
///                $data_value['date_starting_count'] = ($_POST['starting_count'] > 0) ? strtotime(date('d-m-Y H:i:s')) : 0;
                $data_value['target_practice_all'] = $_POST['target'];
///                $data_value['date_target'] = ($_POST['target'] > 0) ? strtotime(date('d-m-Y H:i:s')) : 0;
                $wpdb->get_results("UPDATE " . $wpdb->prefix . "usermeta SET meta_value='" . serialize($data_value) . "' WHERE umeta_id=" . $check_practice[0]->umeta_id . " and `user_id`=" . $_POST['userid'] . " AND `meta_key`='user_meta_current_practice'");
            }
            $meta_acc = get_post_meta($_POST['accumulation']);
            $check_save_practice = check_save_count($meta_acc, $_POST['Practices']);

            if (count($check_save_practice) == 0 && $sumCount > 0) {

                $wpdb->insert(
                    'manis_user_practice_starting_count_date',
                    array(
                        'accumulation' => $_POST['accumulation'],
                        'user_id' => $_REQUEST['userid'],
                        'practices' => $_POST['Practices'],
                        'starting_count' => $sumCount,
                        'date_save' => date('Y-m-d'),
                        'date_modify' => date('Y-m-d'),
                    ),
                    array(
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s'
                    )
                );
            }
            update_user_meta($_REQUEST['userid'], 'user_meta_my_target', preg_replace('/[^a-zA-Z0-9]/', '', (int)getTargetUser()));
            $get_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $_POST['userid'] . " AND `meta_key`='user_meta_current_practice'");
            $html_practice = '';
            if (isset($_COOKIE['accumulation'])) {
                $accumulation = $_COOKIE['accumulation'];
                $meta_data = get_post_meta($accumulation);
            }
            foreach ($get_practice as $key => $value) {
                $name[] = unserialize($value->value);
            }

            $name = array_sort($name, 'practices_name', SORT_ASC);

            foreach ($name as $key => $value) {
                $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $user_mail . "' AND b.meta_value='" . $value['practices_name'] . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
                foreach ($manis_done as $practice) {
                    $date_manis = get_post_meta($practice->ID, 'wpcf-date-center', true);
                    if ($date_manis > $meta_data['wpcf-acc-start-date'][0] && $date_manis < $meta_data['wpcf-acc-end-date'][0]) {
                        $total_user_practice += get_post_meta($practice->ID, 'wpcf-manis-done', true);
                    }
                }
                $target = ($value['target'] > 0) ? " of " . number_format($value['target']) : "";

                $html_practice .= '<div class="group-box col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="row ' . $key . '">
                        <div class="main-content">
                            <div class="text-left col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="row practices_name">
                                   <p class="title">' . $value['practices_name'] . '</p>
                                 </div>
                            </div>
                            <div class="text-right col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                <div class="row practices_button details">
                                   <a class="button details" href="javascript:;" onclick="App.editPractices(\'' . $value['practices_name'] . '\',' . $key . ',\'' . number_format($total_user_practice + get_starting_count($value['practices_name'], $_POST['accumulation'])) . $target . '\')">' . __('details', 'Divi') . '</a>
                                   <a class="button load display" href="javascript:;">Load...</a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="row">
                                    <input type="text" class="count_practice" value="' . number_format($total_user_practice + get_starting_count($value['practices_name'], $_POST['accumulation'])) . $target . '" readonly>
                                </div>
                            </div>
                         </div>
                         <div class="edit-content">
                         </div>
                    </div>
              </div>';
                $total_user_practice = 0;
                $target = '';
            }
            echo json_encode(array(
                'html_practice' => $html_practice,
                'msg' => '',
                'err' => 0,
            ));
            exit;
        }
    } else {
        echo json_encode(array(
            'msg' => 'User does not exist',
            'err' => 1,
        ));
        exit;
    }
}

/*
 * addNewPractice
 */
add_action('wp_ajax_addNewPractice', 'addNewPractice');
add_action('wp_ajax_nopriv_addNewPractice', 'addNewPractice');

function addNewPractice()
{
    $current_user = wp_get_current_user();
    if (0 != $current_user->ID) {
        $user_id = $current_user->ID;
    }
    if ($user_id) {
        $user_info = get_userdata($user_id);
        $post_id = wp_insert_post(array(
            'post_title' => $_POST['practice_name'],
            'post_type' => 'practic',
            'post_status' => 'private',
        ));
        echo json_encode(array(
            'add' => '<option value="' . $_POST['practice_name'] . '">' . $_POST['practice_name'] . '</option>',
            'practice_name' => $_POST['practice_name'],
            'msg' => 'Add practice successful',
            'err' => 0,
        ));
        exit;
    } else {
        echo json_encode(array(
            'msg' => 'User does not exist',
            'err' => 1,
        ));
        exit;
    }
}

/*
 * formAddPractice
 * html form Add Practice
 */

function formAddPractice()
{

    $html = '<div id="formAddPractice" class="formAddPractice display" name="formAddPractice">
        <label class="label">' . __('Practice name', 'Divi') . '</label>
        <input type="text" id="practice-name" name="practice-name" class="practice-name" value="" placeholder="Practice name"/> ';
    $html .= '<div class="text-center">
        <div class="submit addnewpractice text-center">
            <a class="button" href="javascript:;" onclick="App.formAddPractice()">' . __('Add Practice!', 'Divi') . '</a>
        </div>';
    $html .= '<div class="submit cancelnewpractice text-center">
            <a class="button" href="javascript:;" onclick="App.formCancelPractice()">' . __('Cancel!', 'Divi') . '</a>
        </div>
        </div>
    </div> ';
    return $html;
}

/*
 * Accumulation_practices
 * Get data if user change acc in page profile(chi tiet practices)
 */

function Accumulation_practices($post_value)
{
    $total_user_practice = 0;
    $value = unserialize(str_replace('\\', '', $post_value['data']));
    $current_user = wp_get_current_user();
    $meta_data = get_post_meta($post_value['id']);
    if (0 != $current_user->ID) {
        $user_id = $current_user->ID;
    }
    if ($user_id) {
        $user_info = get_userdata($user_id);
        global $wpdb;
        $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $current_user->user_login . "' AND b.meta_value='" . $value['practices_name'] . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
        foreach ($manis_done as $practice) {
            $date_manis = get_post_meta($practice->ID, 'wpcf-date-center', true);
            if ($post_value['id'] != '') {
                if ($date_manis > $meta_data['wpcf-acc-start-date'][0] && $date_manis < $meta_data['wpcf-acc-end-date'][0]) {
                    $total_user_practice += get_post_meta($practice->ID, 'wpcf-manis-done', true);
                }
            } else {
                $total_user_practice += get_post_meta($practice->ID, 'wpcf-manis-done', true);
            }
        }

        $target_number = $starting_count = $tagetAll = 0;
        if ($post_value['id'] != '') {
//            $starting_count = ($value['starting_count_' . $post_value['id']] > 0) ? $value['starting_count_' . $post_value['id']] : 0;
            $starting_count = get_starting_count($value['practices_name'], $post_value['id']);
            $check = ((strtotime(date('d-m-Y H:i:s')) > $meta_data['wpcf-acc-start-date'][0]) && (strtotime(date('d-m-Y H:i:s')) < $meta_data['wpcf-acc-end-date'][0])) ? 1 : 0;
            if ($value['date_target_' . $post_value['id']] > 0) {
                $target = ($value['date_target_' . $post_value['id']] > 0) ? " of " . number_format($value['date_target_' . $post_value['id']]) : "";
                $target_number = $value['date_target_' . $post_value['id']];
            }
        } else {
            $starting_count = get_starting_count($value['practices_name']);
            $target_number = $value['target_practice_all'];
            $target = ($target_number > 0) ? " of " . number_format($target_number) : "";
            $check = 0;
            $tagetAll = 1;
        }
        $sum = number_format($total_user_practice + $starting_count);
        echo json_encode(array(
            'err' => 0,
            'type' => 4,
            'id' => $post_value['id'],
            'sum' => $sum . $target,
            'target_number' => $target_number,
            'check' => $check,
            'starting_count' => $starting_count,
            'taget_all' => $tagetAll,
        ));
        exit;
    }
}
