<?php
/**
 * Created by PhpStorm.
 * User: ANH
 * Date: 4/28/2016
 * Time: 9:42 AM
 */


/*
 * formAddCenter
 * Html form Add Center
 */

function formAddCenter()
{

    $html = '<div id="formAddCenter" class="formAddCenter" name="formAddCenter">
        <label class="label">' . __('Center name', 'Divi') . '</label>
        <input type="text" id="center-name" name="center-name" class="center-name" value="" placeholder="Center name"/>
        <label class="label">' . __('Region', 'Divi') . '</label>';
    $html .= get_all_region_html();
    $html .= '<div class="submit addcenter">
            <a class="button" href="javascript:;" onclick="App.formAddCenter()">' . __('Add Center!', 'Divi') . '</a>
        </div>
    </div> ';
    return $html;
}

/*
 * addNewCenter
 * add new center tu profile
 */
add_action('wp_ajax_addNewCenter', 'addNewCenter');
add_action('wp_ajax_nopriv_addNewCenter', 'addNewCenter');

function addNewCenter()
{
    $user_id = username_exists($_POST['email']);
    if ($user_id) {
        $user_info = get_userdata($user_id);
        $post_id = wp_insert_post(array(
//            'post_author' => $user_info->display_name,
            'post_title' => $_POST['center_name'],
            'post_type' => 'list-center',
            'post_status' => 'private',
        ));
        add_post_meta($post_id, 'my-region', $_POST['region']);
        echo json_encode(array(
//            'msg' => (get_option('add_center') ? get_option('add_center') : ""),
            'add' => '<option value="' . $_POST['center_name'] . '">' . $_POST['center_name'] . '</option>',
            'name' => $_POST['center_name'],
            'msg' => 'Add center successful',
            'err' => 0,
        ));
        exit;
    } else {
        echo json_encode(array(
//            'msg' => (get_option('add_center') ? get_option('add_center') : ""),
            'msg' => 'User does not exist',
            'err' => 1,
        ));
        exit;
    }
}