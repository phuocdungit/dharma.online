<?php

add_action('wp_ajax_ajax_list_center', 'ajax_list_center');
add_action('wp_ajax_nopriv_ajax_list_center', 'ajax_list_center');

function ajax_list_center() {
    $args = array(
        'numberposts' => -1,
        'post_type' => 'list-center',
    );
    $postslist = get_posts($args);
    $respose = array(
        'items' => $postslist
    );

    echo json_encode($respose);
    exit;
}

add_action('wp_ajax_get_page_about', 'get_page_about');
add_action('wp_ajax_nopriv_get_page_about', 'get_page_about');

function get_page_about() {
    $respose = array(
        'item' => get_post(2330)
    );
    echo json_encode($respose);
    exit;
}

add_action('wp_ajax_insertCenter', 'insertCenter');
add_action('wp_ajax_nopriv_insertCenter', 'insertCenter');

function insertCenter() {
    $request_body = file_get_contents('php://input');
    $item = json_decode($request_body);
    $item->center = html_entity_decode($item->center);
    global $wpdb;
    $manisdone = preg_replace('/[^a-zA-Z0-9\-_]/', '', $item->manisdone);

    $results = $wpdb->get_results("SELECT COUNT(user_email) AS user FROM " . $wpdb->prefix . "users WHERE user_email ='" . $item->email . "'");
    if ($results[0]->user > 0) {
        $post_id = wp_insert_post(array(
            'post_author' => $item->email,
            'post_title' => $item->email,
            'post_type' => 'manis-history',
            'post_status' => 'publish',
        ));
        add_post_meta($post_id, 'wpcf-my-center', $item->center);
        add_post_meta($post_id, 'wpcf-manis-done', $manisdone);
        add_post_meta($post_id, 'wpcf-date-center', strtotime(date('d-m-Y H:i:s')));
        sendMail($item->email, $item->center, $manisdone);
        echo json_encode(array(
            'msg' => 'add',
            'err' => 0,
        ));
        exit;
    } else {
        $user_id = username_exists($item->email);
        if (!$user_id and email_exists($item->email) == false) {
            $user_id = wp_create_user($item->email, 123456, $item->email);
            update_user_meta($user_id, 'user_meta_leaderboard_list', 2);
            update_user_meta($user_id, 'user_meta_center_filter', $item->center);
        }
//        update_post_meta($post->ID, 'wpcf-count', $sum_all_center);
        $post_id = wp_insert_post(array(
            'post_author' => $item->email,
            'post_title' => $item->email,
            'post_type' => 'manis-history',
            'post_status' => 'publish',
        ));

        add_post_meta($post_id, 'wpcf-my-center', $item->center);
        add_post_meta($post_id, 'wpcf-manis-done', $manisdone);
        add_post_meta($post_id, 'wpcf-date-center', strtotime(date('d-m-Y H:i:s')));
        sendMail($item->email, $item->center, $manisdone);
        echo json_encode(array(
            'msg' => 'add_user',
            'err' => 0,
        ));
        exit;
    }
    exit;
}

add_action('wp_ajax_checkTotal', 'checkTotal');
add_action('wp_ajax_nopriv_checkTotal', 'checkTotal');

//return value 
function checkTotal() {
    $request_body = file_get_contents('php://input');
    $item = json_decode($request_body);
    global $wpdb;
    $user_id = username_exists($item->email);
    $center_select = '';
    if ($user_id) {
        $center_select = get_the_author_meta('user_meta_center_filter', $user_id);
        $personal_target = get_the_author_meta('user_meta_my_target', $user_id);
    }
    $days_left = $total_user = $total_user_center = 0;
    $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $item->email . "' AND b.meta_value='" . $center_select . "' AND a.`ID` = b.`post_id` ");
    $total_user_center = $wpdb->get_results("SELECT SUM(b.`meta_value`)AS sum FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $item->email . "' AND b.meta_key='wpcf-manis-done' AND a.`ID` = b.`post_id` ");

    foreach ($manis_done as $key => $value) {
        $total_user+= get_post_meta($value->ID, 'wpcf-manis-done', true);
    }

    $args = array(
        'numberposts' => -1,
        'post_type' => 'manis-history',
        'orderby' => 'name',
        'order' => 'ASC',
    );
    $postslist = get_posts($args);
    $running_total_yesterday = $day_count_today = $total_all_center = 0;
    foreach ($postslist as $key => $post) {
        $meta_post = get_post_meta($post->ID);
        $total_all_center+=$meta_post['wpcf-manis-done'][0];
        if ($post->post_title == $item->email) {
            if (strtotime(date('d-m-Y')) > $meta_post['wpcf-date-center'][0]) {
                $running_total_yesterday+=$meta_post['wpcf-manis-done'][0];
            } else {
                $day_count_today+=$meta_post['wpcf-manis-done'][0];
            }
        }
    }
    $getpost = get_page_by_title($center_select, OBJECT, 'list-center');
    $center_target = get_post_meta($getpost->ID, 'wpcf-center_target', true);
    $total_target = get_option('total_target');
//    $center_target = get_option('center_target');
//    $personal_target = get_option('personal_target');
    $start_date = get_option('start_date');
    $end_date = get_option('end_date');

    if (strtotime($end_date) >= strtotime(date('d-m-Y')) && strtotime(date('d-m-Y')) >= strtotime($start_date)) {
        $datediff = abs(strtotime(date('d-m-Y')) - strtotime($end_date));
        $days_left = floor($datediff / (60 * 60 * 24)) + 1;
        $daily_target = round(($personal_target - $running_total_yesterday) / $days_left);
        $left_today = $daily_target - $day_count_today;
    }

    $show_days_left = ($personal_target > 0) ? 1 : 0;
    if ($item->email && $center_select && $total_user > 0) {
        echo json_encode(array(
            'msg' => 'send',
            'err' => 0,
            'total_center' => $total_user_center[0]->sum,
//            'total_center_target' => get_post_meta($id_post, 'wpcf-center_target', true),
            'total_center_target' => $center_target,
            'total_all_center' => $total_all_center,
            'total_all_center_target' => $total_target,
            'name' => $post->post_title,
            'total_user' => $total_user,
            'total_user_target' => $personal_target,
            'left_today' => $left_today,
            'daily_target' => $daily_target,
            'running_total_yesterday' => $running_total_yesterday,
            'center_select' => $center_select,
            'id' => $post->ID,
            'email' => $item->email,
            'show_days_left' => $show_days_left,
        ));
        exit;
    }
}

add_action('init', 'handle_preflight');

function handle_preflight() {
// Set the domain that's allowed to make the API call.
    //header("Access-Control-Allow-Origin: *");
// Set the methods
    header("Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE");

    header("Access-Control-Allow-Headers: Origin, Content-Type, Accept");

    if ('OPTIONS' == $_SERVER['REQUEST_METHOD']) {
        status_header(200);
        exit();
    }
}
