<?php
/**
 * Created by PhpStorm.
 * User: ANH
 * Date: 4/25/2016
 * Time: 8:55 AM
 */
global $total_user_practice;

// add custom js for admin
function my_enqueue() {
    wp_enqueue_style('select2-css', get_stylesheet_directory_uri() . '/assets/vendor/select2/css/select2.min.css', false, '1.0.0');
    wp_enqueue_script('select2-script', get_stylesheet_directory_uri() . '/assets/vendor/select2/js/select2.min.js');
    wp_enqueue_script('my-custom-script', get_stylesheet_directory_uri() . '/assets/js/custom_admin.js');
}

add_action('admin_enqueue_scripts', 'my_enqueue');

// add custom style for admin

function admin_style() {
    wp_enqueue_style('my-admin-styles', get_stylesheet_directory_uri() . '/assets/css/admin.css');
}

add_action('admin_enqueue_scripts', 'admin_style');

add_action('add_meta_boxes', 'boot_add_accumulations_meta');

function boot_add_accumulations_meta() {
    //add_meta_box('add_custom_accumulations', 'Targets & totals for accumulations', 'count_center_accumulations_meta', 'accumulations');
    //add_meta_box('add_custom_region', 'Targets & totals for Region', 'count_center_region_meta', 'region');
    //add_meta_box('add_custom_list-center', 'Targets & totals for center', 'count_center_list_center_meta', 'list-center');
}

// key = 1 page user detail
function wpcodex_add_excerpt_support_for_pages() {
    add_post_type_support('dedications', 'author');
}

add_action('init', 'wpcodex_add_excerpt_support_for_pages');

add_filter('wp_dropdown_users', 'change_list_author');

function change_list_author($output) {
    global $post;
    $args = array(
        'orderby' => 'display_name',
        'order' => 'ASC'
    );
    $users = get_users($args);
    $output = '<div id="swpm_form_items_meta_box"></div><select id="post_author_override" name="post_author_override" class="choosen-select2">';
    foreach ($users as $user) {
        $sel = ($post->post_author == $user->ID) ? "selected='selected'" : '';
        $output .= '<option value="' . $user->ID . '"' . $sel . '>' . $user->display_name . '</option>';
    }
    $output .= "</select>";

    return $output;
}

function admin_get_all_practice_html($value = '', $key = 0) {
    if (is_admin()):
        $args = array(
            'numberposts' => -1,
            'post_type' => 'practic',
            'post_status' => 'private',
            'orderby' => 'name',
            'order' => 'ASC',
        );
        $postslist = get_posts($args);
        $html = '<select name="practice_filter" class="practice_filter">';
        //<option value="">' . __('Please choose...', 'Divi') . '</option>';
        foreach ($postslist as $term) {
            $select = ($value == $term->post_title) ? 'selected' : '';
            $html .= '<option ' . $select . ' value="' . $term->post_title . '">' . $term->post_title . '</option>';
        }
        $html .= '</select>';
        return $html;
    else:
        return false;
    endif;
}

/*
 * get_all_accumulations_html
 * get all region xuat ra html ben ngoai
 * key = 1 page user detail
 * key = 2 page list_center
 */

function admin_get_all_accumulations_html($key = 0, $user_id = 0) {
    if (is_admin()):
        $args = array(
            'numberposts' => -1,
            'post_type' => 'accumulations',
            'post_status' => 'private',
            'meta_key' => 'wpcf-acc-start-date',
            'orderby' => 'meta_value',
        );
        $postslist = get_posts($args);
        $list_current = check_current_accumulations();
        if (isset($_COOKIE['accumulation'])) {
            $value = $_COOKIE['accumulation'];
        } else {
            if ($list_current):
                $value = $list_current;
                setcookie(accumulation, $value, time() + (86400 * 30), "/"); // 86400 = 1 day
            else:
                $list_current = '';
                $value = $postslist[0]->ID;
                setcookie(accumulation, $value, time() + (86400 * 30), "/"); // 86400 = 1 day
            endif;
        }
        $name = 'accumulation_filter';
        if ($key == 1):
            $html = '<select data-key="' . $key . '" name="' . $name . '" class="' . $name . '" onchange="App.AdminAccumulation(' . $key . ',' . $user_id . ')">';
        else:
            $html = '<select data-key="' . $key . '" name="' . $name . '" class="' . $name . '" onchange="App.Accumulation(' . $key . ')">';
        endif;
        foreach ($postslist as $key => $term) {
            if ($term->ID == $list_current):
                $curren = '(current)';
            else:
                $curren = '';
            endif;
            $select = (get_the_title($value) == $term->post_title) ? 'selected' : '';
            $html .= '<option id="' . $term->ID . '" ' . $select . ' value="' . $term->ID . '">' . $term->post_title . ' ' . $curren . '</option>';
        }
        if ($value) {
            $html .= '<option value="">' . __('All', 'Divi') . '</option></select>';
        } else {
            $html .= '<option selected value="">' . __('All', 'Divi') . '</option></select>';
        }

        return $html;
    else:
        return false;
    endif;
}

// custom Targets & totals layout for accumulation

function count_center_accumulations_meta($accumulation_id) {
    global $wpdb;
    //get meta data of acc
    $meta_data = get_post_meta($accumulation_id->ID);
    $get_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `meta_key`='user_meta_current_practice'");
    foreach ($get_practice as $key => $value) {
        $name[] = unserialize($value->value);
    }
    $total_user_practice = 0;
    $name = array_sort($name, 'practices_name', SORT_ASC);
    $html = '<div class="table-responsive">
				<table class="table table-bordered table-hover table-striped">
					<thead>
					<tr>
						<th>Practices</th>
						<th>Target</th>
						<th>Total count</th>
						</tr>
					</thead>
			<tbody>';
    foreach ($name as $key => $value) {
        $target = number_format($value['date_target_' . $accumulation_id->ID]);
        if ($target > 0):
            $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE b.meta_value='" . $value['practices_name'] . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
            foreach ($manis_done as $practice) {
                $date_manis = get_post_meta($practice->ID, 'wpcf-date-center', true);
                if ($date_manis > $meta_data['wpcf-acc-start-date'][0] && $date_manis < $meta_data['wpcf-acc-end-date'][0]) {
                    $total_user_practice += get_post_meta($practice->ID, 'wpcf-manis-done', true);
                }
            }
            $html .= '<tr>
					<td>' . $value['practices_name'] . '</td>
					<td>' . $target . '</td>
					<td>' . $total_user_practice . '</td>
				  </tr>';
        endif;
        $total_user_practice = 0;
        $target = 0;
    }
    $html .= '</tbody>
				</table>
		</div>';
    echo $html;
}

//// custom Targets & totals layout for list center

function count_center_list_center_meta($center_id) {
    global $wpdb;
    //get meta data of acc
    $data_id = check_current_accumulations();
    $meta_data = get_post_meta($data_id);
    $get_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `meta_key`='user_meta_current_practice'");
    foreach ($get_practice as $key => $value) {
        $name[] = unserialize($value->value);
    }
    $total_user_practice = 0;
    $name = array_sort($name, 'practices_name', SORT_ASC);
    $html = '<div class="table-responsive">';
    $html .= admin_get_all_accumulations_html($key = 1, $user = 0);
    $html .= '           <br/><br/><table class="table table-bordered table-hover table-striped">
					<thead>
					<tr>
						<th>Practices</th>
						<th>Target</th>
						<th>Total count</th>
						</tr>
					</thead>
			<tbody>';
    foreach ($name as $key => $value) {
        $target = number_format($value['date_target_' . $data_id]);
        if ($target > 0):
            $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE b.meta_value='" . $value['practices_name'] . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");

            foreach ($manis_done as $practice) {
                $date_manis = get_post_meta($practice->ID, 'wpcf-date-center', true);
                if ($date_manis > $meta_data['wpcf-acc-start-date'][0] && $date_manis < $meta_data['wpcf-acc-end-date'][0]) {
                    $total_user_practice += get_post_meta($practice->ID, 'wpcf-manis-done', true);
                }
            }

            $html .= '<tr>
					<td>' . $value['practices_name'] . '</td>
					<td>' . $target . '</td>
					<td>' . $total_user_practice . '</td>
				  </tr>';
        endif;
        $total_user_practice = 0;
        $target = 0;
    }
    $html .= '</tbody>
				</table>
		</div>';
    echo $html;
}

//// custom Targets & totals layout for list center

function count_center_region_meta($center_id) {
    global $wpdb;
    //get meta data of acc
    $data_id = check_current_accumulations();
    $meta_data = get_post_meta($data_id);
    $get_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `meta_key`='user_meta_current_practice'");
    foreach ($get_practice as $key => $value) {
        $name[] = unserialize($value->value);
    }
    $total_user_practice = 0;
    $name = array_sort($name, 'practices_name', SORT_ASC);
    $html = '<div class="table-responsive">';
    $html .= admin_get_all_accumulations_html($key = 1, $user = 0);
    $html .= '           <br/><br/><table class="table table-bordered table-hover table-striped">
					<thead>
					<tr>
						<th>Practices</th>
						<th>Target</th>
						<th>Total count</th>
						</tr>
					</thead>
			<tbody>';
    foreach ($name as $key => $value) {
        $target = number_format($value['date_target_' . $data_id]);
        if ($target > 0):
            $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE b.meta_value='" . $value['practices_name'] . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");

            foreach ($manis_done as $practice) {
                $date_manis = get_post_meta($practice->ID, 'wpcf-date-center', true);
                if ($date_manis > $meta_data['wpcf-acc-start-date'][0] && $date_manis < $meta_data['wpcf-acc-end-date'][0]) {
                    $total_user_practice += get_post_meta($practice->ID, 'wpcf-manis-done', true);
                }
            }

            $html .= '<tr>
					<td>' . $value['practices_name'] . '</td>
					<td>' . $target . '</td>
					<td>' . $total_user_practice . '</td>
				  </tr>';
        endif;
        $total_user_practice = 0;
        $target = 0;
    }
    $html .= '</tbody>
				</table>
		</div>';
    echo $html;
}

add_action('save_post', 'boot_save_custom_meta_box');

function boot_save_custom_meta_box($post_id) {

}

/*
 * denied_add_custom_user_profile_fields
 * Them field cho user trong profile
 */

function denied_add_custom_user_profile_fields($user) {
    ?>
    <div class="denied-leaderboard">
        <table class="form-table">
            <tr>
                <th>
                    <label for="address"><?php _e('This is a group collection', 'Divi'); ?>
                    </label></th>
                <td>
                    <input type="checkbox"
                           name="denied-leaderboard"<?php echo (get_the_author_meta('user_meta_leaderboard_list', $user->ID) == 1) ? 'checked="checked"' : '' ?>
                           value="1"/>
                </td>
            </tr>
            <tr>
                <th>
                    <label for="address"><?php _e('Description of the group', 'Divi'); ?>
                    </label></th>
                <td>
                    <textarea rows="4" cols="50" class="group-description"
                              name="group-description"><?php echo get_the_author_meta('user_meta_group_description', $user->ID) ?> </textarea>
                </td>
            </tr>
            <tr>
                <th>
                    <label for="set-ontop"><?php _e('On top', 'Divi'); ?>
                    </label></th>
                <td>
                    <input type="checkbox"
                           name="set-ontop" <?php echo (get_the_author_meta('user_meta_ontop', $user->ID) == 1) ? 'checked="checked"' : '' ?>
                           value="1"/>
                </td>
            </tr>
            <tr>
                <th>
                    <label for="address"><?php _e('my Center', 'Divi'); ?>
                    </label></th>
                <td>
                    <?php get_all_center(get_the_author_meta('user_meta_center_filter', $user->ID)); ?>
                </td>
            </tr>
            <tr>
                <th>
                    <label for="address"><?php _e('my Target', 'Divi'); ?>
                    </label></th>
                <td>
                    <input class="my_target" name="my_target" type="text"
                           value="<?php echo get_the_author_meta('user_meta_my_target', $user->ID); ?>">
                </td>
            </tr>
            <tr>
                <th>
                    <label for="address"><?php _e('Targets & totals', 'Divi'); ?>
                    </label></th>
                <td>
                    <div class="table-responsive">

                        <?php echo "Accumulations : " . admin_get_all_accumulations_html($key = 1, $user->ID); ?>
                        <br/><br/>
                        <table class="table table-bordered table-hover table-striped target_totals_user">
                            <thead>
                            <tr>
                                <th>Practices</th>
                                <th>Target</th>
                                <th>Mantra</th>
                                <th>Accumulated before joining</th>
                                <th>Total Count</th>
                                <th>Archive</th>
                            </tr>
                            </thead>
                            <tbody class="data_target_totals_user">
                            <?php
                            global $wpdb;
                            $sql = "SELECT * FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $_GET['user_id'] . " AND `meta_key`='user_meta_current_practice'";
                            $get_practice = $wpdb->get_results($sql);
                            foreach ($get_practice as $key => $value) {
                                $name[] = unserialize($value->meta_value);
                            }
                            $data_id = check_current_accumulations();
                            $meta_data = get_post_meta($data_id);
                            $name = array_sort($name, 'practices_name', SORT_ASC);
                            $userdata = get_userdata($_GET['user_id']);
                            if ($name) {
                                foreach ($name as $key => $value) {
                                    $post = get_page_by_title($value['practices_name'], OBJECT, 'practic');
                                    $postMeta = get_post_meta($post->ID, 'wpcf-mantra', true);
                                    $practice_description = ($value['practice_description'] == '') ? $postMeta : $value['practice_description'];
                                    $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $userdata->user_login . "' AND b.meta_value='" . $value['practices_name'] . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
                                    foreach ($manis_done as $practice) {
                                        $date_manis = get_post_meta($practice->ID, 'wpcf-date-center', true);
                                        if ($date_manis > $meta_data['wpcf-acc-start-date'][0] && $date_manis < $meta_data['wpcf-acc-end-date'][0]) {
                                            $total_user_practice += get_post_meta($practice->ID, 'wpcf-manis-done', true);
                                        }
                                    }
                                    echo '<tr class="pracices_' . strtolower(str_replace(' ', '_', $value['practices_name'])) . '">
								 <td ondbclick="App.admin_editPractices()">' . $value['practices_name'] . '</td>
								 <td class="target" onclick="App.admin_editPractices()">' . number_format(sum_all_target_count($value)) . '</td>
								<td class="description" onclick="App.admin_editPractices()">' . $practice_description . '</td>
								<td class="starting_count" onclick="App.admin_editPractices()">' . number_format(sum_all_starting_count($value)) . '</td>
								<td class="total_count" onclick="App.admin_editPractices()">' . number_format($total_user_practice) . '</td>
								 <td class="archive_status"><input type="checkbox" checked/></td>
							</tr>';
                                    $total_user_practice = 0;
                                }
                            }
                            ?>
                            </tbody>
                            <?php echo "Practice : " . admin_get_all_practice_html($value = '', $key = 1); ?>
                            <?php
                            echo '<input type="button" value="Add" /> ';
                            echo '<input type="hidden" name="user_id" value="' . (isset($_GET['user_id']) ? $_GET['user_id'] : '') . '" />';
                            ?>
                            <br/><br/>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <?php
}

/*
 * denied_save_custom_user_profile_fields
 * Save data custom khi update profile
 */

function denied_save_custom_user_profile_fields($user_id) {
    global $wpdb;
    $denied_dmail = 2;
    if ($_POST['denied-leaderboard']):
        $denied_dmail = 1;
    endif;
    $setOntop = 0;
    if ($_POST['set-ontop']) {
        $setOntop = 1;
    }
    if ($_POST['center_filter']) {
        $wpdb->get_results('UPDATE ' . $wpdb->prefix . 'posts a INNER JOIN ' . $wpdb->prefix . 'postmeta b'
            . ' SET b.`meta_value`="' . $_POST['center_filter'] . '" '
            . 'WHERE a.`post_title`="' . $_POST['email'] . '" AND a.`post_status`="private" AND a.`ID`=b.`post_id` AND b.`meta_key`="wpcf-my-center"');
    }
    update_usermeta($user_id, 'user_meta_leaderboard_list', $denied_dmail);
    update_usermeta($user_id, 'user_meta_group_description', $_POST['group-description']);
    update_usermeta($user_id, 'user_meta_ontop', $setOntop);
    update_usermeta($user_id, 'user_meta_center_filter', $_POST['center_filter']);
    update_usermeta($user_id, 'user_meta_my_target', $_POST['my_target']);
}

add_action('show_user_profile', 'denied_add_custom_user_profile_fields');
add_action('edit_user_profile', 'denied_add_custom_user_profile_fields');

add_action('personal_options_update', 'denied_save_custom_user_profile_fields');
add_action('edit_user_profile_update', 'denied_save_custom_user_profile_fields');


add_action('wp_ajax_Accumulation_Admin', 'Accumulation_Admin');
add_action('wp_ajax_nopriv_Accumulation_Admin', 'Accumulation_Admin');

function Accumulation_Admin() {
    if ($_POST['key'] == 1):
        Accumulation_admin_profile($_POST);
    endif;
    exit;
}

function Accumulation_admin_profile($post_value) {
    $user_id = $post_value['user_id'];
    $userdata = get_userdata($post_value['user_id']);
    $meta_data = get_post_meta($post_value['id']);
    if ($user_id) {
        $user_info = get_userdata($user_id);
        global $wpdb;
        $get_practice = $wpdb->get_results("SELECT meta_value as value FROM " . $wpdb->prefix . "usermeta WHERE `user_id`=" . $user_id . " AND `meta_key`='user_meta_current_practice'");
        $html_practice = '<table class="table table-bordered table-hover table-striped target_totals_user">
							<thead>
							<tr>
								<th>Practices</th>
								<th>Target</th>
								<th>Mantra</th>
								<th>Accumulated before joining</th>
								<th>Total Count</th>
								<th>Archive</th>
							</tr>
							</thead>
							<tbody class="data_target_totals_user">';
        foreach ($get_practice as $key => $value) {
            $name[] = unserialize($value->value);
        }
        $name = array_sort($name, 'practices_name', SORT_ASC);
        foreach ($name as $key => $value) {
            $post = get_page_by_title($value['practices_name'], OBJECT, 'practic');
            $postMeta = get_post_meta($post->ID, 'wpcf-mantra', true);
            $practice_description = ($value['practice_description'] == '') ? $postMeta : $value['practice_description'];
            $manis_done = $wpdb->get_results("SELECT a.ID as ID FROM " . $wpdb->prefix . "posts a INNER JOIN " . $wpdb->prefix . "postmeta b  WHERE a.`post_title`='" . $userdata->user_login . "' AND b.meta_value='" . $value['practices_name'] . "' AND a.`ID` = b.`post_id` AND a.`post_status`='private'");
            foreach ($manis_done as $practice) {
                $date_manis = get_post_meta($practice->ID, 'wpcf-date-center', true);
                if ($date_manis > $meta_data['wpcf-acc-start-date'][0] && $date_manis < $meta_data['wpcf-acc-end-date'][0]) {
                    $total_user_practice += get_post_meta($practice->ID, 'wpcf-manis-done', true);
                }
            }
            $html_practice .= '<tr class="' . $value['practices_name'] . '">
								<td>' . $value['practices_name'] . '</td>
								 <td class="target">' . number_format(sum_all_target_count($value, $post_value['id'])) . '</td>
								<td class="description">' . $practice_description . '</td>
								<td class="starting_count">' . number_format(sum_all_starting_count($value, $post_value['id'])) . '</td>
								<td class="total_count">' . number_format($total_user_practice) . '</td>
								 <td class="archive_status"><input type="checkbox" checked/></td>
							</tr>';
            $total_user_practice = 0;
            $target = '';
        }
        $html_practice .= '    </tbody>
						</table>';

        echo json_encode(array(
            'msg' => 'sendinfo',
            'err' => 0,
            'type' => 3,
            'html_practice' => $html_practice,
            'id' => $user_id,
        ));
        exit;
    }
}

//relationship post type
function relationship_post_type() {
    p2p_register_connection_type(array(
        'name' => 'practic_to_acc',
        'from' => 'practic',
        'to' => 'accumulations'
    ));
}

add_action('p2p_init', 'relationship_post_type');
