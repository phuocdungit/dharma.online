<?php 
if (is_admin()) {

    /**
     * http://www.wpoptimus.com/626/7-ways-disable-update-wordpress-notifications/
     * @global type $wp_version
     * @return type
     */
//   function remove_core_updates() {
//        global $wp_version;
//        return(object) array('last_checked' => time(), 'version_checked' => $wp_version,);
//    }
//    add_filter('pre_site_transient_update_core', 'remove_core_updates');
//    add_filter('pre_site_transient_update_plugins', 'remove_core_updates');
//    add_filter('pre_site_transient_update_themes', 'remove_core_updates');
 
}

function remove_editor() {
  remove_post_type_support('services', 'editor');
  remove_post_type_support('menus', 'editor');
  remove_post_type_support('contacts-eta', 'editor');
  remove_post_type_support('animations', 'editor');  
 // remove_post_type_support('actualite', 'editor');
//  remove_post_type_support('actualite', 'editor');
  remove_post_type_support('alletablissements', 'editor');
}
add_action('admin_init', 'remove_editor');

