<a href="javascript:void(0);" id="shareBtn" class="button">
    <?php echo esc_html__($button_text, 'Divi'); ?>
</a>
<script src="https://connect.facebook.net/en_US/all.js" async></script>
<script>
    jQuery(document).ready(function() {
        function initFB() {
            FB.init({
                appId: '139471563374615',
                status: true,
                xfbml: true,
                version: 'v2.7'
            });
        }
        if (typeof (FB) != 'undefined'
                && FB != null) {
            initFB();
        }

        document.getElementById('shareBtn').onclick = function() {
            initFB();
            FB.ui({
                method: 'feed',
                link: '<?php echo $share_link; ?>'
            }, function(response) {
            });
//            if (headline && picture && description) {
//                FB.ui({
//                    method: 'feed',
//                    name: headline,
//                    link: url,
//                    picture: picture,
//                    description: description
//                }, function(response) {
//                });
//            } else {
//                
//            }

        }
    });
</script>