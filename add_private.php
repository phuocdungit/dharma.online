<?php

/**
 * WordPress Cron Implementation for hosts, which do not offer CRON or for which
 * the user has not set up a CRON job pointing to this file.
 *
 * The HTTP request to this file will not slow down the visitor who happens to
 * visit when the cron job is needed to run.
 *
 * @package WordPress
 */
ignore_user_abort(true);


if (!empty($_POST) || defined('DOING_AJAX') || defined('DOING_CRON'))
    die();


/**
 * Tell WordPress we are doing the CRON task.
 *
 * @var bool
 */
define('DOING_CRON', true);


if (!defined('ABSPATH')) {

    /** Set up WordPress environment */
    require_once(dirname(__FILE__) . '/wp-load.php');
}

//Begin accumulations
$argruments = array(
    'numberposts' => -1,
    'post_type' => 'accumulations',
    'post_status' => 'publish',
    'orderby' => 'name',
    'order' => 'ASC'
);
$AllPost = get_posts($argruments);
if ($AllPost) {
    foreach ($AllPost as $key => $value) {
        $post = array('ID' => $value->ID, 'post_status' => 'private');
        wp_update_post($post);
    }
    echo '<h2>Update Accumulations Done</h2>';
}
//End accumulations

//Begin list-center

$argruments = array(
    'numberposts' => -1,
    'post_type' => 'list-center',
    'post_status' => 'publish',
    'orderby' => 'name',
    'order' => 'ASC'
);
$AllPost = get_posts($argruments);
if ($AllPost) {
    foreach ($AllPost as $key => $value) {
        $post = array('ID' => $value->ID, 'post_status' => 'private');
        wp_update_post($post);
    }
    echo '<h2>Update list center done</h2>';
}
//End list-center

//Begin manis-history

$argruments = array(
    'numberposts' => -1,
    'post_type' => 'manis-history',
    'post_status' => 'publish',
    'orderby' => 'name',
    'order' => 'ASC'
);
$AllPost = get_posts($argruments);
if ($AllPost) {
    foreach ($AllPost as $key => $value) {
        $post = array('ID' => $value->ID, 'post_status' => 'private');
        wp_update_post($post);
    }
    echo '<h2>Update manis history done</h2>';
}
//End manis-history

//Begin practic

$argruments = array(
    'numberposts' => -1,
    'post_type' => 'practic',
    'post_status' => 'publish',
    'orderby' => 'name',
    'order' => 'ASC'
);
$AllPost = get_posts($argruments);
if ($AllPost) {
    foreach ($AllPost as $key => $value) {
        $post = array('ID' => $value->ID, 'post_status' => 'private');
        wp_update_post($post);
    }
    echo '<h2>Update practic done</h2>';
}
//End practic

//Begin region

$argruments = array(
    'numberposts' => -1,
    'post_type' => 'region',
    'post_status' => 'publish',
    'orderby' => 'name',
    'order' => 'ASC'
);
$AllPost = get_posts($argruments);
if ($AllPost) {
    foreach ($AllPost as $key => $value) {
        $post = array('ID' => $value->ID, 'post_status' => 'private');
        wp_update_post($post);
    }
    echo '<h2>Update region done</h2>';
}
//End region
//Begin theme-mail

$argruments = array(
    'numberposts' => -1,
    'post_type' => 'theme-mail',
    'post_status' => 'publish',
    'orderby' => 'name',
    'order' => 'ASC'
);
$AllPost = get_posts($argruments);
if ($AllPost) {
    foreach ($AllPost as $key => $value) {
        $post = array('ID' => $value->ID, 'post_status' => 'private');
        wp_update_post($post);
    }
    echo '<h2>Update theme mail done</h2>';
}
//End region


