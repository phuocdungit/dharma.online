<?php

/**

 * WordPress Cron Implementation for hosts, which do not offer CRON or for which

 * the user has not set up a CRON job pointing to this file.

 *

 * The HTTP request to this file will not slow down the visitor who happens to

 * visit when the cron job is needed to run.

 *

 * @package WordPress

 */
ignore_user_abort(true);



if (!empty($_POST) || defined('DOING_AJAX') || defined('DOING_CRON'))
    die();



/**

 * Tell WordPress we are doing the CRON task.

 *

 * @var bool

 */
define('DOING_CRON', true);



if (!defined('ABSPATH')) {

    /** Set up WordPress environment */
    require_once( dirname(__FILE__) . '/wp-load.php' );
}
//Begin Add Practices "Chenrezig" User

$args = array(
    'role' => '',
    'role' => 'Subscriber',
    'orderby' => 'ID',
    'order' => 'ASC',
    'offset' => '',
    'meta_query' => array(
        array(
            'key' => 'user_meta_current_practice',
            'compare' => 'NOT EXISTS',
        ),
    ),
    'orderby' => 'login',
    'order' => 'ASC',
    'count_total' => false,
    'fields' => 'all',
);
$blogusers = get_users($args);
echo '<ol>';
foreach ($blogusers as $value) {
    $data = array(
        'practices_name' => 'Chenrezig',
        'practice_description' => '',
        'starting_count' => 0,
        'target' => 0
    );
    update_user_meta($value->ID, 'user_meta_current_practice', $data);
    echo '<li><a  target="_blank" href="' . get_edit_user_link($value->ID) . '">' . $value->user_email . '</a></li>';
}
echo '</ol>';
echo '<h2>Add Practices "Chenrezig" User  => <b>Done</b></h2>';

//End Add Practices "Chenrezig" User


//Begin Add Center "Other Friends - International" User
$args = array(
    'role' => '',
    'role' => 'Subscriber',
    'orderby' => 'ID',
    'order' => 'ASC',
    'offset' => '',
    'meta_query' => array(
        array(
            'key' => 'user_meta_center_filter',
            'compare' => 'NOT EXISTS',
        ),
    ),
    'orderby' => 'login',
    'order' => 'ASC',
    'count_total' => false,
    'fields' => 'all',
);
$blogusers = get_users($args);

echo '<ol>';
foreach ($blogusers as $value) {
    update_user_meta($value->ID, 'user_meta_center_filter', 'Other Friends - International');
    echo '<li><a  target="_blank" href="' . get_edit_user_link($value->ID) . '">' . $value->user_email . '</a></li>';
}
echo '</ol>';
echo '<h2>Add Center "Other Friends - International" User => <b>Done</b></h2>';

//End Add Center "Other Friends - International" User


//Begin Add Practice "Chenrezig" to history

$argruments = array(
    'numberposts' => -1,
    'post_type' => 'manis-history',
    'post_status' => 'publish',
//    'post_status' => 'trash',
    'orderby' => 'name',
    'order' => 'ASC',
    'meta_query' => array(
        'relation' => 'AND',
        array(
            'key' => 'wpcf-my-practice',
            'compare' => 'NOT EXISTS',
//            'compare' => 'EXISTS',
        ),
    )
);
$historyPost = get_posts($argruments);
echo '<ol>';
foreach ($historyPost as $key => $value) {
    update_post_meta($value->ID, 'wpcf-my-practice', 'Chenrezig');
    echo '<li>' . $value->post_title . '</li>';
}
echo '</ol>';
echo '<h2>Add Practice "Chenrezig" to history => <b>Done</b></h2>';

//End Add Practice "Chenrezig" to history


//Begin Add Practice to Practices List
$practic = array
    (
    '1' => 'Amitabha',
    '2' => 'Green Tara',
    '3' => 'Mahakala',
    '4' => 'Vajradhara',
    '5' => 'Vajrasattva',
    '6' => 'White Chenrezig',
    '7' => 'White Tara',
    '8' => 'Amitayus',
    '9' => 'Guru Rinpoche',
    '10' => 'Vajrakilaya',
    '11' => 'Yellow Dzambhala',
    '12' => 'Phowa',
    '13' => '7 point mind training',
    '14' => 'Dzambala',
    '15' => 'Dukar',
    '16' => '8th Karmapa',
    '17' => 'Sangje Menla',
    '18' => 'Kalachakra',
    '19' => 'Medicine Buddha',
    '20' => 'Vajrapani',
    '21' => 'Milarepa',
    '22' => 'Shinay Meditation',
    '23' => 'Chod',
    '24' => 'Red Chenrezig – Gyalwa Gyamtso',
    '25' => 'Korlo Demchok',
    '26' => 'Karma Pakshi',
    '27' => '15th Karmapa',
    '28' => 'Gampopa',
    '29' => '1000 Buddha transmission',
    '30' => '35 buddhas',
    '31' => 'White Mahakala',
    '32' => 'Rechungpa',
    '33' => 'Marpa',
    '34' => '21 Tara',
    '35' => 'Manjushri',
    '36' => 'Avalokiteshvara',
    '37' => 'Döjo Bumzan',
    '38' => 'Yeshe Tsogyal',
    '39' => 'Gyalwa Longchen Rabjam',
    '40' => 'Red Tara',
    '41' => 'Könchog Chidü Guru Rinpoche',
    '42' => 'Akshobhya',
    '43' => 'Kubera',
    '44' => 'Kurukulle',
    '45' => 'Ushnisha Vijaya',
    '46' => 'Dorje Pagmo',
    '47' => 'short mahakala madagma',
    '48' => 'Guru Rinpoche Prayer',
    '49' => 'Phwa',
    '50' => 'Kagyu Ngag Dzo',
    '51' => 'Oser Chenmo/Marici',
    '52' => '3rd Karmapa',
    '53' => 'Dorje Sempa',
    '54' => 'Buddha Shakyamuni',
    '55' => 'Vajrayogini',
    '56' => 'Chenrezig',
);
echo '<ol>';
foreach ($practic as $value) {
    $post = get_page_by_title($value, OBJECT, 'practic');
    if (!$post) {
        $post_id = wp_insert_post(array(
            'post_author' => 'sysadmin',
            'post_title' => $value,
            'post_type' => 'practic',
            'post_status' => 'publish',
        ));
        echo '<li>' . $value . '</li>';
    }
}
echo '</ol>';
echo '<h2>Add Practices => <b>Done</b></h2>';

//End Add Practice to Practices List

//Begin Add Countries to Countries List
$countries = array
    (
    'AF' => 'Afghanistan',
    'AX' => 'Aland Islands',
    'AL' => 'Albania',
    'DZ' => 'Algeria',
    'AS' => 'American Samoa',
    'AD' => 'Andorra',
    'AO' => 'Angola',
    'AI' => 'Anguilla',
    'AQ' => 'Antarctica',
    'AG' => 'Antigua And Barbuda',
    'AR' => 'Argentina',
    'AM' => 'Armenia',
    'AW' => 'Aruba',
    'AU' => 'Australia',
    'AT' => 'Austria',
    'AZ' => 'Azerbaijan',
    'BS' => 'Bahamas',
    'BH' => 'Bahrain',
    'BD' => 'Bangladesh',
    'BB' => 'Barbados',
    'BY' => 'Belarus',
    'BE' => 'Belgium',
    'BZ' => 'Belize',
    'BJ' => 'Benin',
    'BM' => 'Bermuda',
    'BT' => 'Bhutan',
    'BO' => 'Bolivia',
    'BA' => 'Bosnia And Herzegovina',
    'BW' => 'Botswana',
    'BV' => 'Bouvet Island',
    'BR' => 'Brazil',
    'IO' => 'British Indian Ocean Territory',
    'BN' => 'Brunei Darussalam',
    'BG' => 'Bulgaria',
    'BF' => 'Burkina Faso',
    'BI' => 'Burundi',
    'KH' => 'Cambodia',
    'CM' => 'Cameroon',
    'CA' => 'Canada',
    'CV' => 'Cape Verde',
    'KY' => 'Cayman Islands',
    'CF' => 'Central African Republic',
    'TD' => 'Chad',
    'CL' => 'Chile',
    'CN' => 'China',
    'CX' => 'Christmas Island',
    'CC' => 'Cocos (Keeling) Islands',
    'CO' => 'Colombia',
    'KM' => 'Comoros',
    'CG' => 'Congo',
    'CD' => 'Congo, Democratic Republic',
    'CK' => 'Cook Islands',
    'CR' => 'Costa Rica',
    'CI' => 'Cote D\'Ivoire',
    'HR' => 'Croatia',
    'CU' => 'Cuba',
    'CY' => 'Cyprus',
    'CZ' => 'Czech Republic',
    'DK' => 'Denmark',
    'DJ' => 'Djibouti',
    'DM' => 'Dominica',
    'DO' => 'Dominican Republic',
    'EC' => 'Ecuador',
    'EG' => 'Egypt',
    'SV' => 'El Salvador',
    'GQ' => 'Equatorial Guinea',
    'ER' => 'Eritrea',
    'EE' => 'Estonia',
    'ET' => 'Ethiopia',
    'FK' => 'Falkland Islands (Malvinas)',
    'FO' => 'Faroe Islands',
    'FJ' => 'Fiji',
    'FI' => 'Finland',
    'FR' => 'France',
    'GF' => 'French Guiana',
    'PF' => 'French Polynesia',
    'TF' => 'French Southern Territories',
    'GA' => 'Gabon',
    'GM' => 'Gambia',
    'GE' => 'Georgia',
    'DE' => 'Germany',
    'GH' => 'Ghana',
    'GI' => 'Gibraltar',
    'GR' => 'Greece',
    'GL' => 'Greenland',
    'GD' => 'Grenada',
    'GP' => 'Guadeloupe',
    'GU' => 'Guam',
    'GT' => 'Guatemala',
    'GG' => 'Guernsey',
    'GN' => 'Guinea',
    'GW' => 'Guinea-Bissau',
    'GY' => 'Guyana',
    'HT' => 'Haiti',
    'HM' => 'Heard Island & Mcdonald Islands',
    'VA' => 'Holy See (Vatican City State)',
    'HN' => 'Honduras',
    'HK' => 'Hong Kong',
    'HU' => 'Hungary',
    'IS' => 'Iceland',
    'IN' => 'India',
    'ID' => 'Indonesia',
    'IR' => 'Iran, Islamic Republic Of',
    'IQ' => 'Iraq',
    'IE' => 'Ireland',
    'IM' => 'Isle Of Man',
    'IL' => 'Israel',
    'IT' => 'Italy',
    'JM' => 'Jamaica',
    'JP' => 'Japan',
    'JE' => 'Jersey',
    'JO' => 'Jordan',
    'KZ' => 'Kazakhstan',
    'KE' => 'Kenya',
    'KI' => 'Kiribati',
    'KR' => 'Korea',
    'KW' => 'Kuwait',
    'KG' => 'Kyrgyzstan',
    'LA' => 'Lao People\'s Democratic Republic',
    'LV' => 'Latvia',
    'LB' => 'Lebanon',
    'LS' => 'Lesotho',
    'LR' => 'Liberia',
    'LY' => 'Libyan Arab Jamahiriya',
    'LI' => 'Liechtenstein',
    'LT' => 'Lithuania',
    'LU' => 'Luxembourg',
    'MO' => 'Macao',
    'MK' => 'Macedonia',
    'MG' => 'Madagascar',
    'MW' => 'Malawi',
    'MY' => 'Malaysia',
    'MV' => 'Maldives',
    'ML' => 'Mali',
    'MT' => 'Malta',
    'MH' => 'Marshall Islands',
    'MQ' => 'Martinique',
    'MR' => 'Mauritania',
    'MU' => 'Mauritius',
    'YT' => 'Mayotte',
    'MX' => 'Mexico',
    'FM' => 'Micronesia, Federated States Of',
    'MD' => 'Moldova',
    'MC' => 'Monaco',
    'MN' => 'Mongolia',
    'ME' => 'Montenegro',
    'MS' => 'Montserrat',
    'MA' => 'Morocco',
    'MZ' => 'Mozambique',
    'MM' => 'Myanmar',
    'NA' => 'Namibia',
    'NR' => 'Nauru',
    'NP' => 'Nepal',
    'NL' => 'Netherlands',
    'AN' => 'Netherlands Antilles',
    'NC' => 'New Caledonia',
    'NZ' => 'New Zealand',
    'NI' => 'Nicaragua',
    'NE' => 'Niger',
    'NG' => 'Nigeria',
    'NU' => 'Niue',
    'NF' => 'Norfolk Island',
    'MP' => 'Northern Mariana Islands',
    'NO' => 'Norway',
    'OM' => 'Oman',
    'PK' => 'Pakistan',
    'PW' => 'Palau',
    'PS' => 'Palestinian Territory, Occupied',
    'PA' => 'Panama',
    'PG' => 'Papua New Guinea',
    'PY' => 'Paraguay',
    'PE' => 'Peru',
    'PH' => 'Philippines',
    'PN' => 'Pitcairn',
    'PL' => 'Poland',
    'PT' => 'Portugal',
    'PR' => 'Puerto Rico',
    'QA' => 'Qatar',
    'RE' => 'Reunion',
    'RO' => 'Romania',
    'RU' => 'Russian Federation',
    'RW' => 'Rwanda',
    'BL' => 'Saint Barthelemy',
    'SH' => 'Saint Helena',
    'KN' => 'Saint Kitts And Nevis',
    'LC' => 'Saint Lucia',
    'MF' => 'Saint Martin',
    'PM' => 'Saint Pierre And Miquelon',
    'VC' => 'Saint Vincent And Grenadines',
    'WS' => 'Samoa',
    'SM' => 'San Marino',
    'ST' => 'Sao Tome And Principe',
    'SA' => 'Saudi Arabia',
    'SN' => 'Senegal',
    'RS' => 'Serbia',
    'SC' => 'Seychelles',
    'SL' => 'Sierra Leone',
    'SG' => 'Singapore',
    'SK' => 'Slovakia',
    'SI' => 'Slovenia',
    'SB' => 'Solomon Islands',
    'SO' => 'Somalia',
    'ZA' => 'South Africa',
    'GS' => 'South Georgia And Sandwich Isl.',
    'ES' => 'Spain',
    'LK' => 'Sri Lanka',
    'SD' => 'Sudan',
    'SR' => 'Suriname',
    'SJ' => 'Svalbard And Jan Mayen',
    'SZ' => 'Swaziland',
    'SE' => 'Sweden',
    'CH' => 'Switzerland',
    'SY' => 'Syrian Arab Republic',
    'TW' => 'Taiwan',
    'TJ' => 'Tajikistan',
    'TZ' => 'Tanzania',
    'TH' => 'Thailand',
    'TL' => 'Timor-Leste',
    'TG' => 'Togo',
    'TK' => 'Tokelau',
    'TO' => 'Tonga',
    'TT' => 'Trinidad And Tobago',
    'TN' => 'Tunisia',
    'TR' => 'Turkey',
    'TM' => 'Turkmenistan',
    'TC' => 'Turks And Caicos Islands',
    'TV' => 'Tuvalu',
    'UG' => 'Uganda',
    'UA' => 'Ukraine',
    'AE' => 'United Arab Emirates',
    'GB' => 'United Kingdom',
    'US' => 'United States',
    'UM' => 'United States Outlying Islands',
    'UY' => 'Uruguay',
    'UZ' => 'Uzbekistan',
    'VU' => 'Vanuatu',
    'VE' => 'Venezuela',
    'VN' => 'Viet Nam',
    'VG' => 'Virgin Islands, British',
    'VI' => 'Virgin Islands, U.S.',
    'WF' => 'Wallis And Futuna',
    'EH' => 'Western Sahara',
    'YE' => 'Yemen',
    'ZM' => 'Zambia',
    'ZW' => 'Zimbabwe',
);
echo '<ol>';
foreach ($countries as $value) {
    $post = get_page_by_title($value, OBJECT, 'region');
    if(!$post){
    $post_id = wp_insert_post(array(
            'post_author' => 'sysadmin',
            'post_title' => $value,
            'post_type' => 'region',
            'post_status' => 'publish',
        ));
        echo '<li>' . $value . '</li>';
    }
    
}
echo '</ol>';
echo '<h2>Add Countries => <b>Done</b></h2>';

//End Add Countries to Countries List